
import matplotlib.pyplot as plt
import pandas as pd

plt.ion() #Activando trabajo de manera interactiva
print(plt.isinteractive()) #verificando interactividad

# header none indica que la tabla no tiene encabezado
data = pd.read_csv("Analitica Datos Python\cursoanalisis\car.csv", header= None )
#asignando un encabezado a la tabla
data.columns = ['Price','Maintenance Cost','Number of Doors','Capacity','Size of Luggage Boot','Safety','Decision',]


print("5. graficando valores selecionados")
#para no alterar el dataframe puede almacenar los valores en una variable  var1 = data['Decision'].value_counts().sort_index(ascending= True)
desicion = data['Decision'].value_counts()

plt.plot(desicion.index, desicion.values)
#se debe activar para ver las graficas


