

import matplotlib.pyplot as plt
import pandas as pd

#se debe activar para ver las graficas
plt.ion() #Activando trabajo de manera interactiva
print(plt.isinteractive()) #verificando interactividad

#https://archive.ics.uci.edu/ml/machine-learning-databases/car/

# header none indica que la tabla no tiene encabezado
data = pd.read_csv("Analitica Datos Python\cursoanalisis\car.csv", header= None)
#asignando un encabezado a la tabla
data.columns = ['Price','Maintenance Cost','Number of Doors','Capacity','Size of Luggage Boot','Safety','Decision',]
print("1. visualizacion de dataframes ")
print("----------------Mostrando las primeras 5 filas del dataframe-----------------------------------------------------------------------------")
print(data.head(5)) #muestra los primeros 5 filas de la tabla
print("----------------5 filas aleatorias------------------------------------------------------------------------------")
print(data.sample(5)) #muestra 5 datos aleatorios de filas
print("----------------ultimas 5 filas ------------------------------------------------------------------------------")
print(data.tail(5)) # muestra los ultimos 5 elementos de la tabla

# caracteristicas de los dataframes 
print("2. caracteristicas de los dataframes  ")
print("mostrando la cantidad de atributos: ", data.shape, "(filas, columnas)") 
print()
print("mostrando la cantidad de datos de toda la tabla: ", data.size) 
print()

# buscar registros al azar  por columna y cantidades para hacerlo con los primero usamos head o con lo ultimos tail
print("3. busqueda de datos por columnas y cantidades")
print(data['Price'].sample(5))

print("Busqueda en columnas por rangos")
print(data['Price'][:5]) #data['encCOlumna'][fila x:filas y]

print("Mostrar datos de varias columnas al tiempo")
print(data[['Price', 'Decision','Safety' ]].tail(4)) 
print()

print("4. Ordenamiento y agrupacion")
print(".value_counts() toma los valores repetidos y los unifica y muestra ")
print(data['Decision'].value_counts())


print("Ordenando de manera ascendente por el nombre de los datos ")
print(data['Decision'].value_counts().sort_index(ascending= True))#ascending= False hace que el orden sea descendente
print()

print("5. graficando valores selecionados")
#para no alterar el dataframe puede almacenar los valores en una variable  var1 = data['Decision'].value_counts().sort_index(ascending= True)
desicion = data['Decision'].value_counts()

desicion.plot(kind= 'bar')



print("6. mosrar los valores unicos de las columnas ")
print(data['Price'].unique())

#reemplazar valores 
print("remplazar valores unicos de una columna por datos numericos ")
# reemplazo los valores unicos 'vhigh', 'high', 'med', 'low' por  4,3,2,1
data.Price.replace(('vhigh', 'high', 'med', 'low'),(4,3,2,1), inplace= True)
print(data['Price'].unique())

#graficando los nuevos valores

price = data['Price'].value_counts()
colors = ['#AA6547','#DD26','#CA8931','#123214']
price.plot(kind ='bar', color =colors)
plt.xlabel('Precio')
plt.ylabel('Autos')
plt.title('Precio de los autos')

plt.show()

