import modulo_normalizacionD as md
from sklearn.linear_model import LinearRegression #regresiones
import seaborn as sns #libreria basada en matplotlib para visualizar regreciones lineales
import matplotlib.pyplot as plt
import pandas as pd

'''
EN EST EJERCICIO SE VA A ELEJRI MOTOR POTENCIA , ASIENTOS Y COMBUSTIBLE PARA PREDECIR EL PRECIO 
COMBUSTIBLE ES UNA COLUMNA CATEGORICA A SI QUE SERA CONVERTIDO A DUMMI
Y MOTOR Y POTENCIA SERAN NORMALIZADAS PARA MAYOR EFECTIVIDAD EN LA PREDICCION
'''
#---enviando la ruta para leer el archivo
ruta = 'C:\\Users\\Usuario\\Documents\\develop\\Archivos para analisis\\'
fichero = 'datosModificados.csv' #nombre del fichero
datos= md.cargardatos(ruta, fichero,',')
print(datos.head())

print("-------------------------------verificacion datos nulos------------------------------------------------")
print(datos.isnull().sum()) #verificando datos nulos


print("detectando columnas con datos nulos")
#detectando columnas con datos nulos
columna_nulos = [col for col in datos.columns  if datos[col].isnull().any()]
print('columnas con datos nuelos : ', columna_nulos)


#OJO ESTE PASO APLICA SOLO PARA ESTOS CASOS  -cuando existe una columna en el daframe con indice lo que hacemos es afignarlo como indice 
#solo para este caso en que exite una columna numerdad  podemos seleccionarla como indice de filas SOLO PARA ESTE CASO 
datos.set_index('indice', inplace= True)
print("----------------------------------------------------------------------------------------------")
print(datos.head(3))


print("------------------------------NOMBRE DE COLUMNAS-----------------------------------------------------")
#columnas
print(md.verNombrecolumnas(datos))

print("--------------------VARIABLES DUMIES----------------------------------------------------------------")
#hot encoding, convirtiendo variable categorica combustible en dummies
datos_dummies = md.obtenerDummies(datos,'combustible')
print(datos_dummies.head(3))

#concatenando datos dummies con el dataframe
datos = pd.concat([datos,datos_dummies], axis=1)

print("--------------------------Nombre de columnas incluyendo variables dummies-----------------------")
#columnas
print(md.verNombrecolumnas(datos))


#normalizacion de columnas los valores numericos altos deben ser normalizados
'''
ejemplo columna motor y potencia
'''
datos = md.normalizacionColumna(datos,'motor')
datos = md.normalizacionColumna(datos,'potencia')


#===========modelo=======================================================
#creando la regresion 
regresion = LinearRegression()

#====================================================================
# opcion de hacer la regresion lineal entrenando las variables 
#--------------SEPARACION------ENTRENAMIENTO Y TESTING------------------------------
''' Retiramos la variable categorica combustible  y agregamos las variables dummis 'CNG', 'Diesel', 'Electric',
       'LPG', 'Petrol '''

X= datos[['motor', 'potencia', 'asientos','CNG', 'Diesel', 'Electric',
       'LPG', 'Petrol']]

print(X)
y= datos['precio']

#========================================================================================
#========================================================================================

# ---------prediccion------ informativo
#entrenamiento o ajuste de modelo
regresion.fit(X,y)


prediccion = regresion.predict(X)
a = regresion.intercept_
b = regresion.coef_
# Mostramos ecuacion con variables entrenadas esto es solo informativo 
print('*'*100)
#print(f'y={a}+{b1}*x1 +b2* x2 + b3* x3 ')
print('*'*100)
print('Prediccion')
print(prediccion)
print(y)
print('*'*100)
print("El Resultado de la ecuacion debe ser proximo a 1 -->", regresion.score(X,y)) # lo ideal es que este proximo a 1

#comparando el resultado con el valor d eprediccion

resultado = {'Real': datos['precio'], 'Prediccion': prediccion}
Dataresultado = pd.DataFrame(data=resultado) #creanod nuevoi dataframe para ver informacion real vs la preicha
print(Dataresultado.head(4))

#verificando metricas para la efectividad del modelo

from sklearn import metrics
#error cuadratico medio  INVESTIGAR LOS VALORES LIMITES 
print("error cuadratico medio")
ECM = metrics.mean_squared_error(datos['precio'], prediccion)
print(ECM)

#rcuadrado
print("EL r2 siempre debe ser igual al resultado de la ecuacion  ")
r_cuadrado = metrics.r2_score(datos['precio'], prediccion)
print(r_cuadrado)

#grafica

plt.plot(datos['motor'],prediccion, 'r',label = 'prediccion')
plt.scatter(datos['motor'],y,label= 'Datos')
plt.title("Regresion lineal ")
plt.xlabel('motor')
plt.ylabel('Y')
plt.legend()
plt.grid()
plt.show()