
#------------------ manejo de cadena de caracteres-------------
#---identificando letras por posiciones----
#fruta = 'fresa'
#letra= fruta[4]
#print(letra)
#-------uso de la funcion len()---saber la longitud de una cadena string-------------
#fruta = 'fruticas'
#letra= len(fruta)
#print('la cantidad de caracteres es'+str(letra))
# -----------------obteniendo el ultimo caracter de la cadena -----
#fruta = "uva"
#cant = len(fruta)
#ultima= fruta[cant -1]
#print('la ultima letra de la palabra es: '+str(ultima))
# -----------------rebanadas o trozos de  strings------------si el primer indice es mayor o igual que el segundo devuelve una cadena vacia---------------------------------
#fruta = "maracuya"
#cant = len(fruta)
#ultima= fruta[0:2]
#ultima= fruta[0:] # omite desde el primer digito
#ultima= fruta[:2] # omite despues del segundo digito
#print('la ultima letra de la palabra es: '+str(ultima))
# -------------una cadena de caracteres no se puede inmutar pero si se puede tomar la original y crear una nueva y modificar-----
#fruta = "cebolla"
#fruta2 = "x" + fruta[5:]
#print(fruta2)
#-----------operador IN -----verificamos si una palabra esta dentro de la cadena de caracteres------------------
#sopa= "sopa platano"
#busqueda = "platano"
#print(busqueda in sopa)
#------------------------oredenar cadenas en orden alfavetico------------------debe comprar mayusculas con mayusculas y minusculas con minusculas-----------------------------------------
#verdura = "ensalada"
#ugo = "lulo"
#if verdura > jugo:
    #print(verdura + " vienes despues de "+ jugo)
#else:
    #print(jugo+ " viene despues de " +verdura)

# ---------metodos string -----------------
#------metodo uper -----------convierte la cadena a mayusculas-----
fruta = "Curuva"
print(fruta.upper()) #convierte la cadena a mayusculas
print(fruta.find("v")) #devuelve la ubicacion de un caracter
print(fruta.strip())# quita los pesacios que esten al principio o al final de la cadena
print(fruta.startswith('a')) #sirve para identificar si la cadena inicia con la letra indicada
print(fruta.lower()) #convierte los caracteres en minusculas 
print(fruta.lower().startswith('c')) #convinacion identifica la primera letra y la convierte minuscula
#---FORMATOS----- caracteres especiales -------------------
#------ %d-enteros--
tuverculos =34
hoas= 12
print("jugo de: %d" %tuverculos)  #% concatena numeros con string
print("jugo de: %d y bolsas de hojas %d " %(tuverculos, hoas)) 
# -----%f-----------decimales---------
print("jugo de: %f" %tuverculos)  #% concatena numeros con string
#-----%s -----representar cadenas-
print("jugo de: %s" %tuverculos)  #% concatena numeros con string
#-------buscar mas letras de formatos y pasar la foto a aqui