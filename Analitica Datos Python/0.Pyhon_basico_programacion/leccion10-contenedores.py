# ----convertir cadenas en listas tuplas y conjuntos
c = "hola"
c= list(c)#lista
print(c)
c = set(c)#conjunto
print(c)
c = tuple(c)#tupla
print(c)

#-----convirtiendo una lista en cadena
L = list("hola mundo")
L = "".join(L)#join sirve para lostas, tuplas etc

#----tranformar cadena en diccionario
var1 = "Hola"
dic1 = dict(zip(range(len(var1)),var1))#fun zip recibe 2 parametros
print(dic1)

#----tranformar lista en diccionario
var1 = list("como estas?")
dic1 = dict(zip(range(len(var1)),var1))#fun zip recibe 2 parametros
print(dic1)
#---convirtiendo diccionario a tuplas
dict = {
    0: 'c',
    1: 'a',
    2: 'l',
    3: 'l',
    4: 'e'
}
dict = set(dict.keys())
print(dict)
#--- 
dict1 = {
    0: 'c',
    1: 'a',
    2: 'l',
    3: 'l',
    4: 'e'
}
dict1 = set(dict1.values())
print(dict1)
#--- convertir diccionario a 
dict2 = {
    0: 'c',
    1: 'a',
    2: 'l',
    3: 'l',
    4: 'e'
}
dict2 = set(dict2.items())
print(dict2)