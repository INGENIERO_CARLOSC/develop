#---- conjunto de datos ordenados separados por comas y representados con []  son MUTABLES--se dejan manipular-

nombre = ["carlos","andres","maria","tifany","susana"]
print("Lista general: ",nombre)
print("--0----Mostrando el tipo de dato  -------------")
print("El tipo de elemento es: ",type(nombre))# identifica el tipo de valor
print()
#----- conocer la dimecion de una lista ----
print("---1-----conocer la dimecion de una lista ----------------")
print("La dimencion de la lista es ", len(nombre))
print()
#----accede a la informacion de forma individual de una lista ---
print("-2---accede a la informacion de forma individual de una lista---")
print("el elemento numero 2 es: ",nombre[1])
print()
#--- navegacion inversa en la lista ---
print("---3--------navegacion inversa en la lista----------------")
print("El nombre en la poscion inversa -3 es: ",nombre[-3])
print()
#-----imprimir un rango ---
print("----4--------------imprimir un rango-------------------")
print("Los nombres en el rango 2 al 5 son: ",nombre[2:5])
print()
#----iterar contenido de una lista con for --
print("---5---iterar contenido de una lista con for----------")
for i in nombre:
    print(i, end= ", ")
print()
#------actualizando o cambiando datos de una listad --
print("----6--actualizando o cambiando datos de una listad --------")
print("lista original :",nombre)
nombre [0]= "pepe"
print("Lista actualizada: ",nombre)
print()
#-----validando un elemento de la lista---
print("----7-----validando un elemento de la lista con un if---------")
if "carlos" in nombre:
    print("Carlos sigue en la clase")
else:
    print("Carlos salio de la clase")
print()
#-----agregando elemento al final de la lista --
print("----8-----agregando elemento al final de la lista -------------")
nombre.append("Elemento nuevo")
print(nombre)
print()
#-----agregar informacion en el indice i posicion deseada-
print("-----9---agregar informacion en el indice i posicion deseada----")
print("Lista actual: ", nombre)
nombre.insert(4, "segundo elemento nuevo")
print("Lista modificada: ",nombre)
print()
#------remover un elemento  especifico de la lista ----
print("----10----eliminar -elemento  especifico de la lista-------------")
nombre.remove("pepe")
print(nombre)
print()
#--- --
print("----11------remover el ultimo elemento de la lista---------------")
nombre.pop()
print("Eliminando al ultimo dato ",nombre)
print()
#------remover el elemento indice o pocision---
print("----12----remover con  el elemento indice o pocision--------")
print("Eliminando el elemento 2 :",nombre)
del nombre[2]
print(nombre)
print()
#--- limpiar los elementos dentro de  toda la lista ----
print("-----13---limpiar los elementos dentro de  toda la lista -------")
nombre.clear()
print(nombre)
print()
#-----eliminar por completo una lista --
print("-----14---eliminar por completo una lista------")
del nombre
#print(nombre) #sale error por que ya no existe la lista
print()
print("--------------------------------------------------------")