# # 1  impresion de consola: print("soy un duro")------------------------------------------------------------------
#
# # 2Declaracion de variables - no es nesesario indicar el tipo de variable python lo entiende por su contenido-----
# miVariable1 = "Hola programador"  # string
# miVariable2 = 2  # numerico
# miVariable3 = 6, 4  # decimal o tupla
# miVariable4 = False  # bolean
# miVariable5 = 3.4  # float
# # identificar el tipo de variable con la propiedad type
# # print(type(miVariable1),miVariable1)
# # print(type(miVariable2),miVariable2)
# # print(type(miVariable3),miVariable3)
# # print(type(miVariable4),miVariable4)
# # print(type(miVariable5),miVariable5)
# # 3 suma de variables-----------------------------------------------------------------------------------
# a = 1
# b = 5
# suma = a + b
# # print(suma)
#
# # 4 obtener la direccion de memoria id devuelve la posicion en direccion de memoria del valor suma -------------
# # print(id(suma))
#
# # 5 uso de cadenas de caracteres -----------------------------------------------------------------------------
# cadena1 = "metallica" + " " + " es la mejor banda"  # concatenacion de cadena en una variable
# cadena2 = " Utima hora!!"
# # print(cadena2+cadena1) #concatenando cadenas con variables
# # print("La noticia es : Que "+cadena1) #concatenacion de  cadenas con signo mas
# # print("La noticia es : Que",cadena1) #concatenacion de  cadenas con el uso de la coma
#
# # 6 conviertiendo cadena a entero para hacer sumas ----------------------------------------------------------
#
# a = "1"
# b = "2"
#
# #El valor concatenado es", a + b)
# #print("pero la suma de los valores es", int(a) + int(b))  # conversion de string a entero
# # 7 variables de tipo boolean ----------------------------------------------------------------------
#
# var = True #a signamos la validez de la variable
#
# var = 1 > 2 #hacemos la operacion de comparacion
#
# #if var: #hacemos la accion deacuerdo a la valide de la logica de la variable
#     #print("el valor es verdadero")
# #else:
#     #print("el valor es falso")
#
# # 8 ingresando datos a la consola por teclado con la funcion input-------------------------------------
#
# #print("porfavor escriba un numero ")
# #numeroa = input()
# #numerob = input( " escribe un segundo valor")
#
# #print("el numero que usted escribo es",int(numeroa)+int(numerob))
#
# # 9 otra forma de convertir string a entero-----------------------------------------------------------
#
# #A = int(input(" ingrese un numero"))
# #B = int(input(" ingrese otro numero"))
# #print("La suma de  ", A, " y ",B, "es", A+B)
#
# #10 Operadores aritmeticos   +-*/ // ** %---------------------------------------------------------------
#
# # operandoa = 5
# # operandob = 3
# # #suma
# # suma= operandoa+operandob
# # print(f'Elresultado de la suma de {operandoa} mas {operandob} es: {suma}') # otra froma de imprimir valores con f literal#suma
# # #resta
# # resta= operandoa-operandob
# # print(f'Elresultado de la resta  de {operandoa} menos {operandob}es: {resta}') # otra froma de imprimir valores con f literal#resta
# # #multiplicacion
# # mult= operandoa*operandob
# # print(f'Elresultado de la multiplicacion  de {operandoa} por {operandob} es: {mult}') # otra froma de imprimir valores con f literal
# # #division
# # div= operandoa/operandob
# # #division en entero
# # diventero= operandoa//operandob
# # print(f'Elresultado de la division de {operandoa} entre {operandob} es: {div}') # otra froma de imprimir valores con f literal
# # print(f'Elresultado de la division redondeada  de {operandoa} entre {operandob} es: {diventero}') # otra froma de imprimir valores con f literal
# # #modulo o reciduo
# # mod= operandoa%operandob
# # print(f'Elresultado de residuo de la division de {operandoa} mod {operandob} es: {mod}') # otra froma de imprimir valores con f literal
# # #Operador de exponenete
# # exp= operandoa**operandob
# # print(f'Elresultado de  {operandoa} a la  {operandob} es: {exp}') # otra froma de imprimir valores con f literal
# # 11 ejercicio de perimetro y area -------------------------------------------------------------------------
# # alto = int(input("Proporciona el alto"))
# # ancho = int(input("Proporciona el ancho"))
# # print("Area",alto*ancho)
# # print("Perimetro",alto+ancho)
# #
# # 12 Operadores de Asiganacion --=-------------------------------------------------------------------------
#
# #asignacion = 10
# #print(asignacion)
# #incrementacion en 1
# #asignacion = asignacion + 1
# #print(asignacion)
# #incrementacion con reasignacion
# #asignacion += 5
# #print(asignacion)
# #decrementacion con reasignacion
# #asignacion -= 9
# #print(asignacion)
# #multiplicacion  con reasignacion
# #asignacion *= 9
# #print(asignacion)
#
# #13 Operadores de Comparacion ----------------------------------------------------------------------------
#
# a = 4
# b= 2
#
# #asignacion
# resultado = a == b
# #print(f'Resultado es: {resultado}')
# # valores distintos
# res = a != b
# #print(f'Resultado es: {res}')
# # mayor que i o menor que / mayor igual o menor igual
# may = a>b
# #print(f'Resultado es: {may}')
#
# #14 ejercicio  numero par e impar ------------------------------------------------------------------
#
# #A= int(input('Escribe un valor porfavor'))
# #if A % 2 == 0:
#    # print("El numero ",A, "es Par")
# #else:
#    # print("El numero ", A, "es Impar")
#
# #15 Ejercicio para identificar si es mayor de edad -------------------------------------------------
# #edad = 18
# #edadPersona = int(input("Ingresa la edad"))
# #if edadPersona >= edad:
#    # print("es mayor de edad")
# #else:
#     #print("Es menor de edad")
# #16 operadores logicos ---------and or not --------------------------------------------------------------
# # AND devuelve verdadero si a y b son verdaderos
# # OR devuelve verdadero si  a es verdadero y b es falso o viseversa
# # NOT devuelve verdaddero si a son es falsa , es decir invirte el valor de a
#
# aa= True
# bb= False
# #res = aa and bb
# #print(res)
#
# #res = aa or bb
# #print(res)
#
# res = not aa
# print(res)
# #17  valor dentro del rango ejercicio -----operador and----------------------------------------------------------------------
# valor =  int(input("Escriba un valor"))
# vmin = 0
# vmax = 5
#
# dentrorango = valor >= vmin and valor <=vmax
#
# if dentrorango:
#     print("El valor es",dentrorango, "esta dentro del rango")
# else:
#     print("No esta dentro del rango")

#18 Ejercicio con el operador or -----------------------------------------------------------------------------------------------
# vacaciones = False
# diadescanso= True

#if vacaciones or diadescanso:
   # print("puede ir al juego de su hijo")
#else:
    #print("No puede asistir")
#19 Ejercicio con operador Not -----------------------------------------------------------------------------------------------
# if not(vacaciones or diadescanso) :
#     print("Tiene deberes, no puede salir ")
# else:
#     print("Puede ir al juego de su hijo ")
#20 ejercicio tienda de libros ----------------------------------------------------------------------------------------------
# print("Ingresa los datos del libro ")
# nombre =input("Ingresa el nombre del libro ")
# id = int(input("Ingresa el ID libro "))
# precio = float(input("Ingresa el valor libro "))
# enviogratuito = input("Indica si es envio gratuito con True o no con False ")
#
# if enviogratuito == 'True':
#     enviogratuito = True
# elif enviogratuito == 'False':
#     enviogratuito = False
# else:
#     enviogratuito = 'valor incorrecto escriba True o False'

# print(f'''
#     Nombre: {nombre}
#     Id: {id}
#     Precio: {precio}
#     Envio Gratuito: {enviogratuito}
#
# ''')
# 21  sentencias de control---------------------------if-----------------------------------------------------------------------
# condicion = False # si la variable tiene un valor numerico o texto sera verdaddera si esta vacia con comillas  '' sera falsa
#
# if  condicion == True:
#     print("condicion verdadera")
# elif condicion == False:
#     print("condicion falsa")
# else:
#     print("condicion no reconocida")

#22 ejercicio de convertir numero a texto-----------------------------------------------------------
#
# numero = int(input("Ingrese un numero entre 1 y 3"))
# numeroTexto = ''
#
# if numero == 1:
#     numeroTexto = 'Uno'
# elif numero == 2:
#     numeroTexto = 'Dos'
# elif numero == 3:
#     numeroTexto = 'Tres'
# else:
#     numeroTexto = 'Valor fuera del rango'
#
# print(f'El Numero ingresado es : {numero} - {numeroTexto}')

# 23 sintaxis if else simplificado Operaodr ternario ----------------solo se recomienda en codigos pequeños--------------------------------

#condicion = False
#print('Condicion Verdadera' )if condicion else print('Condicion Falsa')

#24 Ejercicio de calcular la estacion segun el mes proporcionado ---------------------------------------------------------------------

# mes = int(input("Ingrese el mes del año de 1 a 12: "))
# estacion = None #none es que el valor no esta asignado
# if mes ==1 or mes ==2 or mes == 12:
#     estacion = 'Invierno'
# elif mes ==3 or mes ==4 or mes ==5:
#     estacion = 'Primavera'
# elif mes ==6 or mes ==7 or mes ==8:
#     estacion = 'Verano'
# elif mes == 9 or mes == 10 or mes == 11:
#     estacion = 'Otono'
# else:
#     estacion= 'Mes incorrecto'
# print(f'para el mes {mes} la estacion es {estacion}')

# 25 Ejercicio etapas de la vida segun edad -----------------------------------------------------------------------------------------
# edad = int(input("Escribe una edad entre 1 y 30"))
# mensaje = None
# if 0 <= edad <10:
#     mensaje = 'La infancia es increible '
# elif   10 <= edad <20:
#     mensaje = 'Muchos cambios increibles'
# elif 20 <= edad <30:
#     mensaje = 'Eres un adulto'
# else:
#     mensaje= 'Etapa no reconocida'
# print(f'Tu edad es : {edad}, {mensaje}')
# 26 eJERCICIO DE CALIFICACIONES ------------------------------------------------------------------------------------------------------
# nota = int(input("Escribe una Nota entre 0 y 10"))
# mensaje = None
# if 9 <= nota <=10:
#     mensaje = 'A'
# elif   8 <= nota  <9:
#     mensaje = 'B'
# elif 7 <= nota  <8:
#     mensaje = 'C'
# elif 6 <= nota  <7:
#     mensaje = 'D'
# elif 0 <= nota  <7:
#     mensaje = 'F'
# else:
#     mensaje= 'Nota no reconocida'
# print(f'Tu calificacio : {nota}, {mensaje}')
# 27 ------------CICLO WILE --------------repetir acciones hasta que se cumpla la condicion---------------------------
# contador = 0
# while contador <11:
#     print(contador)
#     contador +=1
# else:
#     print('Fin ciclo while')
#
# # 28 mostrar numeros de 5 para atras con while --------------------------------------------------
# i=5
# while i >= 1:
#     print(i)
#     i=i-1
# 29 CICLO FOR-----------------------------------------------------------------------------------------------------
#cadena = 'Hola'
#letra recorreo los elementos dentro de cadena
# for letra in cadena:
#     print(letra)
# else:
#     print("Fin del ciclo")
# 30 como usar BREAk----------------------------recorrer la cadena y tenerse en la primera letra a----------------------------------------

# for letra in 'Holanda':
#     if letra == 'a':
#         print(f'letra encontrada: {letra}')
#         break # rompre el siclo al encontrar la primer letra a
#     else:
#         print('fin del ciclo for')
# 31-----como usar la palabra CONTINUE -imprimiendo los numeros pares que se encunetran dentro de un rango de datos --------------------------------------------------
# for i in range(6):
#     if i % 2 !=0: #si el reciduo de dividir 2 entre i es diferente de 0 osea es impart  pare la busqueda
#         continue # continue con la siguiente orden "permite eecutar la siguiente oteracion dentro del siclo"
#     print(f'valor: {i}') #imprime los pares
# 32  LISTAS -------------conjuntos de elementos---------------------------------------------
#nombres = ['carlos','yesid','diego','valeria'] #definiendo lista de tipo string
#imrpimiendo la lista
# print(nombres[0]) #recupera el primer nombre
# print(nombres[-1]) #recupera el ultimo nombre
# print(nombres) #muestra todos los nombres de la lista
# #imprimiendo un rango de la lista
# print(nombres[0:2]) #recupera el rango de nombres entre 0 y 1 el 2 no lo toma en cuenta
# print(nombres[:3]) #recupera el rango de nombres entre el inicio de la lista hasta el 3r nombre
# print(nombres[2:]) #recupera el rango de nombres entre el el nombre 3 en adelante
# #cambiar valores de la lista
# nombres[3] = 'pedro'
# print(nombres)
# #33 -- Iterara una lista con un for
#
# for iterar in nombres:
#     print(iterar)
# else:
#     print('No existen mas elementos')
# 34-------Mostrar la cantidad de elementos que ytiene una lista ------------------------------------------------------------------
# print( len(nombres))  # con la propiedad len obtenemos la dimencion de la lista
# #45------------------Agergar un nuevo elemento a la lista con la funcion append----------------
# nombres.append('lorenzo') #funion append ahgregar un nuevo elemento
# print(nombres)
# #35---------------insertar un elemento en la lista en un orden especifico-----------Funsion Insert-------------------------------------------
# nombres.insert(0,'charlee')  #inserto a charlee de primeras en la lista
# print(nombres)
# #36-------------------- eliminar un elemento -----------funcion Remove--------------------------------------------------
# nombres.remove('carlos')
# print(nombres)
# # 37-----------Eliminar el ultimo elemento agregado ----funcion pop -----------------------------------------------
# nombres.pop()
# print(nombres)
# # 38 eliminar un elemento en un indice indicado o especifico con la funcion ----Del
# del nombres[2] #borra el nombre que esta en la posicion 3
# print(nombres)
# # 39--------------------ELiminanado todos los elementos de la lista usando la funcion -------clear --
# nombres.clear()
# print(nombres)
#
# #40 ------------eliminar la lista por completo con le funcion Del -----------------------------
# del nombres
# print(nombres)

# ejercicio    iterar numeros de 0 a 10 y mostrar solo los divisibles entre 3 -------
# ejemplo con la funcion Range ----------------------------------------
# for i in range(10):
#     if i %3 == 0:
#         print(i)
# #ejemplo llenando la lista con valores ---------------------------------
# numeros = [0,1,2,3,4,5,6,7,8,9,10]
# for num in numeros:
#     if num % 3 == 0:
#         print(f'Los numeros divisibles de 3 son : {num}')

# 41-------------------------------Tuplas -----------------una tupla es una listade elementos no modificables- o inmutable------------------------------------
# frutas = ('naranja','platano','guayaba',) #delcarando la tupla /siempre hay que dajr una coma al final de el ultimo elemento
# #imprimir la tupla
# print(frutas)
# #saber la dimencion de la tupla
# print(len(frutas))
# #acceder a un elemento especifico  de la tupla
# print(frutas[0])
# #navegacion inversa
# print(frutas[-3])
# #acceder a un rango de
# print(frutas[0:1])
# #recorrer elemento sde l auna tupla
# for recorrer in frutas:
#     print(recorrer, end=' ') #imprineod la tupla en una misma linea con end
# # cambiar el valor de una tupla -- no se puede la unica seria convertir la tupla en una lista
# frutaslista= list(frutas) # nombre de la lista = con version list (nombre de la tupla)
# #modificando el valor
# frutaslista[0] = 'aguacate' #realizando el cambio de componenete
# #convirtiendo la lista en tupla denuevo
# frutas= tuple(frutaslista) #convserion tuple
#
# print(frutas)
#
# #eliminar la tupla completa
# del frutas
#  42 ejercicio de tuplas----------------------Dada la siguiente tupla, crear una lista que sólo incluya los números menor que 5 utilizando un ciclo for: tupla = (13, 1, 8, 3, 2, 5, 8)---------------------------------------------------------------------
# tupla = (13,1,8,3,2,5,8)
# listup = list(tupla)
# for i in listup:
#     if i < 5:
#         print(i)
# #---------------------------------------- otra forma de hacer el ejercicio --------------------------------
# #Dada la siguiente tupla,
# #crear una lista que sólo incluya los números menor que 5
# tupla = (13, 1, 8, 3, 2, 5, 8)
# lista = []
#
# for elemento in tupla:
#     if elemento < 5:
#         lista.append(elemento)
#
# print(lista)


#-43------------------Coleccion SET --- coleccion que no mantiene un orden y no deja tener elementos duplicados no se puede eliminar elementos solo agregar
# Nota listas se usa [], tuplas () y coleccion set  {}
# en las colecciones set no existe el indice 0,1,2,3,4, etc aqui nada tiene orden / sirve para evitar tener datos duplicados en una lista de datos
# planetas = {'marte', 'jupiter', 'venus'}
# print(planetas)
# #conocer la longitud de la coleccion
# print(len(planetas))
# #Revisar si un elemento esta presente en la coleccion
# print('marte'in planetas)
# #---------------------------agregar un elemento en la coleccion set con la funcion add-------------------------------------------
# planetas.add('tierra')
# print(planetas)
# #eilimnar un elemento
# planetas.remove('venus')
# print(planetas)
# #ignorar o descartar un elemento de la coleccion ---------------------funcion discart ----------------------------------------------------
# planetas.discard('jupiter')
# print(planetas)
# #eliminar todos los elementos
# planetas.clear()
# 44 ---------------------Diccionarios ---------------------------------------------------------------------------------------------------
# un diccionario contiene una llave o key y despues el valor
#definiendo un diccionario
# dic = {
#     # llave  ---  contenido o valor
#     'IDE': 'entorno de desarrollo integrado',
#     'OOP': 'Programacion orientada a objetos',
#     'DBMS': 'Systema motor de base de datos'
# }
# print(dic)
# #conocer la lonjitud del diccionario
# print(len(dic))
# #Acceder a un elemento especifico del diccionario
# print(dic['IDE'])
# # ---------------otra forma de acceder o recurar un elemento con la funcion GET----------------------------
# print(dic.get('OOP'))
# # ---modificando valores especificos de una elemento del diccionario
# dic['IDE'] = 'ide cambiado'
# print(dic)
# # recorrer los elementos de un diccionario -------------con la funcion items - la cual permite obtener los elementos separador por lleve y valor ---------------------------------------------------------
# print('Llave  / Valor')
# for llave, valor in dic.items():
#     print(llave,valor)
# # ----------recorrer el diccionario solo para obtener la llave o termino  con la funcion--- keys-------------
#
# for llave in dic.keys():
#     print(llave)
#
# # ---------- obteniendo solo los valores asociados sin las llaves ------con la funcion--- Values----------------------
# for valor in dic.values():
#     print(valor)
#
# # ----------------------------comprobar la exietncia de un elemento en el diccionario--------------------------------
# print('IDE' in dic)
# # ----------------------agregando un elemento al diccionario -------------------------------------------------------
# dic['PK']= 'Llave primaria'
# print(dic)
# # ----------------------------remover un elemento del diccionario
# dic.pop('IDE')
# print(dic)
# # --------------------- eliminar elementos deltro del diccionario ------
# dic.clear()
# print(dic)
# # eliminar por completo la variable de diccionario -----------------------------------
# del  dic
# 45 ------------------------Funciones --------------------------------------------------------
# una funcion es un bloque de codigo  que podemos llamar n cantidad de veces
# definiendo una funcion
#def miFuncion(nombre,apellido): #lo que esta dentro de la funcion son parametros
    #print('mi funcion de hola mundo')
    #print(f'nombre: {nombre}, Apellido: {apellido}')


#ejecutabdo la funcion
#miFuncion('juan','perez') #lo que esta dentro de la funcion al llamarla se llama argumnetos
#  46 usando Returno en las funciones--------------------------------------------------------------------

# def sumar( a, b):
#     return a + b
# #forma normal de usar la funcion
# #resultado = sumar(12, 4) #enviandole argumentos a la funcion y almacenandolo en resultado
# #print(resultado)
# #mostrar los datos operando la funcion dentro de la impresion
# print(f'Resultado suma: {sumar(3,4)}')

# # 47 -------------------- valores por defecto de una funcion ------------------------------------------------------
# def restar(a = 5, b = 2):
#     return a - b
#
# print(f'la resta es {restar()}') #si llena los parentesis de arugumentos estos remplazaran los valores default de los parametros de la funcion restar(10,1)}

#48 --llenando funciones con DUPLAS------------------Argumentos Variables en Funciones -------usando asteriscos------------------------------------------------------
# def listadonombres (*nombres): # con el *  podemos especificar que vamos a llamar a x cantidad de nombre que no sabemos aun
#     for i in nombres:
#         print(i)
# #enviando datos a la variable de nombres en la funcion para imprimir
# listadonombres('carlos','pedro','maria')

#49 -------------------ejercicio de argumntos variables en funciones --------------sumar----------------------------------------
# def listadonombres (*numeros): # con el *  podemos especificar que vamos a llamar a x cantidad de nombre que no sabemos aun
#     suma = 0
#     for i in numeros:
#         suma += i
#     return suma
# #enviando datos a la variable de nombres en la funcion para imprimir
# print(listadonombres(2,2,3))

#50 -------------------ejercicio de argumntos variables en funciones -------------multiplicar-----------------------------------------
# def multiplicarnumeos (*numeros): # con el *  podemos especificar que vamos a llamar a x cantidad de nombre que no sabemos aun
#     m = 1
#     for i in numeros:
#         m *= i
#     return m
# #enviando datos a la variable de nombres en la funcion para imprimir
# print(multiplicarnumeos(2,2,3))

# 51 ---llenando funciones  con DICCIONARIOS------------ FUNCIONES DE TIPO LLAVE - VALOR-----kwamrs---------- ingresar datos variables en forma de diccionarios---------
# def listardiccionario(**dic):
#     for llave, valor in dic.items():
#         print(f'{llave} : {valor}')
#
# #enviando los argumentos
# listardiccionario(NOMBRE=' Carlos', APELLIDO= 'Laverde')

# 52 ---------Distintos dipos de datos como argumentos en al funcion  -------------------------------------------
# def desplegarnombres(nombres):
#     for n in nombres:
#         print(n)
#
# #creando la lista por separado
# nombres= ['michael','yesid','diego','carlos']
# #desplegando nombres o enviando la lista aña funcion para mostrar
# desplegarnombres(nombres) #enviando el listado
# desplegarnombres('charlee') #enviando una cadena de caracteres
# desplegarnombres((2,5,6,12)) #enviando numeros en forma de tupla
# desplegarnombres([1,3,5,7]) #enviando numeros en forma de lista

# 53 --------------- FUNCIONES RECURSIVAS --------------hacer el factorial de un numero---------------------------------------
# def factorial (numero):
#     if numero ==1: #punto de ruptura para no llamar mas a la funcion factorial y evitar hacer mas multiplicaciones
#         return 1
#     else:
#         return  numero * factorial(numero-1) #multiplica el primer valor por el mismo -1  3*2   = 6 ese valor queda
#     # almacenado luego sigue el valor restado  (numero-1) osea 2 -1  = 1 entonces entra en numero ==1 y rompe el proceso
#
# resultado = factorial(3)
# print(resultado)
# 54  --------ejercicio1 mostrar numeros de 5 a 1------- FUNCIONES RECURSIVAS ----------------------------------------------------
# def numerosdemayoramenor (numero):
#     if numero >= 1: #punto de ruptura para no llamar mas a la funcion factorial y evitar hacer mas multiplicaciones
#         print(numero)
#         numerosdemayoramenor(numero-1)

# numerosdemayoramenor(5)
# 55 ------------ejercicio de funciones recrusivas  calcular impuestos -------------------
# def calcular_total(pago, impuesto): #recibe los argumentos para realizar las operaciones
#     # calcular el valor del impuesto
#     calculoimpuesto= pago + pago*(impuesto/100)
#     print(f'el valor de {pago} + {impuesto} % es de: {calculoimpuesto}') #imprimir el valor
# # enviamos los 2 parametros por teclado
# calcular_total(pago = float(input(" ingrese el valor")), impuesto=float(input(" ingrese el impuesto")))

#-------------ejercicio funciones recursivas   calcular grados farenhei y viseversa 2 funciones
#funcion 1
# def celsius_fahrenheit(celsius):
#     fahrenheit  = celsius * 9/5 + 32
#     print(f'{celsius} grados celsius equivalen a {fahrenheit} grados fahrenheit')
# #enviando parametros
# celsius_fahrenheit(celsius = float(input(" ingrese los grados celsius")))
#
# #funcion 2
# def fahrenheit_celsius(fahrenheit):
#     celsius = (fahrenheit-32) * 5/9
#     print(f'{fahrenheit} grados farenheith equivalen a {celsius} grados selsius')
# #enviando parametros
# fahrenheit_celsius(fahrenheit= float(input(" AHora  ingrese los grados farenheith")))
#
# # ------------------otra forma de hacer ele ejercicio imprimiento porfura de las funciones ----------
# # Función que convierte de celsius a fahrenheit
# def celsius_fahrenheit(celsius):
#         return celsius * 9/5 + 32
#
# def fahrenheit_celsius(fahrenheit):
#         return (fahrenheit-32) * 5/9
#
# # Realizamos algunas pruebas de conversión
# celsius = float(input('Proporcione su valor en celsius: '))
# resultado = celsius_fahrenheit(celsius)
# # Los dos puntos después de la variable de resultado dan un formato de 2 digitos flotantes
# print(f'{celsius} C a F: {resultado:.2f}')
# fahrenheit = float(input('Proporcione su valor en fahrenheit: '))
# resultado = fahrenheit_celsius(fahrenheit)
# print(f'{fahrenheit} F a C: {resultado:.2f}')

#  56==================================CLASES Y OBJETOS ==================================================---
















