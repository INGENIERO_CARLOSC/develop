class Color:
    #iniciaizando parametros
    def __init__(self, color):
        self._color =  color #encalsulando el parametros

    @property # accediendo a paremos encapsulado
    def color(self):
        return  self._color

    @color.setter # editando parametro encapsulado
    def color(self,color):
        self._color = color

    def __str__(self): # habilitando en envio de datos a todo el proyecto
        return f'Color [{self._color}]'
