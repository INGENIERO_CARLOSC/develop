from Cuadrado import Cuadrado #importando la clase Cuadrado
from Rectangulo import Rectangulo #importando la clase Cuadrado


#-------------------------------------Objeto y modificacion de Cuadrado----------------------------------
print('Creacion Objeto Cuadrado'.center(50,'-'))
cuadrado1 = Cuadrado(5,'rojo') #valor antiguo asignado
cuadrado1.ancho = 9 #modificando el valor del cuadrado
#accedimos a color de la clase Padre Color y el Area del cuadrado de La clase padre Cuadrado
print(f'El cuadrado de color: {cuadrado1.color} Tiene un area es de {cuadrado1.calcular_area()}')
#-------------------------------------Objeto y modificacion de Rectangulo ----------------------------------
#print(Cuadrado.mro()) #metodo para identificar el orden gerargico de llamada de clases
print('Creacion Objeto Rectangulo'.center(50,'-'))
rectangulo1 = Rectangulo(9,4,'amarillo') #valor antiguo asignado
rectangulo1.ancho = 3 #modificando el valor del ancho del rectangulo
print(f'El Rectangulo de color: {rectangulo1.color} Tiene un area es de {rectangulo1.calcular_area()}')
