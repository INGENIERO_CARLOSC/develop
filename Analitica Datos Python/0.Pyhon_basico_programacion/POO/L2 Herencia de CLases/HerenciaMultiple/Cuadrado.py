from FiguraGeometrica import FiguraGeometrica
from Color import Color

class Cuadrado(FiguraGeometrica,Color):
    def __init__(self,lado, color):
#Nombre de la clase Padre a llamar / metodo init / self con el lado 2 veces por que un cuadrado tiene lados iguales
        FiguraGeometrica.__init__(self, lado, lado)
#Nombre de la clase Padre a llamar / metodo init / self con atributo color
        Color.__init__(self, color)
    # -----------metodo para calcular el area del cuadrado ------------------
    def calcular_area(self):
        return  self.alto * self.ancho #accediendo a los atributos de la clase padre
    #---habilitando el envio de datos a too el proyeto
    def __str__(self):
      return  f'{FiguraGeometrica.__str__(self)} {Color.__str__(self)}'


