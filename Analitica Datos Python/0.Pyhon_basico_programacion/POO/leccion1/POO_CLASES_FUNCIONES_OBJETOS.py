# 1 ========================POO  Clases y objetos ==================================================
# class Persona:
#     #pass # palabra reservada paracrear correctamente la clase indica que la clase no tiene ningun contenido
#     # metodo __init__   inicializa los atributos de una clase es similar a un contructor
#     def __init__(self): #self "uno mismo" toma la referencia del objeto ()
#         #atributos de instancia
#         self.nombre = 'Juan'
#         self.apellido = 'Perez'
#         self.edad = 28
# # creando los objetos
# persona1 = Persona() #objeto
# print(persona1.nombre) #mostrando atributo nombre del obejto persona

# 2 =====================CREACION DE OBJETOS CON ARGUMENTOS ============================================
# class Persona:
#     # metodo __init__   inicializa los atributos de una clase es similar a un contructor
#     def __init__(self, nombre, apellido, edad ): # nombre, apellido, edad es un parametro
#         #atributos / parametros
#         self.nombre = nombre # ejemplo:  nombre es --> parametro * que  esta solito encambio self.nombre es un atributo
#         self.apellido = apellido
#         self.edad = edad
#
#
# # creando el objeto Persona
# persona1 = Persona('Carlos','Cogua',30) #Enviando parametros a el metodo  __init__
# # mostrando el objeto
# print(f'El señor de nombre {persona1.nombre} con el apellido {persona1.apellido} tiene una edad de : {persona1.edad}') #mostrando atributo nombre del obejto persona
#

# ===================CREACION DE MAS OBJETOS EN UNA CLASE ===============================================
# class Persona: #clase o plantilla
# #------iniciando los argumentos -------------------------------------------
#     def __init__(self, nombre, apellido, edad ): # argumentos
#         # izquierda   atributos   /   derecha  parametros
#         self.nombre = nombre # ejemplo:  nombre es --> parametro * que  esta solito encambio self.nombre es un atributo
#         self.apellido = apellido
#         self.edad = edad
#
# # -------------creando objetos----- instancias de la clase o plantilla------------------------------------------
# persona1 = Persona('Carlos','Cogua',30) #Enviando parametros a el metodo  __init__
# persona2 = Persona('Yesid','Rincon',23) #Enviando parametros a el metodo  __init__
# persona3 = Persona('Diego','Melo',24) #Enviando parametros a el metodo  __init__
# # -------------mostrando los objetos---------------------------------------------
# print(f'El señor de nombre {persona1.nombre} con el apellido {persona1.apellido} tiene una edad de : {persona1.edad}') #mostrando atributo nombre del obejto persona
# print(f'El señor de nombre {persona2.nombre} con el apellido {persona2.apellido} tiene una edad de : {persona2.edad}') #mostrando atributo nombre del obejto persona
# print(f'El señor de nombre {persona3.nombre} con el apellido {persona3.apellido} tiene una edad de : {persona3.edad}') #mostrando atributo nombre del obejto persona

#=========MODIFICAR ATRIBUTOS DE UN OBJETO =============================================================================
# class Persona: #clase o plantilla
# #------iniciando los argumentos -------------------------------------------
#     def __init__(self, nombre, apellido, edad ): # argumentos
#         # izquierda   atributos   /   derecha  parametros
#         self.nombre = nombre # ejemplo:  nombre es --> parametro * que  esta solito encambio self.nombre es un atributo
#         self.apellido = apellido
#         self.edad = edad
#
# # -------------creando objetos----- instancias de la clase o plantilla------------------------------------------
# persona1 = Persona('Carlos','Cogua',30) #Enviando parametros a el metodo  __init__
# #-------------------modificando  objeto ----------------------------------------
# persona1.nombre = 'sujerto x'
# persona2 = Persona('Yesid','Rincon',23) #Enviando parametros a el metodo  __init__
# #-------------------modificando  objeto ----------------------------------------
# persona2.nombre = 'sujerto y'
# persona3 = Persona('Diego','Melo',24) #Enviando parametros a el metodo  __init__
# #-------------------modificando  objeto ----------------------------------------
# persona3.nombre = 'sujerto z'
# # -------------mostrando los objetos---------------------------------------------
# print(f'El señor de nombre {persona1.nombre} con el apellido {persona1.apellido} tiene una edad de : {persona1.edad}') #mostrando atributo nombre del obejto persona
# print(f'El señor de nombre {persona2.nombre} con el apellido {persona2.apellido} tiene una edad de : {persona2.edad}') #mostrando atributo nombre del obejto persona
# print(f'El señor de nombre {persona3.nombre} con el apellido {persona3.apellido} tiene una edad de : {persona3.edad}') #mostrando atributo nombre del obejto persona

#============================== METODOS DE INSTANCIA EN LAS CLASES ============================================================
#
# class Persona: #clase o plantilla
# #------iniciando los argumentos -------------------------------------------
#     def __init__(self, nombre, apellido, edad ): # argumentos
#         # izquierda   atributos   /   derecha  parametros
#         self.nombre = nombre # ejemplo:  nombre es --> parametro * que  esta solito encambio self.nombre es un atributo
#         self.apellido = apellido
#         self.edad = edad
# #------------funcion para imprimir los detalles de las personas -----------------------------------------------
#     def mostrar_detalle(self):
#         print(f'{self.nombre} ,{self.apellido} , {self.edad}') #mostrando atributo nombre del obejto persona
#
# # -------------creando objetos----- instancias de la clase o plantilla------------------------------------------
# persona1 = Persona('Carlos','Cogua',30) #Enviando parametros a el metodo  __init__
#
# # ------------imprimiendo datos ---------------------------------------------------------------------------------
# persona1.mostrar_detalle()

#====================================EJERCICIO DE ARITMETICA POO ==============================================================
#
# class Aritmetica:
#     """
#     clase aritmetica para hacer operciones de sumar restar, ult etc
#     """
#     #---funcion para inicializar los argumentos
#     def __init__(self,a,b):
#         self.a = a
#         self.b = b
#     #---funciones para realizar las operaciones aritmeticas
#     def sumar(self):
#         return self.a + self.b
#
#     def restar(self):
#         return self.a - self.b
#
#     def multiplicar(self):
#         return self.a * self.b
#
#     def divir(self):
#         return self.a / self.b
# # objetos -------
# aritmetica1 =Aritmetica(5,3)
# #imprimiendo ------
# print(aritmetica1.sumar())
# print(aritmetica1.restar())
# print(aritmetica1.multiplicar())
# print(f' {aritmetica1.divir():.2f}')
# ========Pidiendo datos por teclado================================EJERCICIO POO AREA DE UN RECTANGULO =============================
# class Rectangulo:
#     #iniciliazando los argumentos
#     def __init__(self, base, altura):
#         self.base = base
#         self.altura = altura
#     #--ejecutando las operaciones--
#     def area(self):
#         return self.base * self.altura
# #pedir los valores por teclado al usuario
# base= int(input('Ingresa la base'))
# altura= int(input('Ingresa la altura'))
# # objeto
# calculando = Rectangulo(base,altura)
# # imprimiendo el resultao
# print(f'El area del rectangulo es {calculando.area()}')
# =======Pidiendo datos al usuario =========== EJERCICIO DEL CUBO =============================================================
# class Cubo:
#     def __init__(self, ancho, alto, profundidad):
#         self.ancho =  ancho
#         self.alto =  alto
#         self.profundidad = profundidad
#
#     def Volumen(self):
#         return  self.ancho * self.alto * self.profundidad
#
# #capturndo datos por teclado
# ancho =  int(input('Ingrese porfavor el ancho del cubo'))
# alto =  int(input('Ingrese porfavor el alto del cubo'))
# profundidad =  int(input('Ingrese porfavor la profundidad del cubo'))
# # creando el objeto y enviando parametros  a __init__
# CalculandoVolumen = Cubo(ancho, alto, profundidad)
#
# print(f'El volumen del cubo es de {CalculandoVolumen.Volumen()}')

# ===========TUPLAS Y DICCIONARIOS======================ROBUSTECIOENDO EL METODO __init__ =============pasar tuplas y diccionarios=================================
# class Persona:
#     # para pasar tupla de valores usamos la propiedad *args , si queremos usar un diccionario usamos  **kwargs
#     def __init__(self, nombre, apellido, edad, *tupla, **diccionario ): # nombre, apellido, edad es un parametro
#         #atributos / parametros
#         self.nombre = nombre # ejemplo:  nombre es --> parametro * que  esta solito encambio self.nombre es un atributo
#         self.apellido = apellido
#         self.edad = edad
#         self.tupla = tupla # *args
#         self.diccionario = diccionario # *kwargs
#
#
# # creando el objeto Persona
# persona1 = Persona('Carlos','Cogua',30,'valores de tupla',122,345,1, telefono='3133485269', direccion= 'cll 33' ) #Enviando parametros a el metodo  __init__
# # mostrando el objeto
# print(f'El señor de nombre {persona1.nombre} con el apellido {persona1.apellido} tiene una edad de : {persona1.edad} -  {persona1.tupla}- dicionario: {persona1.diccionario}') #mostrando atributo nombre del obejto persona
# ======GET Y SET =====================================ENCAPSULAMIENTO POO =================================================================
# class Persona:
#     # Iniciando los argumentos con __init__
#     def __init__(self, nombre, apellido, edad): # nombre, apellido, edad es un parametro
#         #atributos / parametros
#         self.__nombre = nombre # el guion bajo indica que la informacion del argumento esta encapsulada  __nombre y solo puede ser sobreescrito dentro de la funcion o funciones
#         self.apellido = apellido
#         self.edad = edad
#     #----Decorador para restringir el acceso a argumentos encapsulados del metodo  __init__
#     @property
#     #----Metodo para recuperar los valores de los argumentos
#     def nombre_get(self):
#         return self.__nombre
#     #----Decorador para el  modificando el atributo nombre dando permisos  con el decorador
#     @nombre_get.setter
#     #--------metodo para modificar un nombre
#     def nombre_set(self,nombre):
#         self.__nombre = nombre
#
# # creando el objeto Persona
# persona1 = Persona('Carlos','Cogua',30) #Enviando parametros a el metodo  __init__
# print(persona1.nombre_get) #mostrando objeto sin modificaciones
#
# #modificando el argumento nombre por medio de metodo SET
# persona1.nombre_set = 'Mario benedeti' #modificando indirectamente con el metodo set
# print(f' El nombre : Carlos ha sido cambiado por : {persona1.nombre_get}, con los apellido {persona1.apellido} y edad: {persona1.edad}')
# ====================================ENCAPSULANDO TODOS LOS ATRIBUTOS  DE LA CLASE ======================================================
# class Persona:
#     # Iniciando los argumentos con __init__
#     def __init__(self, nombre, apellido, edad): # nombre, apellido, edad es un parametro
#         #atributos / parametros
#         self.__nombre = nombre # el guion bajo indica que la informacion del argumento esta encapsulada  __nombre y solo puede ser sobreescrito dentro de la funcion o funciones
#         self.__apellido = apellido
#         self.__edad = edad
#     #------------------------ACCEDIENDO Y MODIFICANDO EL NOMBRE-----------------------------------------------------------------------
#     #----Decorador para restringir el acceso a argumentos encapsulados del metodo  __init__
#     @property
#     #----Metodo para recuperar los valores de los argumentos
#     def nombre_get(self):
#         return self.__nombre
#     #----Decorador para el  modificando el atributo nombre dando permisos  con el decorador
#     @nombre_get.setter
#     #--------metodo para modificar un nombre
#     def nombre_set(self,nombre):
#         self.__nombre = nombre
#
#     #----------------ACCEDIENDO Y MODIFICANDO EL APELLIDO------------------------------------------------------
#     # ----Decorador para restringir el acceso a argumentos encapsulados del metodo  __init__
#     @property
#     # ----Metodo para recuperar los valores de los argumentos
#     def apellido_get(self):
#         return self.__apellido
#     # ----Decorador para el  modificando el atributo nombre dando permisos  con el decorador
#     @apellido_get.setter
#     # --------metodo para modificar un nombre
#     def apellido_set(self, apellido):
#         self.__apellido = apellido
#
#     #----------------------------ACCEDIENDO Y MODIFICANDO LA EDAD ----------------------------------------------
#     # ----Decorador para restringir el acceso a argumentos encapsulados del metodo  __init__
#     @property
#     # ----Metodo para recuperar los valores de los argumentos
#     def edad_get(self):
#         return self.__edad
#     # ----Decorador para el  modificando el atributo nombre dando permisos  con el decorador
#     @edad_get.setter
#     # --------metodo para modificar un nombre
#     def edad_set(self, edad):
#         self.__edad = edad
#
# # creando el objeto Persona con los primeros datos
# persona1 = Persona('Carlos','Cogua',30) #Enviando parametros a el metodo  __init__
# #mostrando los primeros datos enviados
# print(f' Datos originales {persona1.nombre_get} , {persona1.apellido_get} , {persona1.edad_get}') #mostrando objeto sin modificaciones
#
# #---------------modificando los argumentos -------------------------------
# persona1.nombre_set = 'Mario ' #modificando indirectamente con el metodo set
# persona1.apellido_set = 'Balaguera' #modificando indirectamente con el metodo set
# persona1.edad_set = '18' #modificando indirectamente con el metodo set
# #-----------------mostrando los datos actuales modificados ---------------------------
# print(f' Datos originales modificados: {persona1.nombre_get}, {persona1.apellido_get},  {persona1.edad_get}')
#============================================USO DE MODULOS EN CLASES =======================================================================

