#--------------el mismo ejercicio pero validando el if dentro de la funcion
def promedionotas(dicNotas: dict):
    sumatoria = 0
    sumatoria += dicNotas["nota1"]
    sumatoria += dicNotas["nota2"]
    sumatoria += dicNotas["nota3"]
    sumatoria += dicNotas["nota4"]
    promedio = round(sumatoria/4, 2)
    if promedio >= 3.0:
        estado = "aprobado"
    else:
        estado = "reprobado"
    definitivo = {
        "prom":promedio,
        "estado":estado
    }
    return definitivo
 
dicNotas = {}
dicNotas["nota1"] = float(input("ingrese la nota 1"))
dicNotas["nota2"] = float(input("ingrese la nota 2"))
dicNotas["nota3"] = float(input("ingrese la nota 3"))
dicNotas["nota4"] = float(input("ingrese la nota 4"))

total = promedionotas(dicNotas)
print("El promedio total es: " + str(total["prom"]) + " usted ha sido " + total["estado"])

#--- metodo del() ---- elimina un valor especifico del diccionario 
del dicNotas["nota2"] # elimina simplemente y no devuelve el valor eliminado
print(dicNotas)

# ---- funcion len()-- cuantos elementos o llaves tiene un diccionario
print(dicNotas.len())
# -----------metodo update() -----------agrega nuevos elementos clave valor partir de otro diccionario lista o tupla-----
dicprueba={
    "nota5": 4.8
}
print(dicNotas.update(dicprueba)) #agrego la nota 5 de dicprueba en dic notas

#---- metodo pop() --- remueve o elimina la clave del diccionario especifico 
print(dicNotas.pop("nota1")) #muestra el valor que elimino y el diccionario sin el valor
print(dicNotas) 

#-----metodo items() ----- devuelve una lista con la clave y valor del diccionario como tupla
print(dicNotas.items())

#--metodo get --------sirve para devolver un valor de una clave en especifico--------
if dicNotas.get("nota3") == None:
    print("la clave no existe")
else:
    print(dicNotas.get("nota3"))

# ---- metodo fromkeys --- crea un nuevo diccionario con las claves que salen de un tipo de secuencia listas, tuplas
tupla = ("nota1","nota2","nota3") #-- las tuplas solo se hacen con parentesis
dic3 = dict.fromkeys(tupla) #perimte alamacenar la clave y el dato queda none
print(dic3)
#---- copiar el contenido de un diccionario al otro
dic2={}
dic2 = dicNotas.copy()
print(dic2)

# -- metodo que borrar todo el contenido del diccionario
dicNotas.clear()
print(dicNotas)