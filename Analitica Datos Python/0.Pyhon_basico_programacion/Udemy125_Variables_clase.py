#Variables de clase
class Miclass:
    #Atributos de clases
    variables_clase = 'valor variable clase' # se comparte con la misma clase y todos los objetos

    #efiniendo variables de instancia
    def __init__(self, variable_instancia):
        self.variable_instancia = variable_instancia

print(Miclass.variables_clase) #accediendo al variable de clase

#accediendo a la variable de instancia con un objeto
Miclass = Miclass('Valor variable de instancia')
print(Miclass.variable_instancia)

#ccediendo a la variable de clase tambien con un objeto
print(Miclass.variables_clase)