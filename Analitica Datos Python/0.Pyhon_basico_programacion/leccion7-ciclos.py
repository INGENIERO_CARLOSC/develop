# --------actualizacion de variables----
# x=0 #siempre se inicializa la variable en 0
# x = x +1
# print(x)

#---- ciclo while-- mientras----
# n= 5 
# while n>0:
#     print(n)
#     n -= 1 #inicialmente n es 5 y en cada iteracion va dismunuyendo 1
#     #n = n-1
# print(" fin del siclo ----despegue")
# -------------------factorial de un numero ------------------------------
# def factorial(num: int)->int:
#     factori =1
#     num_actual =2

#     while num_actual <= num:
#         factori *= num_actual
#         num_actual += 1
#     return factori

# numero = int(input("ingrese el numero "))
# print("el factorial del numero"+ str(numero) + " es "+ str(factorial(numero)))

#------------------eventos manipulados con while -------------------------------------------------
#---- calcular el promedio de x canridad de notas de un estudiante ----
# promedio, total, contar = 0.0,0,0 # delclarabdo variables seguidas
# nota =float(input(" ingrese la dota del estudiante "))
# while nota != -1:
#     total += nota #sumatoria de las notas
#     contar +=1 #contador para saber cuantas notas ingreso
#     nota= float(input("ingrese la nota del estudiante"))
#     if contar == 1: #0 y 1 
#         nota = -1
# promedio = total/contar
# print("El promedio de notas del grado escolar es  " +str(promedio))
# --------------------while con else ---------------------
# promedio, total, contar = 0.0,0,0 # delclarabdo variables seguidas
# nota =float(input(" ingrese la dota del estudiante "))
# while nota != -1:
#     total += nota #sumatoria de las notas
#     contar +=1 #contador para saber cuantas notas ingreso
#     nota= float(input("ingrese la nota del estudiante"))
#     if contar == 1: #0 y 1 
#         nota = -1
#         #-----------sentencias utiles para wihle ------------------------
#     if contar >= 3: #0 y 1 
#         break # es otra forma de interrumpir una instruccion en un while
# else:
#     promedio = total/contar
#     print("El promedio de notas del grado escolar es  " +str(promedio))

#---------ciclo for -----------------------------------
# #---------- incrementacion---
# for x in range(1,11,2):
#     print("estamos en la iteracion: "+ str(x))
# #--- decrementar --
# for x in range(10,0,-2):
#     print("estamos en la decrementacion: "+ str(x))
# #-----interrumpir un for con break --
# for x in range(10,0,-2):
#     print("estamos en la decrementacion: "+ str(x))
#     if x== 4:
#         break
#--------------- iterando cadenas de texto con for -----------

# oracion = 'La programacion es buena para el desarollo mental'
# frases = oracion.split() # convierte la cadena string en una lista con cada una de las palabras
# print("la oracion analizada es: ", oracion)
# print(frases)

# for palabra in range(len(frases)):
#     print("palabra: {0}, en la frase su posicion es: {1}".format(frases[palabra],palabra))

#-------uso de ciclo for con diccionarios -----------------------

datos_basicos = {
    "nombre":"carlos",
    "apellido":"cogua",
    "estado_civil":"soltero"
}
#-- convirtiendo el diccionario en una lista 
clave = datos_basicos.keys() #almacena la clave del diccionario en esta variable
valor = datos_basicos.values() #almacena el valor de el diccionario en Valor
cantidad_datos= datos_basicos.items() #saca la longitud del diccionario

for clave , valor in cantidad_datos:
    print(clave+ ": "+valor)

#---------------------------otro ejemplp
