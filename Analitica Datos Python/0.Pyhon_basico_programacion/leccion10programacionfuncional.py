#-----programacion funcional ------------------
# def suma(val1 = 0, val2  = 0):
#     return val1 + val2

# def operacion (contenido, val1=0, val2=0):
#     return contenido(val1,val2)


# variable= suma
# resultado = operacion(variable, 10, 20)
# print(resultado)

#----------------otro ejemplo ----
#--- funcion para hacer las operaciones
def definir_operador(operador: str):
    if operador == "+":
        def suma(val1: int = 0, val2: int =0):
            return val1+val2
        return suma
    elif operador== "-":
        def resta(val1: int = 0, val2: int =0):
            return val1-val2
        return resta
    elif operador== "*":
        def mult(val1: int = 0, val2: int =0):
            return val1*val2
        return mult

#---funcion para enviar datos a  def definir_operador
def operacion(funcion, val1: int=0, val2: int=0):
    return funcion(val1,val2)
#----2 funciones------enviando parametros----------
var_func = definir_operador("+") #ejecutando la funcion y enviando el parametro
resultado = operacion(var_func,20,30)#-- envia los valores para hacer la operacion