# =============Archivos de texto =====================================
#--ESTRCUTURAS DE FICHEROS CON TEXTO PLANO---------------------------
# ====================================================================
#---Estructura en textos planos 


'''
open = Abrir el archivo que quiero trabajar
a = append  continuar editando
w =  escribir o sobreescribir dentro del archivo
close =  cerrar el archivo

'''

fichero = open("prueba.txt", "w") # w es write y se usa para modificar el contenido / open es la creacion del archivo
fichero.write("Esto es\n")
fichero.write("Una\n")
fichero.write("Prueba\n")
fichero.close()

# Editando el archivo  reemplezasa todo el contenido 

fichero = open("prueba.txt", "w") # w es write y se usa para modificar el contenido / open es la creacion del archivo
fichero.write("Esto es\n")
fichero.write("Otra\n")
fichero.write("Prueba, Diferente\n")
fichero.close()

#Agregango contenido al fichero sin obrrar su contenido

fichero = open("prueba.txt", "a") # a es append y permite agregar info sin afectar el contenido ultimo
fichero.write("Hecha por el progrmadorch\n")
fichero.close()

# =============Archivos de texto =====================================
#--LECTURA  DE FICHEROS CON TEXTO PLANO---------------------------
# ====================================================================

'''
r = lectura
r+ =lectura y escritura
'''
print("")
print("LECTURA  DE FICHEROS CON TEXTO PLANO")
fichero = open("prueba.txt", "r") # w es write y se usa para modificar el contenido / open es la creacion del archivo
contenido = fichero.read() #almaceno la lectura en una variable para poder ver todo el contenido
fichero.close()
print(contenido)

# ----Otra forma de leer el archivo linea x linea----
print()
print(" ----Otra forma de leer el archivo linea x linea----")
fichero = open("prueba.txt", "r")
for linea in fichero:
    print(linea)
fichero.close()


