# Recorrer el lista1 y encontrar el elemento que contenga la letra M despues agregarlo a la lista nueva 
from xml.dom.minidom import Element

print("# Recorrer el lista1 y encontrar el elemento que contenga la letra M despues agregarlo a la lista nueva  ")
lista1 = ["paola","nelsy","Mariana","Carlos","David"]
listaM = []
for elementos in lista1 : #recorriendo los elementos de lista1 en la variable auxiliar elementos
    if elementos[0] == "M": #Validadndo la existencia del elemento M en el indice 0 de cada elemento de la lista
        listaM.append(elementos.upper()) #agregando el elemento a la listaM En Mayuscula con Upper

print(listaM)
print()

#------------------------Esta es la mas aconsejable para usar for con listas---------------------------------------------------------------------------------


print("#OTra Forma mas facil de hacer ele ejercicio anterior pero con la letra l en el indice 2")
#OTra Forma mas facil de hacer ele ejercicio anterior
lista1 = ["paola","nelsy","Mariana","Carlos","David"]
listaM = []
listaM = [elementos.upper() for elementos in lista1 if elementos[2]== "l"]
print(listaM)

#------Otra forma de hacer compresiones --------------------------------------
print("#------Otra forma de hacer compresiones --------------------------------------")
#pasa a mayusculas y muestra el nombre con la letra m como true y los demas falsos 
dic ={elementos.upper(): elementos[0] == "M" for elementos in lista1 } 
print(dic)

#-------comprenciones y eliminando duplicados de una lista ------convertido a conjunto-------------------
listaD= ["paola","nelsy","paola","Carlos","Carlos"]
d = { duplicado for duplicado in listaD }
print(d)




