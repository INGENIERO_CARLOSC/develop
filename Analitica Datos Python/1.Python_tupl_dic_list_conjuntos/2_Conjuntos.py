#=====================================================================
# -----------------------CONJUNTOS-------SET---------------------------
#======================================================================

# secuencias multidimencionales de unico nivel , mutables , de tamaño variable , desordenada, sus elementos son valores o secuencias, sus elementos son unicos no muestra repetidos
#creacionde un conjunto
print("#creacionde un conjunto")

set_1 = {1,2,3,4,5}
print(set_1)

# conversiones casting a conjunto o series a conjuntos
print("# conversiones casting a conjunto o series a conjuntos")
lis = [2,4,7,9]
set_1 = set(lis)
print(set_1)

# union de conjuntos 
print("# union de conjuntos")
set_1 = {1,2,3,4,5,6}
set_2 = {9,10,11,12,5,6}
print(set_1 | set_2)

#insercion de conjuntos que un elemento es en uno y en otro conjunto 
print("#insercion de conjuntos que un elemento es en uno y en otro conjunto  ")

print(set_1 & set_2)

# diferencia de conjuntos Resta de conjuntos lo que no esta en el otro conjunto
print("# diferencia de conjuntos Resta de conjuntos lo que no esta en el otro conjunto principal y en el otro no")
print(set_1 - set_2)

# diferencia cimetrica  de conjuntos unoe los elementos que no tienen en comun los dos conjuntos
print("# diferencia cimetrica  de conjuntos unoe los elementos que no tienen en comun los dos conjuntos")
print(set_1 ^ set_2)
print(set_2 ^ set_1)

# Agregando elementos a un conjunto
print("# Agregando elementos a un conjunto")
con = {5,67,3,2}
print("conjunto original ")
print(con)
print("agregando elemento ")
con.add(69)
print(con)
# Comprobando la existencia de un elemento dentro del conjunto
print("# Comprobando la existencia de un elemento dentro del conjunto")
con = {5,67,3,2}
print(60 in con) 

# Comprobacion de subconjunto 
print("# Comprobacion de subconjunto ")
set_1 = {1,2,3}
set_2 ={1,2,3,4,5}
print(set_1 >= set_2)

#Comprobacion de super conjunto si el conjunto 1 tiene elementos del 2
print("#Comprobacion de super conjunto")
set_1 = {1,2,3}
set_2 ={1,2,3,4,5}
print(set_2 <= set_1)

#Eliminacion de un elemento del conjunto por valor 
print("#Eliminacion de un elemento del conjunto por valor ")

# Eliminar elementos de un conjunto
print(set_2)
set_2.remove(3) #cuento estoy seguro del valor que deseo eliminar si no existe el elemento da error
print(set_2)
set_2.discard(6) #Cuando no estoy seguro del valor que quiero eliminar No da error si no existe

#Eliminar todos los elementos del conjunto
set_2.clear()
print(set_2)