#fUNCION FRAGMENTO DE CODIGO PARA REUTILIZAR MUCHAS VECES
def mi_funcion (lista, inicial, a_mayuscula):
    list_out = [] #lista vacia
    for elemento in lista: # recorre los elementos de la lista dentro de la viable elemento
        if elemento[0] == inicial: # filtra o valida si el indicie cero de cada elemento es igua a inicial o letra " " que enviamos en las impresiones
            if a_mayuscula: #valida si en el impresion le enviamos la orden de tru o false para convertir el valor de mayucula o viseversa
                list_out.append(elemento.upper()) #convierte el valor a mayuscula
            else: 
                list_out.append(elemento.lower()) #convierte el valor a minuscula
    return list_out
#-----------termina la funcion -----------------------------------------

lista = ["uno","dos","tres","cuatro","cinco","seis"]


#imprimiendo y enviando los parametros
# -----------(lista, inicial, s_mayuscula)---------------------
print(mi_funcion(lista, "c", True)) #muestra concidencias en mayuculas
print(mi_funcion(lista, "u", False)) #muestra coincidencias en minusculas
print(mi_funcion(lista, "t", True))
print()



# ===========================================================================
#---------otro ejempl pero con argumentos opciones -----------------
# ===========================================================================
print("--------otro ejempl pero con argumentos opcionales ----------------- ")
#-------------------------------a_mayuscula =True  es el argumento por defecto como verdadero si no se coloca una opcion en el print
def mi_funcion (lista, inicial, a_mayuscula =True):
    list_out = [] #lista vacia
    for elemento in lista: # recorre los elementos de la lista dentro de la viable elemento
        if elemento[0] == inicial: # filtra o valida si el indicie cero de cada elemento es igua a inicial o letra " " que enviamos en las impresiones
            if a_mayuscula: #valida si en el impresion le enviamos la orden de tru o false para convertir el valor de mayucula o viseversa
                list_out.append(elemento.upper()) #convierte el valor a mayuscula
            else: 
                list_out.append(elemento.lower()) #convierte el valor a minuscula
    return list_out


lista = ["uno","dos","tres","cuatro","cinco","seis"]


#imprimiendo y enviando los parametros
# -----------(lista, inicial, s_mayuscula)---------------------
print(mi_funcion(lista, "c", )) #si no es pecifico en la 3ra variable que es true o false 
print(mi_funcion(lista, "u", False)) #muestra coincidencias en minusculas
print(mi_funcion(lista, "t", ))
print()

#=============================EJEMPLO 3 ===================================
#------------RETORNO DE MULTIPLES ELEMENTOS---------EN UNA FUNCION --------
#==========================================================================
print("-----------RETORNO DE MULTIPLES ELEMENTOS---------EN UNA FUNCION --------")
def mi_funcion (lista, inicial, a_mayuscula =True):
    list_out = [] #lista vacia
    for elementof in lista: # recorre los elementos de la lista dentro de la viable elemento
        if elementof[0] == inicial: # filtra o valida si el indicie cero de cada elemento es igua a inicial o letra " " que enviamos en las impresiones
            if a_mayuscula: #valida si en el impresion le enviamos la orden de tru o false para convertir el valor de mayucula o viseversa
                list_out.append(elementof.upper()) #convierte el valor a mayuscula
            else: 
                list_out.append(elementof.lower()) #convierte el valor a minuscula
    return(len(list_out), list_out)  # retorna 2 valores  en una tupla la longitud del valor y el valor 


lista = ["uno","dos","tres","cuatro","cinco","seis"]

#alamacenando los dos valores del retun de la funcion en num_elementos (len(list_out)), elementos (list_out)
num_elementos, elementos = mi_funcion(lista, "c") # enviando los paremetros y almacenandolos la mayuscula esta por default si no se especifica el ,fase

# recorremos los elementos desde el indice 0 hasta el final en este caso letra por letra
for i in range(0,num_elementos ):
    print(elementos[i]) #mostrado los elementos de la lista que empiezan con letra c en forma mayuscula

