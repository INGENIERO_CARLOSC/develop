#CON TODO ESTO ESVITAMOS PARAR LA COMPILACION POR UN ERROR

# CONTROL DE ERRORES / como no puede convertir la cadena a float puedo hacer que me muestre un mensaje de error
# en lugar de parar la conmpilacion 
try:
    cadena = "123.5Hola"
    numero = float(cadena)
except:
    print("se prododujo un error")


#=======================TIPOS DE EXCEPCIONES===================================================
# otra forma de mostrar el error original y el programadohacer exepciones 
print()
print("# otra forma de mostrar el error original y el programadohacer exepciones")
try:
    cadena = "123.5Hola"
    numero = float(cadena)
except Exception as e: #Exception as e es el error original DE PYTHON mostrado en consola
    print(e)
    print("se prododujo un error")#error programado

#REALIZAR UN CONTROL DETALLADO POR DISTINTO DTIPOS DE ERRORES
print()
print(" REALIZAR UN CONTROL DETALLADO POR DISTINTO DTIPOS DE ERRORES ")

try:
    cadena = "123.5Hola"
    numero = float(cadena)
except RuntimeError as e: #Erroes de cualquier tipo durante la ejecucion
    print(e)
    print("se prododujo un error de ejecucion")
except ValueError as e: #valores numericos incorrecto u operaciones o usando tipo de dato numerico incorrecto
    print(e)
    print("se prododujo un error Formato numerico incorrecto")
except Exception as e:
    print(e)
    print("se prododujo un error desconocido")

# ======================Propiedad finaly ==============================================
print()
print("Propiedad finally para realizar una tarea especifica despues del error")
try:
    cadena = "123.5Hola"
    numero = float(cadena)
except:    
    print("se prododujo un error de ejecucion")
finally: # ordena que como el numero no se puede convertir a decimal que le concatene el mensaje adios
    cadena+= " Adios."
print(cadena)