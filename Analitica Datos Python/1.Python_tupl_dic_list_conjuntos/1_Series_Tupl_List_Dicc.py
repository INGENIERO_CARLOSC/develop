#una forma de asignar valores a las variables

a,b = "carlos", "cogua"
#usando la barra invertida para el contenido de ua variable en otro renglon
c=  ", Este es un mensaje muy" + \
    " largo"
#usando las 3 comillas para los strings
d = ''',Otro mensaje suprememente 
Larguisimo
'''
e = a+b #concatenando strings
print(a,b,c,d,e)

#modificacion manual de tipos de variables
'''
print(str(entero))
print(int(decimal))
print(float(boolean))

print(float(cadema))
'''
#====================================================
# Secuencuencias 
#====================================================
# Secuencuencias (listado unidimencional odenado de valores que puede ser de cualquier tipo) Tuplas inmutables, Listas mutables, Cadenas de caracteres
tuple_1 = (1,2,3,4) #no se puede modifica su contenido
list_1 = [1,2,3,4]# si permite modificar su contenido
str_1 = "esto es una cadena"

# convertir un tipo a otro de sucencia hacer cambios entre uno y otro 

tuple2 = tuple(list_1) #almacena en una nueva tupla el contenido del list_1
list_2 = list(tuple_1) #almacena en una lista nueva el contenido de tuple_1
tuple_3 = tuple(str_1) #almacena en la nueva variable el contenido de un string en forma de tupla

print(tuple2)
print(list_2)
print(tuple_3)

#Concatenacion de secuencias 
print("#Concatenacion de secuencias ")
tuplas= tuple_1 +tuple_3 
listas = list_1 + list_2

print(tuplas)
print(listas)

#comprobacion si un elemento existe en la secuencia 
print("#comprobacion si un elemento existe en la secuencia")
T1 =(1,2,3,4)
L1 = [1,2,3,4]
C = "esto es una cadena"

print( 4 in T1) # cuatro esta el la tupla?
print( 5 not in T1) # 5 no esta no la tupla
print( 4 in L1) # 4 esta en la lista?
print( "e" in C) # la letra e esta en la cadena?
print( "z" not in C) # le letrza z no esta en la cadena

# seleccion de Slicind de secuencias 
'''
notacion de secuencia [a:b:c]
a: indice del primer elemento a extrar
b: indice del primer elemento que no se extrae 
c: tamaño del salto a aplicar en la extraccion si no se escribe se asume que es 1
'''
print("tupla T1")
print(T1[:]) #muestra todos los elementos
print(T1[2:]) #muestra apartir del indice 2 en adelante
print("lista L1")
print(L1[:3])
print(L1[2:3]) #muestra lo que hay entre el indice 2 y 3
print(L1[::-1]) #invierte el orden
print("Cadenas de caracteres")
print(C[:-3]) # muestra desde la psocion -3 
print(C[::2]) # muestra todo pero de 2 en dos caracteres

''' 
https://www.youtube.com/watch?v=MyeCc-Xwi5g&t=98s
1:16:16
'''
#extraccion de variables --------------

Tupla = (1,2,3,4,5)
var1,var2,var3,var4,var5 = Tupla
# Reasigna el valor de la tupla pero dentro de las variables independientes
print(var1)
print(var2)
print(var3)
print(var4)
print(var5)

#--------Mezcla ordenada de sucuencias ---
Tuplan =(1,2,3,4,5)
Listadon = ["uno","dos","tres","cuatro","cinco"]
TupaListada = zip(Tuplan,Listadon)
print(list(TupaListada)) #coloco los valores dentro de la tupla y dentro de la lista

#-----Creacion de secuencias  numericas  con range

print("#-----Creacion de secuencias  numericas ")

List_1 = range(6)
print(List_1) # SOLO MUESTRA EL RANGO
print(list(List_1)) #MUESTRA LOS VALORES EN UNA LISTA

List2 = range(5,10) #muestra el intervalo de 4 primeros numeros
print(list(list_2))

List3 = range(0,100,2) #muestra los numeros de 2 en dos desde el cero hasta el 98
print(list(List3))

#-- agregar un elemento al final de la lista 
print("#-- agregar un elemento al final de la lista ")
Lis = ["uno","dos"]
Lis.append("tres")
print(Lis)


#Insercion de un elemento en una posicion especifica
print("#Insercion de un elemento en una posicion especifica")

Lis = ["uno"," tres"]
Lis.insert(1,"dos")
print(Lis)
#Asignacion de valor a un Slice para listas

Lis = [1,2,3,4,5] # indices 0123456
Lis[2:4] = [6,7] # los indices 2 al 4  ose el numero en el indice 2 y 3 = 3,4 reemplazados por 6,7
print(Lis) 
Lis[2:5] = [3,4] #elomino el dato 5
print(Lis)

#recuperar un elemento y eliminarlo 
print("#recuperar un elemento y eliminarlo ")
Lis = [1,2,3,4,5]
print("El elemento a eliminar es: ",Lis.pop(3)) #muestra el elemento a eliminar 
print(Lis)# imprimiendo elemento actualizado

#Eliminacion del primer elemento coincidente
print("#Eliminacion del primer elemento coincidente")
Lis = [1,2,3,[4,5]]
print(Lis)
Lis.remove([4,5])
print(Lis)

#Eliminacion de elementos 
print("#Eliminacion de elementos ")
Lis = [1,2,3,4,5]
print(Lis)
del Lis[3] #elimino al elemento en el indice 3
print(Lis)

#ordenamiento de elementos
print("#ordenamiento de elementos")
Lis = [1,5,4,2,3]
Lis.sort() #ordenando elementos
print(Lis)
print("#ordenando elementos pero en reversa ")
Lis.sort(reverse= True) #ordenando elementos pero en reversa 
print(Lis)

#recuperacion inversa de elementos
print("#recuperacion inversa de elementos")
Lis = [1,4,3,6,2,5]
Lis.reverse()
print(Lis)

#Fusionar listas
print("#Fusionar listas")
lis1 = [1,1,1,1]
lis2 = [2,2,2,2]
lis1.extend(lis2) #fucionando listas
print(lis1)

print("Otra forma de fusionar")
l1 =[2,3]
l2=[4,4,4,4,4]
lisFinal = l1 +l2
print(lisFinal)

#-----cadenas-------------
# Strings  convertir minusculas a mayusculas 
print("# Strings  convertir minusculas a mayusculas ")

cadena = "esto es una prueba"
print(cadena.upper())
cadena2 = "ESTO ES UNA PRUEBA"
print(cadena2.lower())

# Segmentacion por caracteres con Cadenas

print(cadena.split(" ")) # segmento o separo el elmento string en varios elementos al estar separados por un espacio

#reemplazando en cadenas 
print("#reemplazando en cadenas ")
Ca = "esta es una prueba"
print(Ca.replace("una", "cambiaso")) #cambia el valor 1 vez
print(Ca.replace("una", "cambiaso",1)) #cambia el valor 1 vez

#------------Diccionarios-------------------
#creacion de u diccionario
print("#creacion de u diccionario")

Dic = {
    "clave_1": 1,
    "clave_2": 2,
    "clave_3": 3,
}

print(Dic)

# otro ejemplo de diccionarios
Dic2 = {10:1, 2:2}
print(Dic2)

#creacion de diccionarios anidados
print("#creacion de diccionarios anidados")

DicA= {
    "clave1":1,
    "clave2":{"clave_nueva": [1,2,3], # diccionario dentro de otro diccionario y una lista dentro del diccionario interno
    "clave23": "prueba"
    }
}
print(DicA)

#creacion de diccionario desde una tupla y una lista y uniendo los valores
print("#creacion de diccionario desde una tupla ")
tupla = (1,2,3,4)
lista = ["uno","dos","tres","cuatro"]
diccionario = dict(zip(tupla,lista))
print(diccionario)

#union de diccionarios 

dicc = {
    "nombre": "carlos",
    "apellido": "cogua"
}
diccionario.update(dicc)
print(diccionario)

#Insercion o modificacion de elementos en un diccionario
print("#Insercion o modificacion de elementos en un diccionario")

dicP = {
    "one":1,
    "two":2
}

#agregando clave y valor nuevas
dicP["three"]= 3
dicP["four"] = "Cuatro"

print(dicP)

# recuperacion y eliminacion de un elemento con clave valor
print("# recuperacion y eliminacion de un elemento con clave valor")
print(dicP.pop("three"))
print(dicP)

#comprovando si un elemento existe en un diccionario 
print("#comprovando si un elemento existe en un diccionario ")

dicP = {
    "one":1,
    "two":2,
    "six":6,
    "ten":10
}

print("ten" in dicP) # mostrando con la clave
print(2 in dicP) 

#Acceso a un elemento por clave

print("#Acceso a un elemento por clave")
print(dicP["one"])

#eliminacion de un elemento
print("#eliminacion de un elemento")
del dicP["six"]
print(dicP)

#eliminacion de todos los elementos del diccionario
print("#eliminacion de todos los elementos del diccionario")
dicP.clear()
print(dicP)

# recuperacion de elementos del diccionario en forma de lista 
print("# recuperacion de elementos del diccionario en forma de lista ")

dicP = {
    "one":1,
    "two":2,
    "six":6,
    "ten":10
}
print(list(dicP.items()))

#Recuperacion de claves como una lista 
print("#Recuperacion de claves como una lista ")
print(list(dicP.keys()))

#recuperando los valores
print("#recuperando los valores")
print(list(dicP.values()))










