
import pandas as pd
#manejo de archivos 
print("#manejo de archivos ")
# dataframe 
data = {'first_name': ['Sigrid', 'Joe', 'Theodoric','Kennedy', 'Beatrix', 'Olimpia', 'Grange', 'Sallee'],
        'last_name': ['Mannock', 'Hinners', 'Rivers', 'Donnell', 'Parlett', 'Guenther', 'Douce', 'Johnstone'],
        'age': [27, 31, 36, 53, 48, 36, 40, 34],
        'amount_1': [7.17, 1.90, 1.11, 1.41, 6.69, 4.62, 1.01, 4.88],
        'amount_2': [8.06,  "?", 5.90,  "?",  "?", 7.48, 4.37,  "?"]}

#creando el dataframe
datosDataFrame = pd.DataFrame(data)

print(datosDataFrame)
#creando el archivo csv
datosDataFrame.to_csv('ejemplo.csv')
datosDataFrame.to_csv('example.csv', sep=',') #escojo el separador del archivo

#lectura de archivos csv con pandas
print("#lectura de archivos csv con pandas")
print("#lectura de archivos csv")
df = pd.read_csv("ejemplo.csv")
print(df)

#leyendo con el separador
print("#leyendo con el separador")

df = pd.read_csv("example.csv", sep= ';')
print(df)

# el sistema siempre admite la primera fila como cabecera  si no tengo cabecera puedo hacer los isguiente
print("omitiendo cabecera")
df = pd.read_csv("example.csv", header = None)
print(df)

# parametro para ignorar alguna fila del archivo csv
print("#elimina la fila una y coloca la cabecera que yo quiera ")
df = pd.read_csv("example.csv", 
                 skiprows=1, 
                 names=["UID","First Name", "Last Name", "Age", "Sales #1", "Sales #2"])
print(df)

#propiedad para signar valores nulos a caracteres especificos
df = pd.read_csv("example.csv", na_values=['?'])
print(df)





#=================================================================
#creando de archivo de excel 
#===================================================================
print("#creando de archivo de excel ")
#nombre del archivo y nombre de la hoja de excel
datosDataFrame.to_excel('example.xlsx', sheet_name ='example')
print("lectura de archivos de excel")
df2 = pd.read_excel('example.xlsx', sheet_name ='example')
print(df2)


# si no queremos que aparezca la cabecera usamos None
df2 = pd.read_excel('example.xlsx', sheet_name ='example', header =None)
print(df2)


# eliminando las 3 primeras filas
df2 = pd.read_excel('example.xlsx', sheet_name ='example', skiprows =3)
print(df2)
