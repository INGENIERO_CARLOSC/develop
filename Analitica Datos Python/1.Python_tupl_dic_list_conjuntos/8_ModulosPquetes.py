#========================================================================
#------------Modulos Paquetes Numpy---------------
#========================================================================
import numpy #importando la libreria numpy 
# from numpy import *   Importando el contenido completo del modulo, no es recomendable 
#from numpy import array , mean  Importando de manera especifica
array = numpy.array([[1,2],[3,4]]) # creando arreglo array, vector 
media = numpy.mean(array) #usando la proiedad mean para mostrar la media de los elementos
print("mostrando el array")
print(array)
print("Muestra la media de los elementos dentro del array")
print(media)

#========================================================================
#----------Importando el modulo con un alias esta practica es muy usada---
#========================================================================
from numpy import array  as arreglo
print("Importando el modulo con un alias esta practica es muy usada")
print()
array = arreglo([[1,2],[3,4]])
print(array)

#========================================================================
#-----------------------------modulos mas usados ---
#========================================================================
print("")
print("--------------modulos mas usados ---")

'''
sys = se usa para las rutas para el interprete de python, donde buscar modulos 
os = gestion logins / usuarios etc
os.path = funcionalidad para gestion de dorectorios o carpetas
math = funciones matematicas
random =funciones de generacion de numeros aletorios
'''


#========================================================================
#----------------------------------------
#========================================================================



#========================================================================
#----------------------------------------
#========================================================================
