#=======================================================
#---------Un grafico con varios componenetes------------
#====Funcipnes plot()  y show() ============================

import matplotlib.pyplot as plt
import numpy as np

#1. creando set de datos
valor_x = np.linspace(0, 2 * np.pi,100) # creando valores entre 0 y 2 x pi
seno_y_valor = np.sin(valor_x) #calculando el seno del valor_x
coseno_y_valor = np.cos(valor_x)#calculando el cosseno del valor_x

#primera grafica
#2. añadimos la serie de la funcion seno
sin_line = plt.plot(valor_x,seno_y_valor) #plot que representa los valores de x con el seno
cos_line = plt.plot(valor_x,coseno_y_valor)#plot que representa los valores de x con el coseno
#encabezado de la grafica
plt.xlabel('valores de x') #titulo del eje x
plt.ylabel('valores de y')#titulo del eje y
plt.legend([sin_line, cos_line],['Y = sin(x)', 'y=cos(x)'])
plt.show() # show es como el salto final o de linea entre graficas


#-2da grafica del seno de x
sin_line = plt.plot(valor_x,seno_y_valor)
plt.show()#indica que la graficacion ha terminado