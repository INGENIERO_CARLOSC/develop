#=============================================================
#----Visualizacion_Graficas_Python --------------------------
#=============================================================
'''
Es una libreria que permite visualizar los datos de manera grafica en python
tambien hay otras como seaborn, plotly, bokeh 
'''
# FOrma de importar la libreria


from tkinter import Y
import matplotlib.pyplot as plt

#%matplotlib 

#============================
#---Mi primer grafico--------
#============================

#plot es la propiedad que permite visualizar sin terminar de editar el grafico 
# para ejecutar click derecho run current file with windows


# graficando la funcion f(x) =  x^2
x_values = range(10)
y_values = [value ** 2 for value in x_values]
plt.plot(x_values,y_values)

#==========================
#---Entendiendo la funcion plot
#==========================


#plt.ioff() desactiva el trabajo interactivo
plt.ion() #Activando trabajo de manera interactiva
print(plt.isinteractive()) #verificando interactividad