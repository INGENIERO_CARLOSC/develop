


import matplotlib.pyplot as plt
import pandas as pd

#leyendo la tabla mtcars creada anteiormente
mtcars =  pd.read_csv('mtcars.csv', sep=',')
print(mtcars)
print("--------------------")
#leyendo la tabla diamonds.csv creada anteiormente
diamonds =  pd.read_csv('diamonds.csv', sep=',')
print(diamonds)

#===============grafico de barras con la media de consumo por cilindros

mpg_cyl =  mtcars['mpg'].groupby(mtcars['cyl']).mean()
print(mpg_cyl)
#width= 1.5 , tick_label= [4,6,8], color= 'red' son estilos del grfico si los quitamos tenemos el estilo por defaul de la libreri a
plt.bar(mpg_cyl.index, mpg_cyl.values, width= 1.5 , tick_label= [4,6,8], color= 'red')
plt.show()

#-------Cambiando la orientacion del rafico 

mpg_cyl =  mtcars['mpg'].groupby(mtcars['cyl']).mean()

#width= 1.5 , tick_label= [4,6,8], color= 'red' son estilos del grfico si los quitamos tenemos el estilo por defaul de la libreri a
plt.barh(mpg_cyl.index, mpg_cyl.values)
plt.show()

#---------Grafuicos de barras multiples-----

mpg_cyl_a = mtcars[mtcars['am']== 0].loc[:,'mpg'].groupby(mtcars['cyl']).mean()
mpg_cyl_m = mtcars[mtcars['am']== 1].loc[:,'mpg'].groupby(mtcars['cyl']).mean()

print(mpg_cyl_a )
plt.bar(mpg_cyl_a.index -0.3 ,mpg_cyl_a.values, color= 'b', width= 0.6)
plt.bar(mpg_cyl_m.index +0.3 ,mpg_cyl_m.values, color= 'r', width= 0.6)

plt.show()

