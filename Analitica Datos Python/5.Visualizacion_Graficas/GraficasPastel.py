
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#leyendo la tabla mtcars creada anteiormente
mtcars =  pd.read_csv('mtcars.csv', sep=',')
print(mtcars)
print("--------------------")
#leyendo la tabla diamonds.csv creada anteiormente
diamonds =  pd.read_csv('diamonds.csv', sep=',')
print(diamonds)

#---grficos de pastel ---

grafica =  diamonds.groupby('clarity').size()
plt.pie(grafica, autopct= "%.0f%%", labels = grafica.index ) #"%.2f%%" para 2 decimales 
plt.show()

#-----------mostrando el gajo mayor 

dataset =  diamonds.groupby('clarity').size()
explode  =  pd.Series(np.zeros_like(dataset), index = dataset.index, dtype=np.float32)
explode.index = dataset.index
explode[dataset.argmax()]= 0.4 #argumentos maximo sea 4%
plt.pie(dataset, autopct='%.2f', labels=dataset.index, explode= explode)
plt.show()

