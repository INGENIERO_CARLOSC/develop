

from xml.dom.minidom import Element
import matplotlib.pyplot as plt


import pandas as pd
#=================================================
#---grafica con varios componenetes--------------
#=================================================

''' como no tengo las tablas originales tube que crearlas con los siguientes codigos'''
#-------------------------------------------------------------------------------
#primera forma de crear dataframes apartir de un diccionario aparte
data1 = {'mpg': [21.0, 21.0, 22.8, 21.4, 18.7],
        'cyl': [6,6,4,6,8],
        'disp': [160.0, 160.0, 108.0, 258.0, 360.0 ],
        'hp': [110, 110, 93, 110, 175],
        'drat': [3.90, 3.90, 3.85, 3.08, 3.15],
        'wt': [2.620, 2.875, 2.320, 3.215, 3.440],
        'qsec': [16.46, 17.02, 18.61, 19.44, 17.02],
        'vs': [0,0,1,1,0],
        'am': [1,1,1,0,0],
        'gear': [4,4,4,3,3],
        'carb': [4,4,1,1,2]
        }
#creando el dataframes 
datosDataFrame1 = pd.DataFrame(data1)
#print(datosDataFrame1)
#creando los dataframes en tablas csv
datosDataFrame1.to_csv('mtcars.csv', sep=',') #escojo el separador del archivo
#-------------------------------------------------------------------------

#2da forma de crear dataframe directamente 
#las llaves del diccionario son los totulos de las columnas y los onmdex son los indices de las filas 
datosDataFrame2= pd.DataFrame({ 
    'carat':[0.23, 0.21, 0.23, 0.29, 0.31],
    'cut':['Ideal','Premiun','Good','Premium','Good'],
    'color':['E','E','E','I','J'],
    'clarity':['SI2','SI1','VS1','VS2','SI2'],
    'depth':[61.5, 59.8, 56.9, 62.4, 63.3],
    'table':[55.0, 61.0, 65.0, 58.0 ,580.0],
    'price':[326,326,327,334,335],
    'x':[3.95, 3.89, 4.05, 4.20, 4.34],
    'y':[3.98, 3.84, 4.07, 4.23, 4.35],
    'z':[2.43, 2.32, 2.31, 2.63, 2.75]
        })

#----crenado tabla csv----------------------------------------------------------------------------
datosDataFrame2.to_csv('diamonds.csv', sep=',') #escojo el separador del archivo
#-------------fin de la la creacion---------------------------------------------------------

#================================================================
#-----------Analisis---------GRAFICO DE LINE plot----------------
#================================================================

#leyendo la tabla mtcars creada anteiormente
mtcars =  pd.read_csv('mtcars.csv', sep=',')
print(mtcars)
print("--------------------")
#leyendo la tabla diamonds.csv creada anteiormente
diamonds =  pd.read_csv('diamonds.csv', sep=',')
print(diamonds)

#------------------------------GENERANDO GRAFICAS -------------
dataset = mtcars.groupby('cyl') #REFERENCIA DE VALORES DE X 
#VALORES DE Y
mpg_cyl = dataset['mpg'].mean()#CALUCLANDO LA MEDIA DE COLUMNA MPG
hp_cyl = dataset['hp'].mean()#CALUCLANDO LA MEDIA DE COLUMNA HP
plt.xlabel('CYL') #titulo del eje x
plt.ylabel('valores de y')#titulo del eje y
#--------------x----------------y-------------
plt.plot(mpg_cyl.index , mpg_cyl.values, color= 'red' , linestyle='dashed', marker= "o") # o--r es el estilo punteado
plt.plot(hp_cyl.index , hp_cyl.values, color= 'magenta' , linestyle='--', marker= "*" )
# otra forma de trasar las lineas es todo el el mismo fragmento de codigo
#--------------x----------------y---------------x----------------y
#plt.plot(hp_cyl.index , hp_cyl.values, mpg_cyl.index , mpg_cyl.values  )
plt.show()


#================================================
#----grafico de area --------stackplot---------------------
#================================================

mpg_cyl =mtcars['mpg'].groupby(mtcars['cyl']).mean() #agrupando valores unicos mpg con la media de cyl
plt.stackplot(mpg_cyl.index, mpg_cyl.values) #generando grafico de barra
plt.show()


# grafico de area con mas valores ------------------------------

dataset = mtcars.groupby('cyl') #REFERENCIA DE VALORES DE X 
#VALORES DE Y
mpg_cyl = dataset['mpg'].mean()#CALUCLANDO LA MEDIA DE COLUMNA MPG
hp_cyl = dataset['hp'].mean()#CALUCLANDO LA MEDIA DE COLUMNA HP
# otra forma de trasar las lineas es todo el el mismo fragmento de codigo
#--------------x----------------y---------------x----------------y
plt.stackplot(hp_cyl.index , mpg_cyl.values , hp_cyl.values )
plt.show()

#================================================
#---Diagrama de puntos--------------------------
#===============================================

# libreria.funcion tabla.columna1 , tabla.columna2
plt.scatter(diamonds.carat, diamonds.price)
plt.show()

#---diagrama de puntos con opacidad para tener claridad de los datos 
plt.scatter(diamonds.carat, diamonds.price, color = "black", alpha= 0.1)
plt.show()


#----gracfico de corte contra calirdad con la proporcion como tamaño del punto

x_values = []
y_values = []
sizes = []
for i , element_i in  enumerate(diamonds['cut'].unique()):
    for j , element_j in  enumerate(diamonds['clarity'].unique()):
        x_values.append(i)
        y_values.append(j)
        sizes.append(diamonds[(diamonds['cut']== element_i )& (diamonds['clarity']== element_j ) ].size/100)

plt.scatter(x_values, y_values, s = sizes)
plt.show()

#-------Muestra los puntos ordenados en forma de barra------corte / precios----------------------------
cuts = list(diamonds['cut'].unique())
x_values = [cuts.index(element) for element in diamonds['cut']]
y_values = diamonds['price']
plt.scatter(x_values, y_values, alpha= 0.1)
plt.show()


