
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


#leyendo la tabla mtcars creada anteiormente
mtcars =  pd.read_csv('mtcars.csv', sep=',')
print(mtcars)
print("--------------------")
#leyendo la tabla diamonds.csv creada anteiormente
diamonds =  pd.read_csv('diamonds.csv', sep=',')
print(diamonds)

#-----histograma basico 
#cumulative = True hace que la grafica sea alrevez 
plt.hist(diamonds['price'], bins = 20) #bins = 20  tamaño de las barras, normalizar el diagrama normed  = True
plt.show()

#----Histograma multiple 

for i in diamonds['cut'].unique():
    plt.hist(diamonds[diamonds['cut']==i].loc[:,'price'].values, stacked=True)
plt.show()