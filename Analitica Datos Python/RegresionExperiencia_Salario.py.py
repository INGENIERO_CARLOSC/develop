import modulo_normalizacionD as md  #LIBRERIA PERSONALIZADA

#CONFIGURACION PARA ABRIR DATASET
ruta = 'C:\\Users\\Usuario\\Documents\\develop\\Archivos para analisis\\'
fichero = 'Salary_Data.csv' #nombre del fichero
dataset= md.cargardatos(ruta, fichero,',')
print("dataset")
print(dataset.head())
print(" ")
#DIVISION DEL DATASET para trabajar la regresion
#cuando el dataset es de dos columnas esta tecnica es la mejor para separar las variables
X = dataset.iloc[:,:-1].values #iloc sirve para localizar filas i columnas por posicion,[:,:-1] toma todas las filas y todas la columnas exepto la ultima en una array
y = dataset.iloc[:,1].values # toma todas la filas pero solo de la ultima columna

#Hallando pvalor y coeficiciente de correlacion
xcoe= dataset['YearsExperience'] #variable pedictora o independiente
ycoe =dataset['Salary'] #variable a predecir dependiente

#coeficiciente de correlacion y pvalor
print(md.coeficienteCorrelacion(xcoe,ycoe))
#REGRESION LINEAL
 #variables de entrenamiento y prueba
X_train, X_test, y_train, y_test =  md.train_test_split(X,y, test_size= 0.2, random_state= 0)
#Creando el modelo
regresion= md.LinearRegression()    
#entrenamiento del modelo con variables de entreamiento 
regresion.fit(X_train, y_train)
#PREDICCION "estos son los puntos rojos de prediccion en entrenamiento y puntos verdes de datos reales de testeo"
print("VALORES NUMERICOS DE LA PREDICCION")
Yprediccion = md.prediccionRegresionL(regresion,X_test)
print(Yprediccion)
print("valores reales del entrenamiento y")
print(y_test)
#grafica DE LA PREDICCION
md.graficaDispercion(X_train,y_train,regresion, titulo ="grafica de entrenamiento y prediccion", 
                     etiquetaX='Años de experiencia', etiquetaY='sueldo en dolares')
#GRAFICA TRANSPUESTA DEL ENTRENAMIENTO Y EL TESTING   X_train,X_test,y_train,y_test,regresion,titulo,etiquetaX = 'X', etiquetaY= 'Y'
md.graDispersionTransTrainTesting(X_train,y_train,X_test,y_test,
                                  regresion, titulo ="grafica Transpuesta de entrenamiento y testing ",
                                  etiquetaX='Años de experiencia', etiquetaY='sueldo en dolares')