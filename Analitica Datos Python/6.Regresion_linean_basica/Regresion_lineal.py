#======================================
#---Regresion lineal-------------------
#=====================================

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression #importando el modelo para regresiones lineales
from sklearn import metrics

#leyendo la tabla ads creada anteiormente
ads =  pd.read_csv('Archivos para analisis\Libro2.csv', sep=';')
print(ads.head())
print("--------------------")

print(ads.describe()) # muestra los analisis basicos / buscar en la documentacion
print()

#creando variables predicotras
# variable  tabla.filtro por clave[todas las filas:] llama las colmnas exepto precios 
prediccion = ads.loc[:,ads.columns != 'precios'] #filtra todas las columnas excepto ventas
target =ads.precios # variable que apunta al objetivo 

#visualizacion de la variable objetivo con un histograma

historgrama=plt.hist(ads.precios)
plt.show()

#----------------------------
for col in prediccion : #recorreo cada uno de los nombres de las columnas 
    print("*********************", col ,"*******************") #muestra el nombre de la columna
    plt.scatter(prediccion[col], target) # muestra x nuemro de graficas en un paralelo entre la columna y el objetivo(precio)
    plt.show()

#-----------------------------------------
#================Construccion de un modelo 
#-----------------------------------------


# variable  tabla.filtro por clave[todas las filas:] llama las colmnas exepto precios 
preditores = ads.loc[:,ads.columns != 'precios'] #filtra todas las columnas excepto ventas
target =ads.precios # variable que apunta al objetivo 

regresor = LinearRegression() #preparando el modelo de regresion 
regresor.fit(preditores, target) # fit sirve para analizar los preditores para que encuentre la variable onjetivo target 

regresor.intercept_ #definicendo el valor alpha 0
regresor.coef_ #demas coeficientes

preds = regresor.predict(preditores) # creabdo la prediccion
print("predicciones")
print(preds[:10])


# modelando el margen de error -----------
print("-----------------------------------------")
df = pd.DataFrame([target.values, preds]).T
df.columns = ['valor real','prediccion'] #creando 2 columnas 
print(df)
df['Error']= df['valor real'] - df['prediccion']
print("Error")
print(df.head())

#hallando el error cuadratico 

metrics.mean_squared_error(df['valor real'], df['prediccion'])

#analizando visualmente el modelo 

plt.scatter(df['valor real'], df['prediccion']) #ralidad frente a la prediccion 
plt.plot(df['valor real'],df['valor real'], color= 'red', linewidth =2)
plt.show()
plt.hist(df.Error)
plt.show()