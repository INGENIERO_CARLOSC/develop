#Regresion Polinomica
import modulo_normalizacionD as md

'''
PARA ESTE EJEMPLO SE HARA 2 REGRESIONLES LINEAL Y POLINOMICA PARA COMPARAR SU USO CON 1 VA INDEPENDIENTE Y 1 VAR DEPENDIENTE
'''

#---enviando la ruta para leer el archivo
ruta = 'C:\\Users\\Usuario\\Documents\\develop\\Archivos para analisis\\'
fichero = 'Position_Salaries.csv' #nombre del fichero
dataset= md.cargardatos(ruta, fichero,',')
print(dataset.info())
print(dataset.head(10))

#----------------Separacion de variables del dataset---------------------------
print("Nombre de columnas")
print(dataset.columns)
print("")
#se recomienda  usar i loc para dividir el dataset
#X= dataset.iloc[:,1:2].values #variables independientes
#y= dataset.iloc[: ,2].values #Variable dependediente i a predecir

X= dataset[['Level']] #no tomamos la columna categorica 
y= dataset['Salary'] #columna a predecir

#================================
#=========Regresion lineal======
#================================
# NORMALMENTE SE DEBE DIVIDIR LOS DATOS PARA ENTRENAMIENTO Y TESTING PERO COMO LA TABLA TIENE SOLO 10 FILAS OMITIREMOS EL PASO
#Ajustand0 la Regresion lineal con el dataset 
lin_reg = md.regresionLineal(X,y) #en este caso la regresion la hacemos sin usar md por que no separamos las variables x_test ni y_test
#visualizacion del modelo lineal
md.graficaDispercion(X,y,lin_reg, 'grafica de la regresion lineal', 'Posicion del empleado','Salario')

#===================================================================
#Ajustando la regresion Polinomica
#===================================================================
#creabdo objeto para obtener caracteristicas polinomicas con el degree o potencia del tamaño deseado
poly_reg = md.MatrizPolinomica(4) #le enviamos un valor de degree para ajustar la potencia y la linea 
# toma los avlores de X y la estrcutura polinomica  para entrenar los datos
X_poly = md.EntrenamientoMatrizPolinomica(X,poly_reg)
print("Mostrado la variable independiente de manera polinomilal")
print(X_poly)  #por defceto la columna de 1 es necesaria 
#regresion lineal del polinomio
lin_reg2 = md.RegresionPolinica(X_poly, y)
#visualizacion de modelo polinomico
md.graficaDispercionPolinomicaSimple(X,y,lin_reg2,poly_reg, 'Regresion polinomica', 'Experiencia', 'Salario')

#prediccion de los modelos 
print("El valor del sueldo a pagar para el empleado de nivel 6.5 es: ")
prediccionSalario= md.PrediccionValorPolinomico(lin_reg2, poly_reg, 6.5) # le enviamos los parametros mas el valor de x posicion del caro y predice el sueldo
print(prediccionSalario)
#print(md.PrediccionValorPolinomico(lin_reg2,poly_reg, 6))
'''
En conclusion los puntos  son los datos reales y la linea es la prediccion de lo que deberia estar ganando en realidad cada persona
Haciendo un diagrama de sipercion podemos notar que una regresion lineal no nos da claridad en los datos 
y que una regresion polinomica si nos permite ser mas acertados en la curva de datos
'''


