#======================================================
#--------CONCATENACION DE DATAFRAMES -----------------
#======================================================

'''
esta funcion nos permite fusionar estrcuturas sin realizar ningun tipo de cruse entre ellas , si no colocandolas juntas para
creacionde una estrcutura mayor, podemos hacerlo tanto en filas como en columnas por defecto, se 
se concatenan filas y se mantienen las columnas de ambas, aun que no coincidan los elementos claves 
dejando valores Nan intactos 
'''

import pandas as pd
'''
pandas ofrece 2 formas para fusionar estrcuturas, 
'''
peliculas= pd.DataFrame({ #las llaves del diccionario son los totulos de las columnas y los onmdex son los indices de las filas 
        'año':[2014,2014,2013,2013],
        'valoracion':[6,None,8.75, None],
        'Presupuesto': [160,250,100,None],
        'Director':['Peter jacson', 'Carlos cogua', 'Martin scosesse','albert ens']       
        }, index=['Gozilla','El hobit 3', 'El lobo de wallstreet', 'gravity'])
print("Dataframe de peliculas")
print(peliculas)
print()

#----------concatenacion por filas------------------

pelicula2 = pd.DataFrame({
    'año':[1975,1942],
    'valoracion': [7.34 ,9.45],
    'Director': ['Peter jacson', 'Carlos mora']    
}, index=["La entrevista","El jugador"])

print("Dataframe de pelicula2")
print(pelicula2)
print("---------Concatenacion por filas--------------------------------")
#concatena las dos tablas con las claves en comun en este caso año y valoracion muestra todos los datos y rellena con nan los que no existen
print(pd.concat([peliculas,pelicula2], sort= True)) #sort hace el ordenamiento por los años
print()
#-------------concatenacion por columnas--------------

pelicula3 = pd.DataFrame({
    'Recaudacion':[525,722,392]
}, index=['Gozilla','El hobit 3', 'El lobo de wallstreet'])

print("Dataframe de pelicula3")
print(pelicula3)
print("---------Concatenacion por columnas--------------------------------")
#detecta las clave y concatena los avlores en comun de las columnas 
print(pd.concat([peliculas,pelicula3], axis=1,  sort= True))
print()
'''
Aunque es un funcionamiento mas propio de la funcion merge, pandas nor permite eliminar de resultados 
aquellas convinaciones para las que no existe ningun tipo de coincidencia de las dos tablas
'''
print("Limpieza de datos que no tienen nada en comun entre tablas  .concat([peliculas,pelicula3], axis=1,  join='inner'")
print(pd.concat([peliculas,pelicula3], axis=1,  join='inner'))

'''
Por ultimo puede ser util identificar en la estrcutura resultante  el origen de cada una de las filas
para posterior analisis 

La funcion concat incluye en parametros Keys que podemos usar para añadir una clave a cada una de las
estrcuturas de origen, que se convertiria en el nivel mas de un indice gerargico
'''

print(" ----Concatena las tablas  por filas y asigna un subindice con keys=['tabla1','tabla2']--------")
print(pd.concat([peliculas,pelicula2], keys=['tabla1','tabla2'], sort=True))

#==============================================================
#----------OPERACION DE AGRUPACIONES---------------------------
#==============================================================

'''
pANDAS PERMITE HACER AGRUPACION DE RESULTADOS y operaciones sobre algunos grupos (similar a 
la sentencia group by de sql)

'''
print("---Agrupar por año unifica los valores de los años sy hay años repetidos suma esos valores ----")
#variable    dataframe funcion  columna
Agrupado = peliculas.groupby('año')
print(type(Agrupado))

#media por grupo 
print("sacando la media de los años ")
print(Agrupado.mean())
print()

#conteo de valores no nulos 
print("conteo de valores no nulos  ")
print(Agrupado.count())
print()

# AGRUPACION POR MULTIPLES CLAVES 

print("AGRUPACION POR MULTIPLES CLAVES  ")
print(peliculas.groupby(['año','Director']).sum())