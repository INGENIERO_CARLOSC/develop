#==============================================================
#------APlicacion de funciones sobre estrcuturas----
#==============================================================

'''
AL igual que R studio tiene la familia de funciones apply, pandas pone a nuestra disposicion un
conjunto de funciones que nos permite aplicar operaciones a elemento filas por fila y columna por columna
en concreto son 3 funciones
'''
import numpy as np
import pandas as pd
#--APLicacion de funciones  elemento a elemento en Series
serie = pd.Series([1,2,3,4,5,6])
print(serie)


print("#--PLicacion de funciones  elemento a elemento en Series con map")
#funcion para determinar que elemento de la serie es par o impar
def es_par(elemento):
    if elemento % 2 ==0:
        return 'par: ' + str(elemento)
    else:
        return  'impar: ' + str(elemento)

#parametro(serie)iteracion(map)(funcion)
print(serie.map(es_par)) #funcion map itera los elementos es comoun for 



#===============================================================
#================DATAFRAMES=====================================
#===============================================================


#--APLicacion de funciones  elemento a elemento en Dataframes =====APPLYMAP=====
print("-------------------------------------------------------------------------------")
print("#--APLicacion de funciones  elemento a elemento en Dataframes")

#dataframe
dataframe =  pd.DataFrame(np.arange(16).reshape(4,4), index=['f1','f2','f3','f4'], columns=['c1','c2','c3','c4']) #dataframe creado con un array de 4x4 con numeros del 0 al 15
print(dataframe)

#funcion para determinar que elemento de la serie es par o impar
def es_par(elemento):
    if elemento % 2 ==0:
        return 'par: ' + str(elemento)
    else:
        return  'impar: ' + str(elemento)

print("aplicacion de elemento a elemento f/c con applymap Data frame")
print(dataframe.applymap(es_par)) #applymap itera en fila y columna aplicando la funcion creada es_par

#============================FUNCION APPLY + AXIS (0 =columnas, 1 = filas)==============================
print("aplicacion en dataframes con funcion apply ")

def es_suma_par(elemento):
    if np.sum(elemento) % 2 == 0:
        return 'Suma Par: '+ str(np.sum(elemento))
    else:
        return 'Suma Impar: '+ str(np.sum(elemento))

# Aplicacion de la funcion sobre columnas 
print("Aplicacion de la funcion sobre columnas")
print(dataframe.apply(es_suma_par, axis= 0))
print()

# Aplicacion de la funcion sobre filas
print("Aplicacion de la funcion sobre filas ")
print(dataframe.apply(es_suma_par, axis= 1))
print()