#===============================================================
#---TRATAMIENTO CON COLUMNAS con el mismo nombre de encabezado------------------------
#===============================================================


import pandas as pd
'''
pandas ofrece 2 formas para fusionar estrcuturas, 
'''
peliculas= pd.DataFrame({ #las llaves del diccionario son los totulos de las columnas y los onmdex son los indices de las filas 
        'año':[2014,2014,2013,2013],
        'valoracion':[6,None,8.75, None],
        'Presupuesto': [160,250,100,None],
        'Director':['Peter jacson', 'Carlos cogua', 'Martin scosesse','albert ens'],
        'titulo':['Gozilla','El hobit 3', 'El lobo de wallstreet', 'gravity']
        })
print("Dataframe de peliculas")
print(peliculas)
print()

#-------------------------------------------

directores = pd.DataFrame({
    'Director': ['Peter jacson', 'Carlos mora', 'Martin scosesse'],
    'AñoNacimiento':[1975,1942,1949],
    'Nacionalidad': ['England','Usa','Spain'],
    'valoracion': [102,98,81]
})

print("Dataframe de directores")
print(directores)
print()
print("-Sudfijos-----A la izquiera estara la valorizacion de la pelicula -----A la derecha la valorizacion del Director-----------------------")

'''
Si queremos modificar los subfijos repetidos podemos usar el parametro
suffixes = recibe una tupla con los subfijos a utilizar 
'''
print(pd.merge(peliculas,directores, left_on= 'Director', right_on= 'Director', suffixes=('_peli', '_dire')))