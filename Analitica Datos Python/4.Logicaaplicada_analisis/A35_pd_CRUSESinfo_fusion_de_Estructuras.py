
#=========================================
#----Fusion de estrcuturas----------------
#=======CRUCES DE INFORMACION =============
from turtle import left, right
from numpy import outer
import pandas as pd
'''
pandas ofrece 2 formas para fusionar estrcuturas, 
'''
peliculas= pd.DataFrame({ #las llaves del diccionario son los totulos de las columnas y los onmdex son los indices de las filas 
        'año':[2014,2014,2013,2013],
        'valoracion':[6,None,8.75, None],
        'Presupuesto': [160,250,100,None],
        'Director':['Peter jacson', 'Carlos cogua', 'Martin scosesse','albert ens'],
        'titulo':['Gozilla','El hobit 3', 'El lobo de wallstreet', 'gravity']
        })
print("Dataframe de peliculas")
print(peliculas)
print()

#-------------------------------------------

directores = pd.DataFrame({
    'Director': ['Peter jacson', 'Carlos mora', 'Martin scosesse'],
    'AñoNacimiento':[1975,1942,1949],
    'Nacionalidad': ['England','Usa','Spain']
})

print("Dataframe de directores")
print(directores)
print()
print("---------------------------------------------------------------------------------------------")
#====Merge Concatenacion de valores busc clave o columnas que conciden ======================
print("#==Merge busca elementos en comun y hace una concatenacion de la data de las tablas=========")
print(pd.merge(peliculas,directores))
print()

'''
podemos especificar explicitamente el conjunto de columnas a usar en el cruce de informacion
'''
print("---------------------------------------------------------------------------------------------")
print("realizando bsuqueda especificando el conjunto de  columnas a usar en el cruce de informacion ")
#----Cambiand el nombre de las columnas de dataframe directores  ['Nombre','Nacimiento','Nacionalidad'] solo se hizo para este ejemplo
directores.columns= ['Nombre','Nacimiento','Nacionalidad'] #se realizo este cambio para ejemplo en casos de que los encabezados de las tablas no tengan el mismo nombre
# concatena tabla peliculas y directores a la izquierda datos de directores a la derecha los nombres de los
#  merge compara la similitud entre Director data frame pelicula y Nombre en dataframe directores
print(pd.merge(peliculas,directores, left_on='Director', right_on='Nombre')) #muy comun y super importante para joins


#----------------Tipo de join o union -------------------------------------------------------------
print("----------------Tipo de join o union ------------------------------------------------------------- ")
'''
Igual que Joins de Sql, podemos espcificar el modo de cruce de a aplicar, haciendo que las filas
de las estrcuturas de la izquierda, derecha o ambas que no coincidan se mantengan en el resultado 
estableciendo los valores Nan en quellos elementos donde no existe la informacion
'''
print("----------------------------------------------------------------------------------------")
print("Si quiero mantener todos los datos de la tabla izquierda peliculas aunque no tengan director uso how= left ")
print(pd.merge(peliculas,directores, left_on= 'Director', right_on= 'Nombre',how='left'))
print()

print("----------------------------------------------------------------------------------------")
print("Si quiero mantener todos los datos de la tabla derecha directores aunque no tengan peliculas  uso how=right ")
print(pd.merge(peliculas,directores, left_on= 'Director', right_on= 'Nombre',how='right'))
print()

print("----------------------------------------------------------------------------------------")
print("Si quiero tener toda la informacion que coincida entre las dos tablas incluidos Nan de todo how=outer")
print(pd.merge(peliculas,directores, left_on= 'Director', right_on= 'Nombre',how='outer'))
print()


