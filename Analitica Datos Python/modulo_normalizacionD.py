import pandas as pd #dataframes
import numpy as np #vectores y operaciones matematicaas
from scipy import stats #coeficiente de correlacion
import matplotlib.pyplot as plt #graficas
from sklearn.linear_model import LinearRegression #permite crear el modelo para la regresion lineal
from sklearn.model_selection import train_test_split  # regresion lineal simple variables de entrenamiento y test
from sklearn import metrics # permite hayar el error cuadradtico y el R cuadrado
from sklearn.preprocessing import PolynomialFeatures #regresion polinomicas

#**********************************
# data set de prubas solo cuando sea necesario
#**********************************
def crearDatasetPrueba(filas,columnas, Namecolumnas, Rinicial = 0, Rfinal = 1000):
    datos =pd.DataFrame(np.random.randint(Rinicial, Rfinal,( filas, columnas)), columns= Namecolumnas)
    return datos

#**********************************
#------funcion para cargar datos--
#**********************************
def cargardatos(ruta, fichero,separador = ','):
    datos =pd.read_csv(ruta+fichero, sep= separador)
    return datos #retornando los datos del dataset

#**********************************
#-----funion para acceder a columnas---
#**********************************
def verNombrecolumnas(datos):
    return datos.columns

#**********************************
#----funcion para cambiar datos de columnas-------
#**********************************
def cambiarencabezadoAllColumns(datos, columnas): 
    datos.columns = columnas
    return datos  

#*******************************************************
#--metodo para cambiar el nombre de 1 columna especifica--
#*******************************************************
def renombrarColumna(datos, cambio):
    datos.rename(columns= cambio, inplace = True)    
    return datos

#*******************************************************
#----funcion para CREAR COPIAS DEL DATASET O GUARDAR DATASET NUEVO------
#*******************************************************
def guardarDatos(datos,ruta,fichero):
    datos.to_csv(ruta+fichero)
    return True

#*******************************************************
#-ver caracteristicas del dataset ----
#*******************************************************
def caracteristicasDataset(datos,tipo = 'numerico'):
    ''' Tipo numerico o todos'''
    if tipo == 'numerico':
        return datos.describe() #muestra caracteristicas solo datos numericos
    else:
        return datos.describe(include= 'all') #muestra todas las caracteristicas de la tabla

#*******************************************************
#----Reemplazando datos nulos directamente en el dataframe--
#*******************************************************
def remplazarNulosDataframe(datos,columna): #atributos que recibe 
    mediaD= datos[columna].mean() #hayando la media de los valores de la columna
    #reemplazando nan por la amedia
    datos[columna].replace(np.nan, mediaD, inplace= True)
    return datos

#*******************************************************
#--Reemplazando datos nulos en un vector nuevo --
#*******************************************************
def reemplazarNulosXcolumnaEspecifica( columna): # reemplazando nulos de la columna especifica en un nuevo vector consume menos recursos
    mediaD= columna.mean() #hayando la media de los valores de la columna
    #reemplazando nan por la amedia
    columna.replace(np.nan, mediaD, inplace= True)
    return columna    
    
#*************************************************
#----CAMBIANDO EL TIPO DE DATOS A UNA VARIABLE 
#*************************************************
#funcion para cambiar tipo de dato de una columna por defecto si no especificamos el tipo lo pasara a float
def cambiarTipoDatoColumnaDataset(datos,columna,tipo = 'float64' ):
    
    if datos[columna].dtypes == 'object': #si el tipo de dato de la columna es objeto lo convertira a float64 numerico
        datos[columna] = pd.to_numeric(datos[columna], errors= 'coerce') #convierte string en numerico
    else:
        datos[columna] = datos[columna].astype(tipo) #si no es objeto y le especificamos el tipo de datos lo convierte a ese tipo
    return datos

#Funcion para cambiar el tipo de dato a una columna especifica en un nuevo vector

def cambiarTipoDatoColumnaVectorN(columna,tipo = 'float64' ):
    
    if columna.dtypes == 'object': #si el tipo de dato de la columna es objeto lo convertira a float64 numerico
        #si no funciona eliminar el if y dejar solo el coerce 
        columna = pd.to_numeric(columna, errors= 'coerce') #convierte string en numerico
    else:
        columna = columna.astype(tipo) #si no es objeto y le especificamos el tipo de datos lo convierte a ese tipo
    return columna

#************************************
#----NORMALIZACION DE VARIABLES-----
#************************************

#esta funcion aplica para cuando los valores un unda columna son el resultado de operar los deatos de una con una cons
# tante, ejemplo columna kilometros, millas etc , no aplica para todos los casos
def normalizacionColumnaInDataset(datos,columna):
    datos[columna] = datos[columna]/datos[columna].max() #datos de la columna dividismos entre el valor maximo de los mismos
    return datos    

#normalizacion de una columna especifica en una variable nueva
def normalizacionColumnaAparte(columna):
    columna = columna/columna.max() #datos de la columna dividismos entre el valor maximo de los mismos
    return columna

#*************************************************************
#-----------GENERADOR DE DUMMIES- V CATEGORICAS A FICTICIAS---
#*************************************************************
#INCLUYENDOLAS EN EL DATASET COMPLETO 
def obtenerDummiesIncluirlosDataset(datos,columna):
    datos = pd.get_dummies(datos[columna])
    return datos

# CREANDOLAS POR SEPARADO DEL DATASET ORIGINAL
def obtenerDummiesColumaEspecificaAparte(columna):
    columna = pd.get_dummies(columna)
    return columna



#*********************************
#----VISUALIZACION DE DATOS------
#********************************
def graficaBoxplot(columna,titulo):
    plt.boxplot(columna)
    plt.title(titulo)
    plt.show()

def graficaDispercion(X_train,y_train,regresion,titulo,etiquetaX = 'X', etiquetaY= 'Y'):
    plt.scatter(X_train,y_train)
    plt.plot(X_train, regresion.predict(X_train), color= "Red")
    plt.title(titulo)
    plt.xlabel(etiquetaX)
    plt.ylabel(etiquetaY)
    plt.show()
#==========Regresiones polinomicas=======
def MatrizPolinomica(valorDegree):
    poly_reg = PolynomialFeatures(degree= valorDegree) 
    return poly_reg

def EntrenamientoMatrizPolinomica(X,poly_reg): #enviamos el valor de x y la estrcutra polinomica con el exponente asigando
    #poly_reg = PolynomialFeatures(degree= valorDegree)  #transofrmando la matriz de caracteristicas x en matriz polinomial
    X_poly = poly_reg.fit_transform(X) #nomralmente se envia X_train pero en este caso enviamos x
    return X_poly #retornamos la matriz polinomica de x

def RegresionPolinica(X_poly, y):
    lin_reg2 = LinearRegression()
    lin_reg2.fit(X_poly, y) # le enviamos x polinomica y y a predecir
    return lin_reg2 

#muestra la prediccion del salario segun posicion del empleado a contratar
def PrediccionValorPolinomico(lin_reg2, poly_reg, valor):
    prediccion= lin_reg2.predict(poly_reg.fit_transform([[valor]])) #le pasamos del nivel  del empleado 
    return prediccion

#grafica de regresion polinomica simple
def graficaDispercionPolinomicaSimple(X,y,lin_reg2,poly_reg,titulo= 'Sin Titulo',etiquetaX = 'X', etiquetaY= 'Y'):
    #X,y,lin_reg2,poly_reg
    plt.scatter(X,y, color= 'Red')
    plt.plot(X, lin_reg2.predict(poly_reg.fit_transform(X)), color= 'Blue')
    plt.title(titulo)
    plt.xlabel(etiquetaX)
    plt.ylabel(etiquetaY)
    plt.show()
#============fin de regresion polinomica

def graDispersionTransTrainTesting(X_train,y_train,X_test,y_test,regresion,titulo= 'Sin Titulo',etiquetaX = 'X', etiquetaY= 'Y'):
    plt.scatter(X_train,y_train, color ="red" ) #valores para predecir entrenamiento
    plt.scatter(X_test,y_test, color ="green" ) #valores para predecir testeo
    plt.plot(X_train, regresion.predict(X_train), color= "blue")
    plt.title(titulo)
    plt.xlabel(etiquetaX)
    plt.ylabel(etiquetaY)
    plt.show()

#***************************************************************************************
#==================Coeficiente de Correlacion y Pvalor====Regresiones lineales=========
#**************************************************************************************

def coeficienteCorrelacion(var1,var2):
    pearson_coef, p_valor = stats.pearsonr(var1,var2)
    #https://www.monografias.com/trabajos85/coeficiente-correlacion-karl-pearson/coeficiente-correlacion-karl-pearson
    #https://blog.minitab.com/es/que-se-puede-decir-cuando-el-valor-p-es-mayor-que-0-05   
    if pearson_coef < 0 and p_valor < 0.05 :
        return print("Coeficiente = ",pearson_coef," La Hipótesis nula es rechazada.   Pvalor= ", p_valor, "menor al nivel Significativo OK.")
    
    elif(pearson_coef < 0 and p_valor > 0.05): 
        return print("Coeficiente = ",pearson_coef,"La Hipótesis nula es rechazada. Pvalor= ", p_valor, " Superior a 0,05, No significativo.")
    
    elif(pearson_coef > 0 and p_valor < 0.05):                   
        return print("Coeficiente = ",pearson_coef,"Correlación positiva grande y perfecta,Pvalor= ", p_valor, " menor al nivel Significativo OK.")
    elif(pearson_coef > 0 and p_valor > 0.05): 
        return print("Coeficiente = ",pearson_coef,"Correlación positiva grande y perfecta, Pvalor= ", p_valor, " Superior a 0,05, No significativo.")
    
    elif pearson_coef == 0   and p_valor < 0.05:
        return print("Coeficiente = ",pearson_coef,"Correlación nula,   Pvalor= ", p_valor, " menor al nivel Significativo OK.")
    
    elif pearson_coef == 0   and p_valor > 0.05:
        return ("Coeficiente = ",pearson_coef,"Correlación nula, Pvalor= ", p_valor, " Superior a 0,05, No significativo")
        
   
    

#******************************************************
#=========Agrupacion de datos ========================
#*****************************************************

def agruparDatos(columna):
    datos_agrupados = columna.groupby(columna, as_index= False)
    return datos_agrupados
    
    
#******************************************************
#=========Regresion lineal========================
#*****************************************************

def regresionLineal(X,y):
    #variables de entrenamiento y prueba
    X_train, X_test, y_train, Y_test = train_test_split(X, y, test_size= 1/3, random_state= 0 )
    
    #reando el modelo
    regresion= LinearRegression()    
    #entrenamiento del modelo
    regresion.fit(X_train, y_train)
    
    return regresion


def prediccionRegresionL(regresion,X): 
    #prediccion sobre la variable de entrenamiento
    predicccion=regresion.predict(X)    
    #print(regresion.score(X,y))
    return predicccion
    
#RLM

def errorCuadratico(y_real, prediccion):
    ECM = metrics.mean_squared_error(y_real, prediccion)
    return ECM

def r_cuadrado(y_real, prediccion):
    r_cuadrado = metrics.r2_score(y_real, prediccion)
    return r_cuadrado




#-------------------validando la ruta---para ejecutar en el mismo archivo-------------------------------------
#if __name__ == '__main__':
    #ruta = 'C:\\Users\\Usuario\\Documents\\develop\\Archivos para analisis\\'
    #fichero = 'test.csv' #nombre del fichero
    #datos = cargardatos(ruta,fichero) #enviando parametros a la funcion 


#print(datos.head(5))# mostrando 5 filas del dataset
#print("---------------------------------------------------------------------------------------------")


 #graficas
#graficaBoxplot(datos['Kilometers_Driven'], 'Kilometros')
#graficaDispoercion(x,y,titulo= 'Pruebas')
#   filas, columnas nombre de columnas 
#datos2 = crearDatasetPrueba(100,5,['A','B','C','D','E'])
#print(datos2)

#X= datos2['A']
#y= datos2['E']



#mostrando coeficiente de correlacion y pvalor
#print("mostrando coeficiente de correlacion y pvalor ")
#print(coeficienteCorrelacion(X,y))

#regresion lineal -------------------------------------------------------

#X= np.array(X).reshape(1, -1).T #creando en x una array de ejemplo para la regresion lineal 
#regresion = regresionLineal(X,y)
#prediccion = prediccionRegresionL(regresion, X)
#print(prediccion)
#------------------------------------------------------------------------------------------------
#print("Mostrando el nombre de las columnas del dataset")
#print(verNombrecolumnas(datos))# envia parametros para que la funcion me muestr el nombre de las columnas
#print("-------------------------------------------------------------------------------------------------")

#----------------------cambiar nombres de las columnas ---------------------------------------------
#nueva_cabecera = ["nombre","localizacion","año","kilometros_recorridos",
#"combustible","trasmision","tipo_propietario","kilometraje","motor","potencia","asientos","Nuevo_precio"]
#cambiarencabezadoAllColumns(datos,nueva_cabecera)
#mostrando los nuevos nombres de las columnas del dataset
#print("NOMBRES NUEVOS DE LAS COLUMNAS")
#print(datos.columns)



#========================mostrando datos estadisticos ======================================
#print(caracteristicasDataset(datos))

#print("------Todos los datos------------")
#print(caracteristicasDataset(datos,'todos'))

#====================reemplazando los datos nan=============================================
#print("--------------------------Tabla de Datos Nan en todo el dataframe--------------------  " )
#print(datos.isnull().sum())

#remplazarNulos(datos,'asientos') #se le debe especificar la columna

#verificando campos nulos en la columna especifica
#print("verificando campos nulos en la columna especifica")
#print("--------------------------Tabla de Datos Nan modificados--------------------  " )
#print(datos.isnull().sum())

#-------------cambiar tipo de dato-----------------------
#print(datos.dtypes)
#columna = 'asientos'
#cambiarTipoDatoColumna(datos,columna, 'int64')
#print("#####################mostrando el tipo de dato actualizado###################################")
#print(datos)
#print(datos['asientos'].dtypes)

#modificando el nombre de una columna especifica
#print("--------Datos modificacdos deel renombre de Kilometros recorridos------------------")
#renombrarColumna(datos, {'kilometros_recorridos':'Kilometros_nuevos'} )
#print("Kilometros_nuevos sin normalizar")
#print(datos['Kilometros_nuevos'])

#print("#############################normalizando datos de una columna  Kilometros_nuevos############################### ")
#normalizando datos de una columna
#datos= normalizacionColumna(datos,'Kilometros_nuevos')
#print("Kilometros normalizados ")
#print(datos['Kilometros_nuevos'])

#=======================enviando parametros para almacenar el dataset =======================
#guardarDatos(datos,ruta, 'copia.csv')
