import modulo_normalizacionD as md
from sklearn.linear_model import LinearRegression #regresiones
import seaborn as sns #libreria basada en matplotlib para visualizar regreciones lineales
import matplotlib.pyplot as plt
#---enviando la ruta para leer el archivo
ruta = 'C:\\Users\\Usuario\\Documents\\develop\\Archivos para analisis\\'
fichero = 'datosModificados.csv' #nombre del fichero
datos= md.cargardatos(ruta, fichero,',')
print(datos.head())

print("---verificacion datos nulos---")
print(datos.isnull().sum()) #verificando datos nulos

#OJO ESTE PASO APLICA SOLO PARA ESTOS CASOS 
#solo para este caso en que exite una columna numerdad  podemos seleccionarla como indice de filas SOLO PARA ESTE CASO 
datos.set_index('indice', inplace= True)
print("----------------------------------------------------------------------------------------------")
print(datos.head())

#===========Regresion modo simple=======================================================
#creando la regresion 
regresion = LinearRegression()

#====================================================================
# opcion de hacer la regresion lineal entrenando las variables 
#--------------SEPARACION------ENTRENAMIENTO Y TESTING------------------------------

x= datos[['motor']]
y= datos['precio']

#--variables entrenamiento y prueba

from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x,y, test_size= 0.2, random_state= 0) #random_state= 0 es para que los caluclos salgan igual

print("---------variables predictoras-----------")
print(x_train)
print("---------variable objetivo-----------")
print(y_train)

#entrenamiento o ajuste de modelo

regresion.fit(x_train,y_train)


#grafica de regresion con matplotlib entrenamiento y testing transpuesto para verificar su similitud
plt.scatter(x_train,y_train, color ="red" ) #valores para predecir entrenamiento
plt.scatter(x_test,y_test, color ="green" ) #valores para predecir testeo
plt.plot(x_train, regresion.predict(x_train), color= "blue") # Recta de regresion se debe conservar en este grafico la recta es la misma
plt.title("precio vs motor  (train 80% Rojo  y test 20% verde)")
plt.xlabel("Precio")
plt.ylabel("Motor")
plt.show()


#grafica regresion definitiva 
plt.scatter(x_train,y_train, color ="red" ) #valores para predecir entrenamiento
plt.plot(x_train, regresion.predict(x_train), color= "blue") # Recta de regresion se debe conservar en este grafico la recta es la misma
plt.title("precio vs motor  (grafica re fregresion definitiva)")
plt.xlabel("Precio")
plt.ylabel("Motor")
plt.show()

# ---------prediccion------ informativo
prediccion = regresion.predict(x_test)
a = regresion.intercept_
b = regresion.coef_
# Mostramos ecuacion con variables entrenadas esto es solo informativo 
print('*'*100)
print(f'y={a}+{b}*x')
print('*'*100)
print('Prediccion')
print(prediccion)
print(y_test)
print(regresion.score(x_train,y_train))
print('*'*100)
print(regresion.score(x,y)) # lo ideal es que este proximo a 1


