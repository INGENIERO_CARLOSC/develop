#====================================================
#-------------------------------------------------
#===================================================
'''
en caso de que queramos cambiar o midifcar el nombre de los index o o columns se debe hacer con reindex
y obtendremos una nueva copia del index seleccionado

siempre que se crea una copia de index los espacio por default se rellenaran de ceros para evitar 
debemos aplicar:

fill_value: relleno a un valor fijo establecido
method: relleno segun un metodo definido 
        -ffill : relleno mediante observaciond e los ultimos valores rellenos 
        -bfill: relleno mediante observaciond e los proximos valores rellenos 


'''
import pandas as pd
import numpy as np

#serie de 4 valores con sus respectivos encabezado de fila
serie = pd.Series([1,2,3,4], index=['A','B','C','D'])
#dataframe con un array de numeros del 0 al 15 con un orden de 4x4 encabezado de filas y encabezado de columnas
dataF = pd.DataFrame(np.arange(16).reshape(4,4), index=['fila1','fila2','fila3','fila4'], columns=['col1','col2','col3','col4'])

#--reordenacion del indice de una serie 
print("reordenacion del indice de una serie  ")
print(serie.reindex(['D','C','B','A']))
print()
# Adicion de etiquetas a un indise de una serie / como es una etique nueva y no tiene un valor sale Nan
print("Adicion de etiquetas a un indise de una serie ")
print(serie.reindex(['D','C','B','A','Z']))
print()


#--para evitar valores Nan al agregar indices nuevis debe usar
#  
print(" para evitar valores Nan al agregar indices nuevos debe usar ,fill_value=0 ")
print(serie.reindex(['D','C','B','A','Z'],fill_value=0))
print("-----------------------------------------------------------------------------") 

#=================================
#--Data frames--------------------
#=================================
print("-----DataFRame Original--------")
print(dataF)
print("-------------------------------")

#---Seleccionando filas especificas del dataframe fila 1 y fila 3
print("Seleccionando filas especificas del dataframe fila 1 y fila 3")
print(dataF.reindex(['fila1','fila3']))
print()

#--añandiendo etiquetas de filas nuevas en el dataframe 
print("añandiendo etiquetas de filas nuevas en el dataframe ")
print(dataF.reindex(['fila1','fila3','fila5']))
print()

#--Siemrpe rellena con el contenido de la primera fila
print("-Metodo de relleno-, method= 'ffill'  toma los valores de la primera fila relacionada f1 y los pone en la fila nueva-----")
print(dataF.reindex(['fila1','fila2','fila10','fila3','fila4'], method= 'ffill'))
print()

#--rellena con el contenido de la fila anteior a la posicion de el 
print("-metodo de relleno descendente , method= 'bfill'--toma los valores entre f1 y f3 osea f2 y los asogna a la nueva fila f10-")
print(dataF.reindex(['fila1','fila2','fila10','fila3','fila4'], method= 'bfill'))
print()

#-----modificacion de indices por columnas en Dataframes
print("Reordenamiento  de indices por columnas en Dataframes")
print(dataF.reindex(columns=['col4','col2','col3','col1']))
print()

#---mostrando columnas especificas
print(" mostrando columnas especificas ")
print(dataF.reindex(columns=['col1','col3']))
print()

#-.reset_index() Convirtiendo index en datos de columnas
print(".reset_index()---Convirtiendo index en datos de columnas ")
print(dataF.reset_index())

#-.set_index(['col3'])--Reasign a toda la columna 3 y sus valores como el nuevo indice de filas de la tabla----------------
print(" .set_index(['col3']) Reasign a toda la columna 3 y sus valores como el nuevo indice de filas de la tabla")
print("Convirtiendo index en datos de columnas ")
print(dataF.set_index(['col3']))
