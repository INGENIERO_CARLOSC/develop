#=======================================================
#---------------Operaciones con dataFrames -------
#=======================================================

import numpy  as np
import pandas as pd
'''
Los diccionarios dentro de DataFrame son Diccionarios de Columnas 
'''
#---------Tratamiento de series y dataframes como diccionarios -
#--serie valores e index encabezado de filas
serie= pd.Series([1,2,3,4], index=['a','b','c','d'])
# en diccionario clave son elcabezado de las  columnas el valor es serie 
Dataf = pd.DataFrame({'var1': serie, 'var2': serie})
#--dataframe hecho apartir de una Serie 
print(Dataf)
print()

#Indexacion por clave --
print("Indexacion por clave en una serie  ")
print(serie['a'])
print()

# indexacion por nombre de columna de dataframe
print("indexacion en un Dataframe por nombre de columna de dataframe ")
print(Dataf['var2'])
print()

#==========================
#--Comprobando la existencia de una clave
#==========================

#--Comprobando la existencia de una clave en series
print("Comprobando la existencia de una clave en series")
print('b' in serie)
print()

#-- comprobacion de la existencia de la columna en datframes
print("comprobacion de la existencia de la columna en datframes")
print('b' in Dataf)
print()

#-- comprobacion de la existencia de la columna en datframes
print("comprobacion de la existencia de la columna en datframes")
print('var1' in Dataf)
print()

#===================================================
#-Adicion de Elementos-----------------------------
#==================================================

'''
Al añadir clomunas a un dataframe, el tamaño del vector añadido debera coincidir con el 
del dataframe original. de lo contrario se recibira un error
'''

#Adicion de elementos a aseries-
print("Adicion de elementos a aseries/ agregamos la el valor y el index")
serie['e']=5 #agregando el index y el valor
print(serie)
print()

#Adicion de elementos a Dataframe
print(" Adicion de elementos a Dataframe ")
Dataf['var3']=['uno','dos','tres','cuatro'] #se añade el collumn y los valores de la columna que debn ser del mismo tipo
print(Dataf)
print()

#==============================
#---Eliminacion de elementos --
#==============================

#eliminacion de elementos de una Serie
print("eliminacion de elementos de una Serie con el index o nombre de fila")
del serie['e']
print(serie)
print()

#Eliminacion de elementos de un dataframe por columna 
print(" #Eliminacion de elementos de un dataframe por columna ")
del Dataf['var3']
print(Dataf)
print()
