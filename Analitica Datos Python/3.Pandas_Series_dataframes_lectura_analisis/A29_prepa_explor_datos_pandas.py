#================================================================
#-----Preparacion y exploracion de datos -----------------------
#================================================================

import pandas as pd
import numpy as np

# tratamiento y analisis de datos pandas

#==MISSINGS VALUES  gestion de dtaos en blanco  deteccion de valores nulos o vacios y su tratamiento

'''
pandas ofrece  dos funciones para el tratamiento de estos datos

insnull: devuelve una serie o un dataframe booleano  indicando que elementos son Nan o none 
notnull: devuelve el inverso al anterior
'''

#******************LEYENDO EL ARCHIVO CSV*****************************
Tabla = pd.read_csv('Materia_ciclo1\Semana 4\Clase 3\Soluciones\cotizacion.csv', sep= ';' , nrows=4)
print(Tabla)
print()


print("eliminar los datos Nan con .isnull() --------------------------------------------------")
#eliminar los datos Nan  para hacer buen analisis de datos
print(Tabla.isnull())
print()

#------mostrando por columna la cantidad de Na que hay 
print("mostrando por columna la cantidad de Na que hay  ")
print(Tabla.isnull().sum())
print()


print("eliminar los datos Nan con .notnull()----------------------------------------------------")
print(Tabla.notnull())
print()

'''
con el metodo dropna podemos elininar campos nulos o vacions con las siguientes opciones:

axis: seleccion del eje sobre el que se va a realizar la eliminacion 
how: tomara posibles valores 'any' y 'all'  e indica si se debe eliminar la fila o la columna 
cuando hya uno o mas valores nan o cuando todos los valores sean nan

thresh: perdmite indicar, el numero de obeservaciones no nulas que se deben tener para no realizar el borrado
'''
#============Eliminacion de registros con missings values o valoores vacios 
print(" ******************Eliminacion de filas con almenos un valor Na************************")
print(Tabla.dropna(axis= 0, how = 'any'))
print()
#=========ELiminacion de colunas con algun valor NA========================
print("*****************ELiminacion de colunas con algun valor NA************************")
print(Tabla.dropna(axis= 1, how = 'any'))
print()

#================Eliminacion de filas con 2 o mas Nan ====si tiene 1 si lo mostrara===========================
#-thresh: perdmite indicar, el numero de obeservaciones no nulas que se deben tener para no realizar el borrado
print("Eliminacion de filas con 2 o mas Nan  ")
print(Tabla.dropna(thresh=len(Tabla.columns)-1))
print()





