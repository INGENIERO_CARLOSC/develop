#===================================================
#-----------Ordenamiento de series ----------------
#==================================================

import pandas as pd

seriejemplo = pd.Series([3,1,2,4], index=['d','a','c','b'])
print("serie original")
print(seriejemplo)
print()
#================INDICES ===============

#---Ordenamiento por indice 
print("Ordenamiento por indice  ")
print(seriejemplo.sort_index())
print()

#--Ordenamiento descendente por indice 
print("Ordenamiento por indice (ascending= False)  ")
print(seriejemplo.sort_index(ascending= False))
print()

#==========VALORES=========
#-- Ordenamiento por valores
print("Ordenamiento por indice  ")
print(seriejemplo.sort_values())
print()

#-- Ordenamiento por valores descendente 
print("Ordenamiento por indice  ")
print(seriejemplo.sort_values(ascending= False))
print()
