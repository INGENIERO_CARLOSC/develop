#========================================================
#---------Data Frames------------------------------------
#========================================================

'''
Son estrcuturas tabulares bidimencionales,  con las siguientes propiedades:

-compuesta por una serie oredenada de filas y una serie ordenada de columnas
-Tiene, por tanto un indice para las filas y otro para las columnas
-cada columna puede tener un tipo Numpy diferente
-puede ser visto como un diccionario de series, por tanto todas ellas tienen el mismo indice
'''

#------Creacion de dataframes----

'''
debemos usar un funcion constructor = "DataFrame" la cual funciona con los siguientes parametros:

data: Es obligatorio usarla, contiene los datos que queremos cargar en el dataframe, podra ser un diccionario
de series, diccionario de secuencias, un Narray bidimencional, una serie u otro dataframe
index: es opcional, contiene las etiquetas que queremos asignar a las filas del dataframe 
columns: es opcional, contiene las etiquetas que queremos asignar a las Columnas del dataframe
dtype: opcional, fijara el tipo de todas las columnas podra ser cualquier tipo de dato de NUmpy

NOTA: SI EL TAMAÑO DE CADA COLUMNA NO COINCIDE, SE ASIGNARA UN DATFRAME SUFICIENTEMENTE GRANDE
PARA CONTENER LA MAYORIA DE LOS DATOS Y LOS ESPCIOS RESTANTES SERAN LLENANDOS CON NAN
'''
from operator import index
import pandas as pd
import numpy as np
#---Dataframe desde diccionario de secuencias 
print("Dataframe desde diccionario de secuencias ")
Dataframe = pd.DataFrame({
    'enteros': [1,2,3],
    'strings': ['uno','dos','tres'],
    'double': [1.0,2.0,3.0]
})

print(Dataframe)
print()

#--------Data frame desde diccionario de series-----------
print("Data frame desde diccionario de series/ llenena con Nan cuando falta un dato ")
DataDicSer = pd.DataFrame({
    'var1': pd.Series([1,2,3], dtype= np.float64),
    'var2': pd.Series(['a','b'])
})
print(DataDicSer)
print()

#-----Dataframe desde Narray con indices para filas y columnas 
print("Dataframe desde Narray con indices para filas y columnas ")
DataNarray = pd.DataFrame(np.arange(16).reshape(4,4), index= ["f1","f2","f3","f4"], columns= ["c1","c2","c3","c4"] )
print(DataNarray)
print()

#---Dataframe desde Narray con indices para filas y columnas con tipo fijo para todas
print("Dataframe desde Narray con indices para filas y columnas con tipo fijo para todas")
DataNarrayF= pd.DataFrame(np.arange(16).reshape(4,4), dtype= np.float64)
print(DataNarrayF)
print()

#===============================================================
#---------Elementos de un Data Frane----------------------------
#==============================================================

'''
disponemos de 3 elementos para recuperar datos : el indice y las columnas de forma independiente 
'''
DataF = pd.DataFrame({
    'var1': [1,2,3],
    'var2': ['uno','dos','tres'],
    'var3': [1.0,2.0,3.0]
})

print("dataframe original")
print(DataF)
print()

#----valores de u dataframe 
print("#---- mostrando solo los valores de un dataframe in indices de filas y columnas ")
print(DataF.values)
print()
#-----Indices de las filas dataframe---
print("Indices de las filas dataframe ")
print(DataF.index)
print()
#------Indices de las Columnas -----
print("Indices de las filas dataframe ")
print(DataF.columns)
print()

#--- los valores de index i columns no se pueden cambiar por c/u por que son inmutables
#--pero si se puede de manera general ejemplo
print(" los valores de index i columns no se pueden cambiar por c/u por que son inmutables, pero si se puede de manera general ejemplo")
DataF.index= ["fila1","fila2","fila3"]
DataF.columns= ["col1","col2","col3"]

print(DataF)
print()

#--- modificando por columnas

DataF.rename(columns={"col2": "c2"})
print(DataF)