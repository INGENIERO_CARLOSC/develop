#================================================
#-----INDICES JERARGICOS ---
#================================================

from operator import index
import pandas as pd
import numpy as np
'''
Es una forma de poder trabajar tablas de mas de dimneciones
'''
peliculas= pd.DataFrame(
    { #las llaves del diccionario son los totulos de las columnas y los onmdex son los indices de las filas 
        'valoracion':[6,None,8.75, None],
        'Presupuesto': [160,250,100,None],
        'Director':['Peter jacson', 'Carlos cogua', 'Martin scosesse','albert ens']
        }, index = [[2014,2014,2013,2013],['Gozilla','El hobit 3', 'El lobo de wallstreet', 'gravity']]#indice de 2 niveles
)
print("Dataframe general")
print(peliculas)
print()

#-----Indexacion total
print("Indexacion total / trae todos los valores corrspondientes a Gozilla del 2014  .loc[(2014, 'Gozilla')]")
print()
print(peliculas.loc[(2014, 'Gozilla')])
print()

#---indexacion parcial o general muestra todas las oncidencias de peliculas del 2014
print("indexacion parcial o general  .loc[2014]")
print(peliculas.loc[2014])
print()
print("------------------------------------------------------------------------------------------------------------")

#================CONVERTIR INDEX EN COLUMNAS =====solo ejemplo de practica =============
'''
Para convertir los indices de filas a contenido de la traba usamos unstack()
no es aconsejable esolo se usara como parctica
'''
print(" Para convertir los index a encabezados columns   usamos unstack()")
print("------------------------------------------------------------------------------------------------------------")
pelicula2 = peliculas.unstack()
print(pelicula2)
print("-------en este caso volveri a la normalidad la tabla de ejemplo-----------------------------------------------")
print("Pasar los columns o encabezado de columna a encabezados indexz  de filas con stack()")
print(pelicula2.stack())
print()