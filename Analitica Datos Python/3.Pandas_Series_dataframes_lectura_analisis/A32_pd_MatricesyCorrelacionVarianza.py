#==========================================
#----Matrices y  correlacion de Varianza
#=========================================
import pandas as pd

#******************LEYENDO EL ARCHIVO CSV*****************************
Tabla = pd.read_csv('Materia_ciclo1\Semana 4\Clase 3\Soluciones\cotizacion.csv', sep= ';' , nrows=6)
'''
pandas facilita el uso de matrices de correlacion y covarianza con las funciones

-corr
-cov

'''

print("==============================")
print("----------Tabla original-----")
print("==============================")
print(Tabla)
print("*********************************************************************************************")

#---Matriz de correlacion
print("Matriz de correlacion ")
print(Tabla.corr())
print()
#---Matriz de covarianza
print("Matriz de covarianza")
print(Tabla.cov())

