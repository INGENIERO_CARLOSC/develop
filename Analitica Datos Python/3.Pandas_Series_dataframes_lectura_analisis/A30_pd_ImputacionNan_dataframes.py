#==============================================================
#----Imputacion de Registros con missings values -----------
#=============================================================
import pandas as pd

#******************LEYENDO EL ARCHIVO CSV*****************************
Tabla = pd.read_csv('Materia_ciclo1\Semana 4\Clase 3\Soluciones\cotizacion.csv', sep= ';' , nrows=7)
print("==============================")
print("----------Tabla original-----")
print("==============================")
print(Tabla)
print("*********************************************************************************************")
'''
en casos donde no se quiera o no se pueda eliminar valores nulos Nan , en estos casos tendtremos que hacer
una imputacion de los mismos a una valor preestablecido, panas noda ofrecer el metodo filna con los siguientes paremtros:

axis =  decide si rellena el criterio por filas o colunas  1, 0
value = rellenas los valores nulos a un valor fijo
method = permitira establecer un citerio de relleno de la siguiente form
    - ffill: relleno en base a la observacion de los ultimos elementos no nulos
    -bfill: relleno en base a la observacion de los proximos elementos no nulos
limit = contador maximos de elementos inputados
'''

#-----Imputacion de valores a cero--
print("Imputacion de valores a cero  .fillna(0)")
print(Tabla.fillna(0))
print("******************************************************************************************")

#------------------------------------------------------------------------------------

print("==============================")
print("----------Tabla original-----")
print("==============================")
print(Tabla)
print("*********************************************************************************************")

#----imputacion de valores por valor ANTEIOR por columnas ---
print(" imputacion de valores por valor anterior por columnas .fillna(method= 'ffill') ")
print(Tabla.fillna(method= 'ffill'))

#-------------------------------------------------------------------------------------------
print("==============================")
print("----------Tabla original-----")
print("==============================")
print(Tabla)
print("*********************************************************************************************")

#----imputacion de valores por valor SIGUIENTE por columnas ---
print(" imputacion de valores por valor SIGUIENTE por columnas .fillna(method= 'Bfill') ")
print(Tabla.fillna(method= 'bfill' ,limit= 2)) # limit 3 hace que maximo 2 columnas mas tomen ese valor, lo podemos omitir quitandoloy se aplica a toda la tabla