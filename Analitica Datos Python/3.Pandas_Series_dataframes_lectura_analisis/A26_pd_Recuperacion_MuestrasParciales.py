#======================================================
#------Recuperacion de muestras parciales ----------
#======================================================

'''
Recuperar secciones de datos cuando la informacion es extremadamente grande 

hay dos tipos de muestras que pandas y R permiten

head: obtener muestra de inicio de la estrcutura
tail: obtener muestra final 

los dos parametros recibiran como parametro el numero de registro a recuperar
'''
import pandas as pd
import numpy as np

serie = pd.Series(np.arange(100))
print(serie)

print("*******************************")

dataf = pd.DataFrame(np.arange(100).reshape(10,10))
print(dataf)
print()
#======Recuperacion primeros datos de la serie
print(" Recuperacion primeros datos de la serie ")
print(serie.head()) # si no le espcifico la canridad de valores me mostrara 5 por default
print()

#======Recuperacion ultimos datos de la serie
print(" Recuperacion ultimos datos de la serie ")
print(serie.tail()) # si no le espcifico la canridad de valores me mostrara 5 por default
print()

#=======Recuperacion en Dataframes======
print(" Recuperacion en Dataframes 3 primeros datos ")
print(dataf.head(3)) # si no le espcifico la canridad de valores me mostrara 5 por default
print()

#======Recuperacion ultimos 5 datos del dataframe 
print(" Recuperacion ultimos 5 datos del dataframe ")
print(dataf.tail(5)) # si no le espcifico la canridad de valores me mostrara 5 por default
print()

