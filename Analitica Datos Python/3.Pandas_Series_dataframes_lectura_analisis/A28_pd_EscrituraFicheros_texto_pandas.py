#====================================================
#----------Escritura de archivos texto
#=================================================



'''
se hace uso de la funcion unica to_csv con las siguientes propiedades:

path: ruta del fichero que se usa para la escritura
sep: caracter utilizado como seprardor de campo
na_rep: cadena que se debe usar como representaciond e campos Nan
float_format: indicador de datos para valores flotantes
columns: secuencia de seleccion del conjunto de columnas que se desea volcar al fichero
header: flag o secuencia de cadenas que indica si se debe volcar la cabecera al fichero
index: Flag que indica si elindic de la columna debe ser agregada como una columna mas al fichero 
index_label: Nombre que se le debe dar a la columna de indice del fichero
mode: modo de apertura del fichero , por defecto w
encoding: codificacion a utilizar en la escritura del fichero
date_format: indicador forma de utilizar para escribir fechas 
decimal: caracter a utilizar como separador de decimales

NOTA: pantas funciona con archivos de tipo:
binarios: pickle
binarios: Hdf5
Ficheros excel
'''

import pandas as pd 
import numpy as np

#******************LEYENDO EL ARCHIVO CSV*****************************
LoteriaM = pd.read_csv('Archivos para analisis\csv\Resultados_de_los_sorteos_loter_as.csv', sep=(','))
print(LoteriaM.head())

#*******************ESCRITURA EN EL ARCHIVO CSV********************************indecide de filas falso para que no lo guarde 
LoteriaM.to_csv('Archivos para analisis\csv\Resultados_de_los_sorteos_loter_as.csv', sep=',' , index= False)
