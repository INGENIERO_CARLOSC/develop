#===========================================
#------Elementos unicos y Frecuencias-------
#===========================================

'''
unique: nos devuelve un array con el conjunto de elemntos unicos de una serie 
value_counts: que realiza un calculo de frecuencias sobre elementos unicos de una serie 
isin: nos permit revisar si un conjunto de valores se encuentra en unsa serie
'''

import pandas as pd
import numpy as np

#******************LEYENDO EL ARCHIVO CSV*****************************
Tabla = pd.read_csv('Materia_ciclo1\Semana 4\Clase 3\Soluciones\cotizacion.csv', sep= ';' , nrows= 7)

print("==============================")
print("----------Tabla original-----")
print("==============================")
print(Tabla)
print("*********************************************************************************************")

#-- conjunto de columna especifica sin ginorando repeticiones solo muestra unicos 
print("conjunto de columna especifica / nos devuelve un array con el conjunto de elemntos unicos de una serie")
#  dataframe. nombre column, funcion 
print(Tabla.Nombre.unique()) # volumen es el nombre de la columna seleccionada
print()

#--Tabla de frecuncia cuantas veces se repite el valor de columna 1 
print("Tabla de frecuncia cuantas veces se repite el valor de columna 1  ")
#  dataframe. nombre column, funcion 
print(Tabla.col1.value_counts())
print()

#..criterio de existencia de valores 
print("criterio de existencia de valores / me muestra en la columna nombre que filas cumplen con la condicion del nombre AMADEUS")
#----------columna------------valor de busqueda
print(Tabla['Nombre'].isin(['AMADEUS']))
print()