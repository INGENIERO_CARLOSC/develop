#=====================================================
#----Indexacion y Slicing Pandas---------------------
#====================================================

import numpy as np
import pandas as pd


#Indexacion por atricuto de clave
print("Indexacion por atricuto de clave")

seriEjemplo = pd.Series([1,2,3,4], index=['a','b','c','d'])
print(seriEjemplo)
print()
dataframEjemplo = pd.DataFrame(np.arange(16).reshape(4,4), index=['f1','f2','f3','f4'], columns=['c1','c2','c3','c4'])
print(dataframEjemplo)
print()
print()
print()

print("mostrando valores por indexacion en series")
print(seriEjemplo.d)

print("mostrando valores por indexacion en dataframe columna c3")
print(dataframEjemplo.c3)

#----Obteniendo valores con Slicind  enseries 
print("obteniendo valores de series con el valor del indice [0] ")
print(seriEjemplo[0])
print()

print("octeniendo el valor con el nombre la clave  [b]")
print(seriEjemplo['b'])
print()

#---obteniendo el valor con intervalos en la series [0:2]
print("obteniendo el valor con intervalos en la series [0:2] ")
print(seriEjemplo[0:2])
print()

#--obteniendo el valor con intervalos de clave

print("obteniendo el valor con intervalos de clave ['a':'c'] ")
print(seriEjemplo['a':'c'])
print()

#--obteniendo varios elementos por clave especifica
print("obteniendo varios elementos por clave especifica [['a','c']] ")
print(seriEjemplo[['a','c']])
print()

#========DATAFRAMES===== aqui solo se puede hacer busqueda por columnas
print(" dataframes aqui solo se puede hacer busqueda por columnas dataframEjemplo['c4']")
print(dataframEjemplo['c4'])
print()

#===================================================
#---INDEXACION CON METODO LOC---------ILOC----------
#====busqueda por condiciones o por claves==========

#---LOC es busqueda por claves 
print("Usando loc para mostar un contenido especifico de una serie seriEjemplo.loc['b']")
print(seriEjemplo.loc['b'])
print()

#---Buscando por rangos 
print("Buscando por rangos  .loc['b': 'd'] ")
print(seriEjemplo.loc['b': 'd'])
print()

#--buscando elementos especificos 
print("buscando elementos especificos  .loc[['b', 'd']] ")
print(seriEjemplo.loc[['b', 'd']])
print()

#-- buscando con condiciones booleanas
print("buscando con condiciones booleanas ")
print(seriEjemplo.loc[[True,True,False,True]])
print()

#---Buscando en dataframes por filas-----
print("Buscando en dataframes por filas colocamos el nombre del index de fila .loc['f1']) ")
print(dataframEjemplo.loc['f1'])
print()

#----buscar con coordenadas filas y columnas en dataframes
print("buscar con coordenadas filas y columnas en dataframes .loc['f2', 'c2'] valor en fila2 y columna 2")
print(dataframEjemplo.loc['f2', 'c2'])
print()

#---buscar todas las filas pero solo 1 columna 
print("buscar todas las filas pero solo 1 columna  ")
print(dataframEjemplo.loc[:, 'c2'])
print()

#==================================================
#-----iloc posiciones ----------------------------
#==================================================

#--Series--------
print("busqueda de elementos con iloc en Series ")
print(seriEjemplo.iloc[0])
print()

#--intervalos 
print("busqueda de elementos con iloc en Series indices   .iloc[0:2]")
print(seriEjemplo.iloc[0:2])
print()

#--elementos especificos - no se puede colocar fila y columna 
print("busqueda de elementos con iloc en Series indices   .iloc[0:2]")
print(seriEjemplo.iloc[[0,2]])
print()


#--- DATA FRAMES---busqueda de filas y columnas---

print("Mostrar fila iloc[3] ")
print(dataframEjemplo.iloc[3])
print()

#--Mostrar fila 3 y columna 2 .iloc[3,2]
print("Mostrar fila 3 y columna 2 .iloc[3,2] ")
print(dataframEjemplo.iloc[3,2])
print()

#--Todas las columnas hasta la fila 2
print("Todas las columnas hasta la fila 2 .iloc[:2]")
print(dataframEjemplo.iloc[:2])
print()








