#===============================================================================
#-------------------PANDAS-----------------------------------------------------
#======SERIES ================DATAFRAMES/ TABLAS=====solo puede ser de un tipo de valor===
'''
SE PUEDE INTEGRAR CON OTRAS LIBRERIAS COMO 
statsmodel, Scipy, Numpy y Scikit- learn

modulo de alto rendimiento puede ser mejor haciendo uso de Cpython (permite integrar extenciones de C en python)
manejo de vectores, dataframes etc 

diferencia entre matriz y dataframe o tabla

la matriz debe contener todos los dato del mismo valoe / la tabla puede tener el mismo tipo pero por columnas las columnas pueden tener contenido diferente
'''
#---Series -- INFORMACION UNIDIMENCIONAL
#-- DataFrame --Informacion Tabular

import pandas as pd
import numpy as np

#====================================================
#-----Creacion--SERIES -----unidimencional ------------------
#====================================================

#----funciones 

'''
para crear contamos con uns constructur (Series) que recibe los siguientes parametros

data = es obligatrio, contiene los datos que queremos cargar en la serie, escalar , secuencia de un Narray de nunpy unidimencional
index = Es opcional, contiene las etiquetas que queremos asignar en la serie y prodra ser un valor escalar o secuencia de un Narray de nunpy unidimencional
dtype = podra ser cualquier tipo de datos de Numpy

'''

#--- serie desde escalar 
print(" serie desde escalar  ")
serie = pd.Series(5)
print(serie)
print()

#--- serie desde Secuencia  
print(" serie desde Secuencia    ")
serie = pd.Series([1,2,3,4,5])
print(serie)
print()

#--- serie desde Ndarray 
print(" serie desde Narray  de numpy  ")
array = np.array([1,2,3,4,5])
serie = pd.Series(array) # almacenando el array de numpy en la serie de pandas
print(serie)
print()

#----tipando la serie, si no especifico el dtype pandas siempre me colocara int64 por defecto en este caso pusimos string
print("Typando serie string")
seriestring = pd.Series([1,2,3,4,5,6,7,8,9,10], dtype= np.string_)
print(seriestring)
print()

#Serie con indice preestablecido  / los index por defecto estan numeroados de 0 al infinito segun los datos pero podemos modificarlos
print("Serie con indice preestablecido  ")
serie1 = pd.Series([1,2,3,4,5], index = ["dato1","dato2","dato3","dato4","dato5"])
print(serie1)
print()

#serie desde diccionarios Python (el indice se establecera desde la calve del diccionario)
print("serie desde diccionarios Python (el indice se establecera desde la calve del diccionario) ")
seriedicc = pd.Series({
    'estatura': 2,
    'edad': 30,
    'teefono': 3133485276
}, dtype=np.int64)

print(seriedicc)

#===============================================================
#------------------Elementos de una serie ---------------------
#==============================================================
print("-------Elementos de una serie ---  ")
ElementosS= pd.Series([1,2,3,4,5], index=['a','b','c','d','e'], dtype=np.float64)
print(ElementosS)
print()
#--------Valores de una Serie--
print("Valores de una Serie")
print(ElementosS.values)
print()

#-----ver indices una serie -
print("ver indices una serie")
print(ElementosS.index)
print()

'''
los indices son inmutables, lo que impide que cambiemos el valor de un indice de forma independiente, 
sin embargo podemos cambiar un indice completo por otro
'''
#----modificando un indice
#ElementosS.index[0]=4
print("modificando un indice")
print("no se puede")
print()

#---modificando todos los indices
print(" modificando todos los indices")
ElementosS.index=['J','K','L','M','N']
print(ElementosS.index)