#============================================================
#--------------Lectura de ficheros en formato texto---------
#============================================================

'''
read_csv : ficheros separadops por comas 
read_table: ficheros separados por tabuelaciones .tsv
read_fwf: ficheros de ancho fijo 


PRAMETROS COMUNES MAS USADOS:

-path :  ruta del fichero del que se va a relizar la lectura
-sep:    caracteres que se usan como separador del fichero
-header: indice de la fila que contiene el nombre de las columnas (None en caso de no haber)
-index_col: indice de la columna o secuencia de indices que se deben usar como indice de fila de los datos
-skiprows: NUmero de filas o secuencia de indices de la fila que se deben ignorar en la carga
-names: Secuencia que contiene los nombres de las columnas en caso de no haber None
-na_values: seucencia de valores que de encontrarsen en el fichero, deben ser tratados como Nan
-dtype: Diccionario en el que las claves seran el nombre de las columnas y los valores seran de tipo
numpy 
-parse_dates: flag que indica si python debe intentar parsear datos con formajo semejante a las fechas con fechas
puede contener un listado de nombres de columnas que deberan unirse  para el parseo con fechas
-converters: Diccionario en el que las claves sera el nombre de las columnas y los valores seran
funciones que deberan aplicar contenido a las dichas columnas durante la carga
-dayfirst: indica si al parsear fechas de bede indicar primero el dia o el mes 
-nrows:  NUmero de filas a leer desde el principio del fichero
-chunksize: tamaño a usar para la lectura incremental delfichero 
-skip_footer: Numero de filas a ignorar al final del fichero
-enconding:  Codificacion a esperar del fichero leido 
-squeeze: flag que indica si los datos leidos solo tienen una columna, el resultado sea una serie en lugar de un dataframe
-thousands: caracter a utilizar para separador de miles
-decimal: caracter a utilizar para separador decimal
'''

import pandas as pd

#medellin = pd.read_csv('Archivos para analisis\csv\RESULTADOS_LOTERIA.csv', sep=(','))
#print(medellin.head())
#print("*****************************************")

loterias = pd.read_csv('Archivos para analisis\csv\Resultados_de_los_sorteos_loter_as.csv', sep=(','))
print(loterias.head())


print("////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")


#cuando un documento tiene cabecera hacemos la lectura anterior pero cuando el documento no tiene cabecera usamos 
print("cuando un documento tiene cabecera hacemos la lectura anterior pero cuando el documento no tiene cabecera usamos y los indices de columna se vuelven numeros consecutivos  ")
loterias = pd.read_csv('Archivos para analisis\csv\Resultados_de_los_sorteos_loter_as.csv', sep=(','), header= None)
print(loterias.head())
print("#######################################################################################################################################")
#--------------Rebautizando encabezados de tablas ----
#obteniendo las columnas en una lista las podemos udar en el ejepmlo 3 para revautizar las tablas que no tienen encabezado  
columnas = list(loterias.columns)
otrosnombres = list(['col1','col2','col3','col4','col5','col6','col7','col8'])
print("cuando un documento tiene cabecera hacemos la lectura anterior pero cuando el documento no tiene cabecera usamos y los indices de columna se vuelven numeros consecutivos  ")
loterias = pd.read_csv('Archivos para analisis\csv\Resultados_de_los_sorteos_loter_as.csv', sep= ',', header= None, names= otrosnombres)
print(loterias.head())