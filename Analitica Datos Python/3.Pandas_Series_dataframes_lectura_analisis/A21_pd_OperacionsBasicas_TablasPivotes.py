#================================================
#---Operaciones Basicas / Tablas y Pivotes
#================================================
from msilib.schema import Directory
import numpy as np
import pandas as pd
'''
estos metodos de pivote hace que sea mucho mas facil moverse en las tablas para realizacion analisis
'''
peliculas= pd.DataFrame(
    { #las llaves del diccionario son los totulos de las columnas y los onmdex son los indices de las filas 
        'año': [2014,2014,2001,2013,2011],
        'valoracion':[6,None,8.75, None,8.9],
        'Presupuesto': [160,250,100,None,93],
        'Director':['Peter jacson', 'Carlos cogua', 'Martin scosesse','albert ens','michael jordan'],
        'titulo': ['Gozilla','El hobit 3', 'El lobo de wallstreet', 'gravity','lord of the right']
        }
)
print("Dataframe general")
print(peliculas)
print("=======================================================================================")

#---Filas : Año  / Columnas: director / valores: titulo
#--------(index ,  columns , valores)
print("(index ,         columns ,          values)                / pivoteo ")
print("Filas : Año  / Columnas: director / valores: titulo ")
print("=========================================================================================")
print(peliculas.pivot('año','Director','titulo'))
print("-------------------------------------------------------------------------------------------")
print()


#En este caso  añadio a año y director como indices de las filas y los demas datos siguienron siendo columnas
print(" .set_index(['año','Director'] Establecemos el indice a las dos variables sobre las que queremos pivotear")
pivotear = peliculas.set_index(['año','Director'])
print(pivotear)
print()

#---.unstack() Pasamos el ultimo nivel de de indice de filas a oclumnas -------------------------------------------
print(" .unstack() Pasamos el ultimo nivel de de indice de filas a oclumnas ")
print(pivotear.unstack())
print()
#-------------------------------
print("Elejimos unicamente el valor de la columna titulo")
print(pivotear['titulo'])
print("#-------------------------------")

#*************Hacer agregaciones o modificaciones de una colmuna de la tabla*****************
print("----Hacer agregaciones --Todas las peliculas del 2001 actualizala a 2014-.loc[peliculas['año']== 2001, 'año']= 2014---")
peliculas.loc[peliculas['año']== 2001, 'año']= 2014
print(peliculas)

#********************
print("**********************Dataframe Original ***********************************")
print(peliculas)
print("***************Dataframe pivoteado ****Por año y director********************")
#------vitotear--------
print( pd.pivot_table(
    # tabla    ,  valores 
    peliculas, values='Presupuesto', 
    index=['Director'], #nombre de las filas 
    columns=['año'], # Nombre de las columnas 
    aggfunc= np.sum)) #suma del presupuesto de las peliculas con numpy



