#=====================================================
#---Resumen de datos y estdisticos basicos------------
#====================================================


import pandas as pd

#******************LEYENDO EL ARCHIVO CSV*****************************
Tabla = pd.read_csv('Materia_ciclo1\Semana 4\Clase 3\Soluciones\cotizacion.csv', sep= ';' , nrows=6)
''''
Funciones: 

describe = Presenta un conjunto con la estadistica basica mas comunes sobre las columnas de la tabla 
equivalente a la funcion summary de R
count = Numero de elementos habiles o no nulos 
min, max = valor minimo y maximo 
argmin, argmax, idxmax, idxmin = pocisiones con valor minimo y maximo
quantile = cuantil calculado 
sum =  suma de elementos
mean = media aritmetica de los elementos
median =  mediana de los elementos
std: desviacion estandart de los elementos
var =  varianza de los elementos
cumsum = suma acomulada de elementos
cumprod = producto acumulado de elementos


La mayor parte de estos metodos pueden recibir 3 parametros

axis=  indica si el calculo es por filas o columnas 1,0
skipna = indica si se deben ignorar valores Nan a la hora de realizar los calculos
'''


print("==============================")
print("----------Tabla original-----")
print("==============================")
print(Tabla)
print("*********************************************************************************************")

#---describe()
print("describe = Presenta un conjunto con la estadistica basica mas comunes sobre las columnas de la tabla")
print(Tabla.describe())
print()

#--suma por columnas
print("suma por columnas ")
print(Tabla.sum())
print()

#--suma por filas ignorando Nan
print("suma por filas ignorando Nan ")
print(Tabla.sum(axis= 1, skipna= True))
print()

#---mean---
print("Metodo mean media aritmetica de los elementos")
print(Tabla.mean())

