#=============================================
#----Ordenamiento de DataFrames----
#=============================================

from matplotlib.pyplot import prism
import pandas as pd
import numpy as np

data = pd.DataFrame(np.array([1,3,5,7,9,8,6,4,2]).reshape(3,3), index=['f3','f2','f1'], columns=['c2','c1','c3'])
print("Dataframe original derodenado en indices columns y valores")
print(data)
print("**********************")
#=================Indices=========

#ordenamiento por indices de filas  solo ordena la posicion de filas sin alterar el orden de su contenido
print("ordenamiento por indices de filas  solo ordena la posicion de filas sin alterar el orden de su contenido")
print(data.sort_index())
print()

#ordenamiento por indices  solo ordena la posicion de columnas sin alterar el orden de su contenido
print("ordenamiento por indices  solo ordena la posicion de columnas sin alterar el orden de su contenido")
print(data.sort_index(axis= 1)) #axis =1 especifica que son exclusivamente columnas
print()

#ordenamiento descendente por indice de filas 
print("ordenamiento descendente por indice de filas de orden  ")
print(data.sort_index(ascending= False))
print()

print("******************************************************************")

print("Dataframe original derodenado en indices columns y valores")
print(data)
print("*****************")
#=========Valores ===============

#ordenamoento por valores de las filas 
print("ordenamiento por valores de fila especifico fila 1  .sort_values(['f1'], axis=1) / las demas filas copian el orden de la fila 1")
print(data.sort_values(['f1'], axis=1))
print()

#ordenamoento por valores de columnas
print("ordenamiento por valores columnas columna 2  .sort_values(['c2']) las demas columnas copian el reordenamiento de la columa 2")
print(data.sort_values(['c2']))
print()