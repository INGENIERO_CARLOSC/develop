#====================================================
#-----Operaciones entre estrcuturas Pandas-----------
#====================================================
 
''' Division escalar en dataframe 
1:22:03
 https://www.youtube.com/watch?v=vnBjUEKl4Vw

'''
import numpy as np
import pandas as pd
#serie
Sejemplo = pd.Series([1,2,3,4], index= ['a','b','c','d'])

Datafa= pd.DataFrame({ #dataframe con un diccionario de sieres  creado apartir de una serie especifica
    'val1': pd.Series(Sejemplo, dtype= np.int32), #hizo el llenado con numeros enters
    'val2':  pd.Series(Sejemplo, dtype= np.string_) # hizo el llenado con cadena de caractres
    })
#--Ignorar la b que sale porque es una forma de representacion por default de pandas y no causa problemas
print(Datafa)
print()

#-------------------------------------------------------------------------------------------------
#-- creando ua copia de una serie 
print("creando una igualdad de de una serie ")
serienueva = Sejemplo[:] #usar esta tecnica es buena para evitar la sobreescritura o cambios en la serie original
print(serienueva)
print()

#--- cambiando un valor de la serie con el indice 
print("cambiando un valor de la serie con el indice")
serienueva['e'] = 7
print(serienueva)
print()

#-----creando una igualdad de  un dataframe y agregando una columna nueva con valores
print("creando una igualdad de un dataframe y agregando una columna nueva con valores")
datagrameNuevo = Datafa # copiando el dataframe 
datagrameNuevo['var3'] = [10,20,30,40] #columna nueva con valores int 
print(Datafa)

# --suma de dataframes deshiguales
print("suma de dataframes deshiguales- cuando ua serie tiene mas valores que otra por defecto se pone NAN")
print(Sejemplo+serienueva)
print()

#-suma de dataframes
'''
en teoria Datafa no tiene la tercera columna dataframenuevo si por que hicimos la modificacion
pero cuando ponemos datagrameNuevo = Datafa   no estamos creando una copia independiente
sino que esta creando una igualdad y todos los cambios que le hagamos a datagrameNuevo 
el dataframe original tambien se modificara por eso en la impresion deberia salir 
var3 = 10,20,30,40 y en realidad sale 20,40,60,80 

para evitar que los cambios realizados en  datagrameNuevo se puede usar la tecnica [:] como en las series
'''

print("pasa o mis mo que las seires ")
print(Datafa+datagrameNuevo)
print()

#======================================
#--------TRANSPOSICION DATAFRAMES----
#======================================

#--cambiar filas por columnas 
print("cambiar filas por columnas ")
print(datagrameNuevo.T)
print()

#==============================================
#-------Funciones de numpy---------------------
#==============================================

#--operaciones sobre series 
'''
con dataframe nunca podremos indicar axis ni filas ni columas todo funciona por columnas
'''
print("operaciones sobre series/ raiz cuadrada ")
print(np.sqrt(serienueva))
print()

#-operaciones sobre dataframes
print("operaciones sobre dataframes ")
print(np.sum(Datafa))
print()

#-operaciones sobre dataframe especificando eje --se puede pero hay que tener cuidado por que no es seguro manejar por filas
print(" operaciones sobre dataframe especificando eje")
print(np.sum(Datafa, axis =1))
print()

