#===================================================
#---Trataiento de Dataframes y Series como Narays--
#==================================================

'''
con pandas en las series y dataframes se pueden realizar todas la operaciones que se realizan 
en un narray d numpy.

Como un narray no puede mexclar elementos de diferente tipo y un dataframe si , alfunas operaciones 
en dataframe estaran supeditadas a que todas las columnas tengan el mismo tipo
'''
import numpy as np
import pandas as pd
#serie
Sejemplo = pd.Series([1,2,3,4], index= ['a','b','c','d'])

Datafa= pd.DataFrame({ #dataframe con un diccionario de sieres  creado apartir de una serie especifica
    'val1': pd.Series(Sejemplo, dtype= np.int32), #hizo el llenado con numeros enters
    'val2':  pd.Series(Sejemplo, dtype= np.string_) # hizo el llenado con cadena de caractres
    })
#--Ignorar la b que sale porque es una forma de representacion por default de pandas y no causa problemas
print(Datafa)
print()
#============================================
#-----Consultas de composicion---------------
#============================================

#--Consulta del tipo de almacena de una serie 
print("Consulta del tipo de almacena de una serie ")
print(Sejemplo.dtype)
print()

#-- consulta de los tipos almacenados en un dataframe
print(" consulta de los tipos almacenados en un dataframe ")
print(Datafa.dtypes)
print()

#--consultar numero de dimenciones de una serie 
print(" consultar numero de dimenciones de una serie ")
print(Sejemplo.ndim)
print()

#--consultar numero de dimenciones de un dataframe
print("consultar numero de dimenciones de un dataframe")
print(Datafa.ndim)
print()

#-consulta de la forma de una serie 
print("consulta de la forma de una serie ")
print(Sejemplo.shape)
print()

#-consulta de la forma de un Dataframe
print("consulta de la forma de un Dataframe")
print(Datafa.shape)
print()

#-consulta del tamaño o numero de elementos de una serie
print("consulta del tamaño o numero de elementos de una serie ")
print(Sejemplo.size)
print()

#-consulta del tamaño o numero de elementos de un dataframe
print(" consulta del tamaño o numero de elementos de un dataframe")
print(Datafa.size)
print()

#==========================================================
#--Operaciones con escalares-------------------------------
#==========================================================

#suma de series y escalar 
print("suma de series y escalar ")
print(Sejemplo + 2)
print()

#divion de serie escalar
print("divion de serie escalar ")
print(1/ Sejemplo )
print()

#multiplicacion escalar con Dataframe 
print(" multiplicacion escalar con Dataframe  ")
print(Datafa *2)
print()

# Division escalar en dataframe 
#print("Division escalar en dataframe ")
#print(1 / Datafa)
#print()
