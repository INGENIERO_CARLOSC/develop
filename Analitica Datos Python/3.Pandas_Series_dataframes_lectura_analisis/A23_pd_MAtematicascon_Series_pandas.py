#==========================================================
#-----Operaciones aritmeticas con estrcuturas  Pandas 
#==========================================================

import pandas as pd
import numpy as np

serie1 = pd.Series([1,2,3,4], index=['a','b','c','d'])
serie2 = pd.Series([5,6,7,8], index=['c','d','e','f'])
print("serie1")
print(serie1)
print("serie2")
print(serie2)
print()


#--operacion basica de suma de elementos
print("SOlo suma los valores que tienen indice en comun en este caso d y e los demas los rellena con NA")
print(serie1 + serie2)
print()

#--operacion con pandas suma de elementos la diferencia es que add me permite sustitituir valores nan por 0
print("operacion con pandas serie1.add(serie2) ")
print(serie1.add(serie2))
print()

#---usando add y fill_values para sustitur valores nan---------------
print("usando add y fill_values para sustitur valores nan .add(serie2, fill_value= 0)")
print(serie1.add(serie2, fill_value= 0))
print()

#--------------------------------------
print("#------Resta--------------------------")
print(serie1.sub(serie2, fill_value= 0))
print()

#--------------------------------------
print("#-----Multiplicacion---------------------")
print(serie1.mul(serie2, fill_value= 0))
print()

#--------------------------------------
print("#--------Division------------------------")
print(serie1.div(serie2, fill_value= 0))
print()