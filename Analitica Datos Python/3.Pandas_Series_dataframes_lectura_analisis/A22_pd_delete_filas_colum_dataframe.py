#==============================================
#---Eliminaciond e filas y columnas con pandas
#==============================================
import numpy as np
import pandas as pd

seriEjemplo = pd.Series([1,2,3,4], index=['a','b','c','d']) #serie con 4 valores si encabezado de felas

DataEjemplo =pd.DataFrame(np.arange(16).reshape(4,4),  #dataframe rellenado con un array de numero del 0 al 15 en una estrcutura de 4x4
#encabezados de la tabla index y columns
index=['f1','f2','f3','f4'],  #asiganando en valor de las filas de la tabla
columns=['c1','c2','c3','c4']) #asignando el valor de las columnas de la tabla
print("*******Serie**********************")
print(seriEjemplo)
print("***********Data frame *****************")
print(DataEjemplo)
print("***************************************")

#==============================================
#--Eliminacion de valores de una serie
#==============================================
print("eliminando valores  b de la serie con drop('b')")
print(seriEjemplo.drop('b'))
print()

#otra froma de eliminar con del
print("otra froma de eliminar con del seriEjemplo['b']")
del seriEjemplo['b']
print(seriEjemplo)

#==================================================
#==============Eliminacion de filas de un Dataframe
#==================================================

print("Eliminacion de filas de un Dataframe .drop('nombre de la fila')")
print(DataEjemplo.drop(['f2']))
print()

#--- eliminacion de una columna del dataframe 
print("Eliminacion de columnas de un Dataframe .drop('c1', axis=1)") #columna c1 y axis =1 significa que es una columna
print(DataEjemplo.drop(['c1','c2','c4'], axis=1 )) #elimine 3 columnas 