import numpy as np
#=================================================================================
#-------------------Indexacion Slicing Basico ---[a,b,c]---------------------------------
#=================================================================================
print()
print("--Indexacion Slicing Basico ---[a,b,c]-[inicio:fin:intervalo]- ")
print("Arreglo original con indices")
print("---0---1---2---3---4---5---6---7---8---9")
i_arreglo = np.array([1,11,3,100,12,34,67,99])
print(i_arreglo)
print()
#-------Indexacion con primer parametro-----
print("Indexacion con primer parametro/ motrando contenido del indice 3")
print(i_arreglo[2])

#-------Indexacion con primer y segundo  parametro-----
print("Indexacion con primer parametro desde el indice 2 hasta un indice menos del 5")
print(i_arreglo[2:5])

#---Indexacion con tercer parametro ----
print("Indexacion con tercer parametro muestra los valores en intervalos de 2 indices contando el primero ")
print(i_arreglo[::2])

#---Indexacion con tercer parametro negativo ----
print("Indexacion con tercer parametro negativo, invierte los valores  ")
print(i_arreglo[::-1])

#==============================================================================
print()
'''
En  Narrays Multidimencionales hay 2 formas de acceso 

Indexacion recursiva = array[a:b:c en dim_1][a:b:c en dim_2]...[a:b:c en dim_..]
Indexacion con comas = array[a:b:c en dim_1, a:b:c en dim_2, a:b:c en dim_...]
'''
print("--------------------MAtiz a--0--------------------matriz b---1---------------")
arregloM = np.array([[[1,2,3,4],[5,6,7,8]],[[9,10,11,12],[13,14,15,16]]])
print(arregloM)

print("forma de la matriz")
print("2 matrices, 2 filas, 4 columnas")
print(arregloM.shape)

print("indexacion recursiva de primer nivel, muestra la segunda matriz con indice 1")
print(arregloM[1])

print("Indexacion recursiva de segundo nivel, muestra el array 2 y solo la fila 0 o primera fila")
print(arregloM[1][0])

print("Otra forma de visualizarlo ")
print(arregloM[1,0])

print("Indexacion recursiva de tercer nivel, muestra segunda matriz , la primera fila y el elemento 3 ")
print(arregloM[1][0][3])
print("Indexacion recursiva de tercer nivel, otra forma de programar ")
print(arregloM[1,0,3])

#=====================================================================
#----------Indexacion con Slices -------------------------------------
#====================================================================
print("Arrreglo original del ejemplo  Array 0 y array 1")
print("ind-0--1--2--3--4")
print(arregloM)
print()
print("Indexacion recursiva con Slice de tercer nivel")
print("Array 0, fila 0, valores entre el index 0:2")
print(arregloM[0][0][:2])
print("otro ejemplo, array 1, fila 1 y elementos entre rabgos de index 2 al 4 -1")
print(arregloM[1][1][2:4])

print("-------------------------------Matrices originales------------------")
print(arregloM)
print("indexacion recursiva de tercer nivel con negativos")
print("muestra el array 1 fila 1 y los numeros invertidos de 1 en uno")
print(arregloM[1][1][::-1]) #puedo escojer los intervalos dentro de la fila dentro d eun array especifico


#===========================================================================
#-------Modificacion de ocntenido con indexacion Slicing  ------------------
#===========================================================================

print()
print("-Modificacion de opntenido con indexacion posiciones -")

matriz = np.array([[1,2,3,4],[5,6,7,8]])
print("matriz original")
print(matriz)
#--- modificando por posiciones 
print("-- modificando por posiciones -  en posicion 0/1 reemplce con un  5 ")
matriz[0][1] = 5 # en posicion 0/1 reemplce con un  5
print(matriz)

#------Monificacion obtiendo contenido de la matroz con slicing
print()
print("Monificacion obtiendo contenido de la matroz con slicing, en lafila 0 coloque 100 cada 2 indices")
matriz[0][::2] = 100 # en posicion 0/1 reemplce con un  5
print(matriz)
