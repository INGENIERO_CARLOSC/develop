import numpy as np 
#=================================================================
#-------------Indexacion y Slicing con Booleanos----------------
#=================================================================

#array personas string
personas = np.array(["Miguel","Juan","Carlos","Maria"])
print(personas)
#numeros aleatorios en una matriz
datos = np.random.rand(4,4)
print(datos)

#--- Indexacion con slincs boolenanos obre valores
print()
print("Mascara boolena")
print(datos <0.7)
print("Muestra solo los valores verdaderos segun la validacion")
print(datos[datos<0.7])

#--- Mascara booleana --
print()
print(personas)
print("Mascara booleana")
print(personas=='Carlos')
print()
print("Mostrando valores con slincing masacara indexacion")
#me muestr los datos de la matris datos en la posicion MIguel de la otra matriz es decir elementos de la fina 0
print(datos[personas == "Miguel"])
print()
# Miguel es el indice 0 de la matriz personas / entonces de la matriz datos me muestra los datos la que este en la posicion 0 de 2 en dos
print("Mostrando valores por indexacion con slicings / mascara y basi convinado")
print(datos[personas == 'Miguel', ::2])

#Mostrar valores diferentes de un dato 

print()
print(" Mostrar valores diferentes de un dato ")
#datos de la matroz datos diferentes al indice 0 o miguel
print(datos[personas != "Miguel"])

#=========================================================
#--------Modificando valores por indexacion ----
#=========================================================
print()
print("--------Modificando valores por indexacion ----")
array_in = np.random.randn(7,4)
print(array_in)

# eliminacion de valores negativos con slicings
print()
print(" # eliminacion de valores negativos con slicings")
array_in[array_in<0] = 0
print(array_in)
print()

