#=========================================================
#---------------Operaciones sobre Narray Booleanos ------
#=========================================================
import numpy as np
array = np.random.randn(5,5)
print("Array otiginal")
print(array)
print()

#--------elementos mayores que cero 
print(" Sumando los elementos mayores que cero  ")
print((array > 0).sum())
print()

#--------elementos menores que la media 
print("media: ",array.mean())# mostrando la media
print()

print(" Sumando los elementos menores que la media  ")
print((array <array.mean()).sum())
print()

#=======================================================
#--------------Funciones de chequeo---------------------
# ==============ANY======ALL=============================

'''
para tratar arrays booleanos numpy tiene dos funciones de chqueo
any = Para comprobar si ALGUNO de los elementos es True
all = Para comprobar si TODOS los elementos son True
''' 


matrizany = np.random.randn(5,5)
print("Matriz original")
print(matrizany)
print()


# si algunos de los elementos cunplen la condicion  / muestra solo los que cumplen 
print(" # si algunos de los elementos cunplen la condicion  ")
print((matrizany==0).any())

# Si todos los elementos cumplen la condicion / muestra toda la matriz
#muestra de forma booleana si hay elementos menores a -2 y mayores a 2  todos los los elementos
print("Si todos los elementos cumplen la condicion ")
print((matrizany>=-2 )&(matrizany< 2).all())

#=======================================================
#-------------ORDENAMIENTO DE NARRAYS-----------------
#=======================================================

print()
print("Datos ordenados / por filas")
print(np.sort(matrizany))
print()

print("Datos ordenaods segun el primer eje columnas  ")
print(np.sort(matrizany,axis= 0))
print()

