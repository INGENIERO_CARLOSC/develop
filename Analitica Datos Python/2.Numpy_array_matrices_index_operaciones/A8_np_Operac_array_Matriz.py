#==============================================================
#---------FUNCIONES UNARIAS -----------------------------------
#==============================================================

#son funciones que solo reciben como parametro un Narray

'''
abs, fabs = Valor apsoluto
sqrt =  Raiz cuadrada
square = Potencia al cuadrado
exp = potencia de e
log, log10 ,log2, log1p = logaritmos en distintas bases 
sing =Signo(+=1/-=-1/0=0)
ceil=  Techo
floor = suelo
rint = Redonde al entero mas cercano
modf= devuelve 2 arrays 1 con parte fraccionaria y el otro con parte entera
isnan = Devuelve un array booleano indicando si los valores son Nan o NO
isfinite, isinf = Devuelve un array booleano indicando si es finito o infinito
cos, cosh, sin, sinh, tan, tanh = Funciones trigonometricas
arcos, arcosh, arsin, arsinh, arctan, arctanh, Funciones tringonometricas inversas
logical_not = Inverso Booleano de todos los valore arrays
'''
import numpy as np
from scipy import rand
print("Valor absoluto numeros positivos funcion abs")
arreglo = np.array([-1,2.7,3.1,4,5])
#----abs----
print(np.abs(arreglo)) #pasa todos los valores a enteros positivos
print()
#---sign----
print(" #devuelve los signos funcion sign")
print(np.sign(arreglo)) #devuelve los sognos
print()
#--ceil--
print(" #Cuando hay decimales devuelve el entero superior funcion ceil")
print(np.ceil(arreglo)) 
print()



#==================================================================================
#----------FUNCIONES BINARIAS------------------------------------------------------
#==================================================================================
'''
add = suma de los elementos de los 2 arrays  (array1 + array2)
subtract=  resta de los elementos de los 2 arrays (array1 - array2)
multiply = multiplicacion de los elementos de los 2 arrays (array1 * array2)
divide , floor_divide = divide  los elementos de los 2 arrays (array1 /  o // array2)
power = Eleve los elementos del primer array a la potencia del segundo array  (array1 ** array2)
maximun, fmax  = calcula el maximo de 2 arrays elemento a elemento ignora valores NAN
minimun, fmin =  calcula el minimo de 2 arrays elemento a elemento ignora valores NAN
mod =  calcula el residuio de la division entre 2 arrays (array1 % array2)
greater, greater_equal, less, less_equal , equal, not_equal =Comparativas sobre elementos de los 2 arrays
logical_and,  logical_or = Operaciones boleeanas sobre elementos de ambos arrays (elemento a elemento)

'''

array1 = np.random.randn(5,5)
array2 = np.random.randn(5,5)

print("Array random 1")
print()
print(array1)
print()

print("Array random 2")
print()
print(array2)
print()

#-----------------------------------------------
#---minimun
print("funcion minimun, Hace un fusion y muestra los valores minimos de los 2 arrays")
print(np.minimum(array1,array2))
print()
#--divide--
print("Funcion DIvide, los elementos del los 2 array entre ellos")
print(np.divide(array1,array2))
print()
#--floor_divide--
print("Funcion DIvide y muestr el enetro , los elementos del los 2 array entre ellos")
print(np.floor_divide(array1,array2))
print()

