#===============================================================
#-------Funciones de Conjuntos-----------------------------
#===============================================================

'''
Numpy permite realizar tratamientos sobre Narrays asumiendo que el total de los elementos del mismo 
forma un conjunto


unique = calcula el conjunto unico de elementos sin duplicados 
intersect1d= Calcula la interseaccion de elementos de 2 arrays
union1d= Calcula la union de los elementos de los 2 arrays
in1d= calcula un array vooleano que indica si cada elemento del primer arrary esta contenido en el segundo
setdiff1d= Calcula la diferencia entre ambos conjuntos que es este en el primero pero no el en segundo
setxor1d= Calcula la diferencia simetria de ambos conjuntos

'''
import numpy as np
array1 = np.array([6,0,0,0,3,5,6])
array2 = np.array([7,4,3,1,2,6,5])
print("Array 1")
print(array1)
print()
print("Array 2")
print(array2)
print()

#-- Funcion Unique - solo muestra los valores unicos nada de repetidos
print(" calcula el conjunto unico de elementos sin duplicados  solo sde 1 array ")
print(np.unique(array1))
print()

#--Funcion union1d  involucando 2 arrays-Une los elementos unicos  y omite repeticiones --
print("Calcula la union de los elementos de los 2 arrays ")
print(np.union1d(array1, array2))
print()

#calcula un array vooleano que indica si cada elemento del primer arrary esta contenido en el segundo
print("calcula un array vooleano que indica si cada elemento del primer arrary esta contenido en el segundo")
print(np.in1d(array1, array2))
print()



