import numpy as np
from pandas import array

arreglo = np.array([1,2,3,4,5,6], dtype= np.float64) #arreglo de tipo float
#suma
print(arreglo)
print("----SUMA----sumando 5 a todo el array-")
print(arreglo+5)
print()

print("---RESTA-------")
print(arreglo-2)
print()

print("---multiplicacion----")
print(arreglo* 3)
print()

print("---Division decimal----")
print(2/arreglo)
print()

print("---Division Entera----")
print(2//arreglo)
print()

print("---Potencia----")
print(arreglo**2)
print()

print("---Asignacion con operador----")
arreglo +=1
print(arreglo)

print("---SUma de array de diferentes dimenciones--no se puede si son distintos tamaños--")
arreglo2 = np.array([7,8,9,10,11,12])
print(arreglo+arreglo2)
