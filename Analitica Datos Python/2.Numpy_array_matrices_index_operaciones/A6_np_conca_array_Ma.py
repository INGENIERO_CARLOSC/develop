#===================================================
#--------Concatenacion de arrays -------------------
#===================================================
import numpy as np
from pandas import array
'''
se pueden convinar arrays de 2 formas posibles

hstack, column_stack =  los elementos del segundo array se añaden a los del primero a lo ANCHO
vstack, row_stack = los elementos del segundo array se añaden a los del primero a lo LARGO

'''

array1 = np.arange(15).reshape(3,5) # llena en una 3x5 valores del 0 al 14
print(array1)

print()
array2 = np.arange(15,30).reshape(3,5) # llena en una 3x5 valores del 15 al 29
print(array2)
print()
#--------------Concatenacion Horicontal------------------------------
print("Concatenacion Horicontal ")
print(np.hstack((array1,array2)))

print()
print("Concatenacion vertical ")
print(np.vstack((array1,array2)))