
#=========Numerical Python====================
#------------Numpy----------------------------
#=============================================

import numpy as np #usamos la libreria con un alias

#-----Array / vectores / matrices unidimencionales / arreglos
''''
Pueden contener elementos de cualquier tipo 
todos los elemento del mismo array deben ser del mimo tipo
la dimecion del narray se define al principio y no se puede modificar
la organizacion de esos elementos en arrays de diferentes dimenciones si se puede modificar
'''

#=======Creacion basica de arrays=====================================

# array vacios   / empty es la propiedad para vaciar un array (dimensiones del array 3 filas 2 col) tipo 

array_vacio = np.empty((3,2), dtype= np.unicode_)
print("array vacios")
print(array_vacio)
print()

#copiando dimenciones y tipo desde otra estructura 
print("#copiando dimenciones y tipo desde otra estructura  ")
array_vacio_copia = np.empty_like([1,2,3,4,5]) # toma la catidad de indices y rellena los 5 elementos con numeros al azar 
print(array_vacio_copia)

# Arrays de unos 
print()
print("# Arrays de unos ")
array_unos = np.ones((3,2))
print(array_unos)

#creacion de una array de ceros
print()
print(" #creacion de una array de ceros")
array_ceros = np.zeros((3,2))
print(array_ceros)

# creacion de una rray con la matrix identidad 
print("# creacion de una rray con la matrix identidad ")
array_identidad = np.identity(3)
print(array_identidad)

#otro ejemplo pero con la funcion eye
print()
print("#otro ejemplo pero con la funcion eye")
array2_identidad = np.eye(6)
print(array2_identidad)

# matriz Cuadrada con unos en diagonal corridos 
print()
print("# Cuadrada con unos en la mitad  ")
array_segDiagonal = np.eye(4 , k =1) #k =1 me permite cambiar la posicion de la diagonal 
print(array_segDiagonal)

# matriz NO Cuadrada con unos en diagonal corridos 

print()
print("matriz NO Cuadrada con unos en diagonal corridos ")
arrayNo_segDiagonal = np.eye(4 ,3,  k = -1) #k =-1 me permite cambiar la posicion de la diagonal 
print(arrayNo_segDiagonal)

#1 creacion de un array con elementos de una secuencia numerica 
print()
print("#1 creacion de un array con elementos de una secuencia numerica  ")
A_Sec_1 = np.arange(10) #lo rellena con 10 numeros consecutivos
print(A_Sec_1)

#2 creacion de un array con elementos de una secuencia numerica 
print()
print("#2 creacion de un array con elementos de una secuencia numerica  ")
A_Sec_2 = np.arange(5,10) #muestra el intervalo entre 5 y 10
print(A_Sec_2)

#3 creacion de un array con elementos de una secuencia numerica 
print()
print("#3 creacion de un array con elementos de una secuencia numerica  ")
A_Sec_3 = np.arange(5,20,2) #muestra el intervalo entre 5 y 20 contando de 2 en dos
print(A_Sec_3)

#====================================
# CREACION DE UNA ARRAY UNIDIMECIONAL 
#====================================
print()
print("CREACION DE UNA ARRAY UNIDIMECIONAL ")
array_basico_uni = np.array([1,2,3,4,5])
print(type(array_basico_uni))
print(array_basico_uni)

#====================================
# CREACION DE UNA ARRAY mULTIDIMENCIONAL
#====================================
print()
print("CREACION DE UNA ARRAY mULTIDIMENCIONAL")
array_basico_Multi = np.array([[1,2,3,4],[5,6,7,8]])
print(array_basico_Multi)

#-----------------tipos de arrays-----------------------------------------

'''
Enteros consigno = np.int8, np.int16, np.int32, np.int64
Enteros sin signo = np.uint8, np.uint16, np.uint32, np.uint64
NUmeros en coma flotantes = np.float16, np.float32, np.float64, np.float128
Booleanos = np.bool
Objetos = np.object
Cadenas de caracteres = np.string\_ , np.unicode_
'''
#===========================================
# casting especificacion de tipos de Narrays
#============================================
print()
print(" casting especificacion de tipos de Narrays")
print(" Enteros")
array_inicial_enteros = np.array([1,2,3,4,5], dtype = np.int32)
print(array_inicial_enteros)

#--- arrays float 
print("")
print(" #--- arrays float ") #convirtio el array entero en decimal 
array_float = np.asarray(array_inicial_enteros, dtype = np.float64) #en lugar de escribir manualmente los datos le pasamos los del array anteioir
print(array_float)

#---Arrays Strings
print("")
print("#---Arrays Strings ") #convirtio el array decimal a cadena de texto
array_string = np.asarray(array_float, dtype = np.unicode_)
print(type(array_string))
print(array_string)

#========================================================
#--- Consultar la composicion de un Narray -------------
#========================================================
''' 
dtype = Tipo de contenido del Narray
ndim =NUmero de dimenciones del eje del Narray 
shape = estructura / Forma Narray , numero de elemento de cada una de las dimenciones
zise = Numero total de elementos del Narray
'''
print()
print("Consultar la composicion de un Narray")
print()
arreglo = np.array([[1,2,3,4],[5,6,7,8],[9,10,11,12]])
print(arreglo)
print()
print("contenido del array: ", arreglo.dtype) #contenido del array
print("cantidad de dimenciones que tiene el arreglo: ",arreglo.ndim) # cantidad de dimenciones que tiene el arreglo
print("forma de la dimencion /for del  array  cuantos elementos tiene cada fila o culman: ",arreglo.shape) # forma de la dimencion /for del  array  cuantos elementos tiene cada fila o culman
print("cantidad de elementos  o numero total de elementosque tiene el array: ",arreglo.size) #cantidad de elementos  o numero total de elementosque tiene el array

