#======================================================
#----indexacion y slicings basado en secuencia de enteros -- fanci indexing 
#=======================================================
import numpy as np
print("-indexacion y slicings basado en secuencia de enteros")
array_secE = np.empty((8,4)) #crea un matriz vacion de estrcutura 8 filas x 4 columnas
#---------------------------------------------
#haciendo el llenado de la matriz
for i in range(8):#recorre las filas 
    array_secE[i]= i #reasigna el valor de las filas 
print(array_secE)

#Indexacion de un conjunto arbritario de elementos 
print()
print("Indexacion de un conjunto arbritario de elementos ")
print(array_secE[[2,5]]) #muestra solamente las filas 2 y 5 de la matriz

print()
print("Indexacion con indice negativo de un conjunto arbritario de elementos ")
print(array_secE[[-2,-5]]) 

#============================================================================================
#---Indenxando de manera arbritaria multiples dimenciones con secuencias para cada dimencion
#============================================================================================
print()
print("Indenxando de manera arbritaria multiples dimenciones con secuencias para cada dimencion")
#solo podemos usar 8X4 porque coincide cin rango 32 de lo contrario dara error
matriz = np.arange(32).reshape((8,4)) #creando una matriz de 8 x 4 y llenandola con numeros del 0 al 32
print(matriz)

print()
print("Indexacion con Slicing secuencias de varios niveles (elemento elemento)")
#--------------filas----columnas---
print(matriz[[1,5,7,2],[0,3,1,2]]) #coordenadas de cada elemento
print()


print("Indexacion con Slicing columnas ")
#-- --- -----filas----columnas---
#------matriz[filas:,[0,0,0,2]]
print(matriz[:,[0,0,0,2]]) #me trae todas las filas pero solo datois de las columnas que especificque

print("Indexacion con Slicing secuencias de regiones ")
#--------------filas----columnas---
print(matriz[[1,5,7,2]][:,[0,3,1,2]]) #coordenadas de cada elemento

#==================================================================
#------Transposicion y modificacion de ejes / dimenciones ---------
#==================================================================
print()
print("Transposicion y modificacion de ejes / dimenciones")
print("Primer array")
arrayuno = np.arange(15)
print(arrayuno)

# modificacion de ejer/ dimenciones
print("modificacion de ejer/ dimenciones")
print("Segundo array")
arraydos = arrayuno.reshape(3,5) #tomo el contenido del aray 1 y lo reordeno en una dimecion 3x5
print(arraydos)

#transposicion de ejes o dimenciones

print()
print("#transposicion de ejes o dimenciones / las columnas pasan a ser filas y las filas columnas")
print(arraydos.T)