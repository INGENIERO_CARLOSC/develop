#==========================================================================
#--------------Secciones de eemtnos de Narray en funcion de una condicion 
#==========================================================================
import numpy as np
from pandas import array

array1 = np.random.randn(5,5)
array2 = np.random.randn(5,5)

print("Array random 1")
print()
print(array1)
print()

print("Array random 2")
print()
print(array2)
print()

#--- funcion condicional 
print("#--Muestra el valor menor comparando elemnto por elemento- funcion condicional funcion where ")
#--se comparan los arrays --, se devuelve alguno de los 2
print(np.where(array1 <array2, array1,array2 ))

#---Anidacion de condicion 
print()
print(" Anidacion de condicion")
#--------compara array y con array 2
#----------------------------------anida otra comparacion
#--------------------------------------si array1 es negativo muestro 0 o si no muestro el array1
#--------------------------------------------------------------si a1 no es menor a2 devuelve a2
print(np.where(array1 <array2, np.where(array1 <0 , 0, array1),array2 ))

