#=========================================================
#----FUnciones Matematicas y Logatitmiscas pata Narrays---
#=========================================================

'''
sum= suma de elemntos 
mean= media aritmetica de los elementos
median= mediana de los elementos
std= desviacion estandar de los elementos
var= varianza de los elementos
min= valor minimo de los elementos
max= valor maximo de los elementos 
argmin= indice de valor minimo 
argmax= indice de valor maximo 
cumsum= suma acumulada de elementos
cumprod= producto acumulado de elementos

todas estas funciones pueden recibir, ademas del Narray sobre el que se aplicran, un segundo
parametro llamado Axis, si no recibe este paremtros las funciones se aplicaran sobre 
el conjunto global de los delemntos del Narray, pero si se inclye
prodra tomar 2 valores

valor 0 = Aplica la funcion por columnas
valor 1=  Aplica la funcion por filas
'''
import numpy as np
array =  np.random.randn(5,4)
print("MAtriz original random 5x4")
print(array)

#---operacion global  
print(" #---operacion global / suma de todos los elementos del array ")
print(np.sum(array))

# Operacion por columnas 
print()
print(" # Operacion por columnas / suma de elementos de cada columna ")
print(np.sum(array, axis=0))

# Operacion por Filas
print()
print(" # Operacion por filas / suma de elementos de cada Fila ")
print(np.sum(array, axis=1))


#============================================================
#--ALgunas de estas funciones se pueden usar como metodos --
#=============================================================

#---sum()
print("")
print("Metodo sum() / suma de todos los elementos")
print(array.sum())

#---argmax()
print("")
print("Metodo argmax() / indice de valor maximo")
print(array.argmax())

#---argmin()
print("")
print("Metodo argmin()  indice de valor minimo")
print(array.argmin())

#---cumsum()
print("")
print("Metodo cumsum()  /suma acumulada de elementos")
print(array.cumsum())

#---cumprod()
print("")
print("Metodo cumprod()  /producto acumulado de elementos")
print(array.cumprod())

