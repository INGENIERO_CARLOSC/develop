#================================================
# ------Dividir o segmentar Arrays y Matrices---
#================================================
from matplotlib.pyplot import axis
import numpy as np
from pandas import array
'''
numpy y python tienen 3 formas de dividir arrays

hsplit =  Division de arrays en n partes "iguales" por COLUMNAS
vsplit =  Division de arrays en n partes "iguales" por FILAS
split =  Division de arrays en n partesno ASIMETRICAS
'''

Array =  np.arange(16).reshape(4,4)
print(Array)

#Division simetrica por columnas
print()
print() 
print("#Division simetrica por columnas  ")
print(np.hsplit(Array,2)) #2 columnas

print()
print() 
print("#Division simetrica por Filas  ")
print(np.vsplit(Array,2)) #2 filas



'''
Axis 

0 =  aplica la funcion por columna
1= aplica la funcion por fila 
'''
print()
print() 
print("#Division Asimetrica  ")
print(np.split(Array,[1,3], axis=1)) #especifica que haga el corte desde la psocion 1 en la posicion 3

#==============================================
#------SUma y restacon el eje Axis-------------
#==============================================

print("Array original ")
print(Array)
print()
print("--SUma y restacon el eje Axis-")
print("Suma de columnas ")
print(Array.sum(axis=0))

print()
print("Suma de filas")
print(Array.sum(axis=1))

'''
https://www.youtube.com/watch?v=vnBjUEKl4Vw
'''