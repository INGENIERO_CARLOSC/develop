# -*- coding: utf-8 -*-
"""2.Limpieza_datos

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1aTB6wyerXlqykhH2y6QFvrPjC3Guq_te

# **Tratamiento de datos faltantes**
"""

#importacion de librerias
import numpy as np #calculos matematicos cientificos
import matplotlib.pyplot as plt #sublinreria para representacio  grafica
import pandas as pd # libreria para carga y manipulacion de datos
dataset = pd.read_csv('/content/drive/MyDrive/Analitica_Programadorch/Archivoscsv/Data.csv')
dataset.info() #informacion de filas y columnas del dataset



dataset

columnas = dataset.columns #guardando el nombre de las columnas para procesos de limpieza nan

"""**Dividiendo las variables predicotras X y la variable a predecir Y**

La idea es dividir el datrafame en 2 partes nuevas para no afectarlo X y Y por columnas donde una cantidad de datos predeciran los resultados de los datos de Y
"""

#X es la variable independiente predictora  y Y es la variable dependiente a predecir
X = dataset.iloc[:,:-1].values #iloc sirve para localizar filas i columnas por posicion,[:,:-1] toma todas las filas y todas la columnas exepto la ultima en una array

Y = dataset.iloc[:,3].values # toma todas la filas pero solo de la ultima columna

"""# **2.Limpieza de datos y tratamiento de datos Nan**

detectando cantidad de datos faltantes en el dataframe
"""

#detectando cantidad de datos faltantes en el dataframe / con dropna() se pueden eliminar datos faltantes todas la filas 
dataset.isnull().sum()

"""modo manual """

#caluclamos la media de cada columna 
dataset.describe()

X # visulizando los datos de x antes de todos los cambios aqui se miraba los datos nan

#reemplazo los valores nan por el valor proimedio de cada columna 
X[:,1] = dataset['Age'].replace(np.nan, 38) #realiza el remplazo en todas las filas de la columna 2 donde hay valores nan
X[:,2] = dataset['Salary'].replace(np.nan, 63777) #realiza el remplazo en todas las filas de la columna 3 donde hay valores nan

X #visualizando el array X con los datos nan reemplazados por las medias