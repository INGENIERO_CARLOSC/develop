
import modulo_normalizacionD as md
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

ruta = 'C:\\Users\\Usuario\\Documents\\develop\\Archivos para analisis\\'
fichero = 'precios_coches02.csv' #nombre del fichero
datos= md.cargardatos(ruta, fichero,';')


nombre_columnas = ['indice','nombre','localizacion','año','kilometros recorridos','combustible','transmision','tipo propietario', 'kilometraje','motor','potencia','asientos','nuevo precio','precio']
md.cambiarencabezadoAllColumns(datos, nombre_columnas)

print(datos.head())
#creando el intervalo con el minimo y maximo de kl recorridos en 4
intervalo = np.linspace(min(datos['kilometros recorridos']),max(datos['kilometros recorridos']),4)
nombre_grupos = ['pocos','normal','muchos'] #nombre de las grupaciones
#creando nueva columna con  los intervalos "investigar mas"
datos['kilometros agrupados']=pd.cut(datos['kilometros recorridos'],intervalo,labels=nombre_grupos,include_lowest=True)
print(datos['kilometros agrupados'])

#graficando los intervalos
plt.hist(datos['kilometros recorridos'],bins=intervalo, rwidth=0.9, color='#991')
plt.title('Histograma kilometros recorridos')
plt.xlabel('Kilometros')
plt.ylabel('Frecuencia')
plt.show()

print(datos.groupby(['kilometros agrupados'],as_index=False ).count()) #mostrando contenido por agrupaciones
