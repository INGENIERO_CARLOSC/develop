import modulo_normalizacionD as md #importando modulo con funciones personalizadas
import matplotlib.pyplot as plt
from scipy import  stats # libreria para obrener coeficientes de y pvalor
'''
1- verificar la cantidad de filas y columnas
2-verificar el tipo de datos de cada columna
3- contar valores nulos de cada columna
4- rellenar los valores nulos por el valor de la media en columnas numericas
5- las columnas que deberian ser numericas pero estan object, cambiarles el tipo de valor
6- despues de convertir el valor objet a float64 reemplazar nulos por el valor de la media 
7- agrupar y graficar 
'''
#---enviando la ruta para leer el archivo
ruta = 'C:\\Users\\Usuario\\Documents\\develop\\Archivos para analisis\\'
fichero = 'precios_coches02.csv' #nombre del fichero
datos= md.cargardatos(ruta, fichero,';')
print(datos.head())

print("---verificacion datos nulos---")
print(datos.isnull().sum()) #verificando datos nulos

print(datos.dtypes) #mostrando tipo de dato

#OJO ESTE PASO APLICA SOLO PARA ESTOS CASOS 
#solo para este caso en que exite una columna numerdad  podemos seleccionarla como indice de filas SOLO PARA ESTE CASO 
datos.set_index('Unnamed: 0', inplace= True)
datos.index.name = 'indice' #renombrando al indice
print("----------------------------------------------------------------------------------------------")
print(datos.head())

print("nombres de las columnas")
#viendo los nombres de las columnas principales
print(md.verNombrecolumnas(datos))

print("---------------------------------Actualizacion de encabezado de columnas---------------------------------")
#si necesita cambiar el nombre de las columnas puede usar la funcion
nombre_columnas = ['nombre','localizacion','año','kilometros recorridos','combustible','transmision','tipo propietario', 'kilometraje','motor','potencia','asientos','nuevo precio','precio']
md.cambiarencabezadoAllColumns(datos, nombre_columnas)
#viendo los nombres de las columnas modificado
print(md.verNombrecolumnas(datos))

#datos estaditicos
print("-----------------------datos estaditicos---------Numericos------------------------------------------------------------------")
print(md.caracteristicasDataset(datos))
print('*'*100)

print("-----------------------datos estaditicos---------Numericos y categoricos -------------------------------------------------")
print(md.caracteristicasDataset(datos, 'todo'))

print('-----------------------------reemplazando datos nulo asientos----------------------------------------------')
#reemplazando datos nulo
datos= md.remplazarNulos(datos, 'asientos')
print(datos.isnull().sum()) #verificando datos nulos


#cambiando valor de tipo objet a float64 Aplica para cuando los valores son numeros pero estan como object
print("Tipo de dato actual: ",datos['potencia'].dtypes) #viendo el tipo de dato actual
md.cambiarTipoDatoColumna(datos,'potencia') #cambian el dato a float
datos= md.remplazarNulos(datos, 'potencia') #reemplazando nulos por la media de los datos convertidos
print(datos.isnull().sum()) #verificando datos nulos
print("Tipo de dato actual: ",datos['potencia'].dtypes)

#cambiando de objeto a entero solo si especificamos el tipo entero lo convierte de lo contrario lo envia a float
md.cambiarTipoDatoColumna(datos,'potencia','int64') 
print("Tipo de dato actual: ",datos['potencia'].dtypes)

md.cambiarTipoDatoColumna(datos,'nuevo precio') #cambian el dato de objeto a float 
print("Tipo de dato actual precios nuevos: ",datos['nuevo precio'].dtypes)

print(datos.dtypes) #mostrando tipo de dato

#visualizacion de datos de variables
plt.boxplot(datos['precio'])
plt.title('precio')
plt.show()

#grafico de dispercion
y = datos['precio']
x= datos['potencia']
plt.scatter(x,y)
plt.title("potencia / precio")
plt.xlabel('Potencia')
plt.ylabel("Precio")
plt.show()

#grafico con relacion al precio y motor

x= datos['motor']
plt.scatter(x,y)
plt.title("motor / precio")
plt.xlabel('motor')
plt.ylabel("Precio")
plt.show()

#=======================================================================
#visualizacion de datos decoeficiente de correlacion y pvalor
#=======================================================================
'''
si prox a 1 relacion positiva
si proxi a -1 relacion negativa
si proxi a = 0 no hay relacion
pvalor
si < 0.001 sera fuerte certeza entre el resultado 
si < 0.05 sera moderado 
si < 0.1 sera baja o debil
si >0.1 sin certeza de correlacion
'''
#no pueden haber valores nulos sino saldra error  en este caso motor tiene valores nulos vamos a tratarlos
datos= md.remplazarNulos(datos, 'motor')
#hayando coeficiente de relacion y pvalor
pearson_coef, p_valor = stats.pearsonr(datos['motor'],datos['precio'])
print(f"El coeficiente de correlacion de Pearson es : {pearson_coef}")
print(f"El p-valor es : {p_valor}")
#para este caso  el coeficiente de correlacion es 0.65 se aproxima a 1 y el o valor es 0 < 0.001 certeza lo que quiere decir que si
#existe una relacion entre las variables  precio y motor 

columnas = ['nombre','año','kilometros recorridos','motor','potencia','precio'] #lista

#------------creando dataset con las columnas deseadas ----------------
print("dataset nuevo")
datos_nuevos = datos[columnas] #nuevo dataset
print(datos_nuevos.head())

print()
print("agrupando la media de  datos por año")
#agrupando datos por año
datos_agrupados = datos_nuevos.groupby(['año'],as_index=False).mean() #sacando la media
print(datos_agrupados.head())

# guardamos datos
fichero='datosModificados.csv'
md.guardarDatos(datos, ruta, fichero)