import { Component, OnInit } from '@angular/core';
//importando elementos recuperacion de datos
import { FormGroup, FormBuilder } from '@angular/forms'; //ayuda a interacturar con el formulario, para recuperar datos
// importacion para agregar elementos ala formulario
import { CrudService } from 'src/app/servicio/crud.service';
//importacion libreria para realizar redireccionamientos, devolverse a paginas etc
import { Router } from '@angular/router';
@Component({
  selector: 'app-agregar-empleado',
  templateUrl: './agregar-empleado.component.html',
  styleUrls: ['./agregar-empleado.component.css']
})
export class AgregarEmpleadoComponent implements OnInit {

  formularioDeEmpleados:FormGroup; //indica que habra un grupo de elementos que haran una conexion como nombre y correo

  //Constructor para usar las funciones y propoedades de todo lo importado
  constructor(
    //variables declaradas
    public formulario:FormBuilder, // variable para crear formulario
    //Vriable para usar el servicio Crud
    private crudService:CrudService,
    //variable para enrutar o devolverse a otra paginas
    private ruteador: Router
    ){
    //formulario de empleados
    this.formularioDeEmpleados=this.formulario.group({
      nombre:[''], //recepcion de datos
      correo:['']
    })
   }

  ngOnInit(): void {
  }
  //funciones y comportamientos Controlador
  enviarDatos():any{
    console.log("Me presionaste!!");
    //envio de datos del formulario a la BD
    console.log(this.formularioDeEmpleados.value);//verificando la recuperacion de datos del usuario
    this.crudService.AgregarEmpleado(this.formularioDeEmpleados.value).subscribe(respuesta=>{
       //Enrutador o redireccionador
    this.ruteador.navigateByUrl('/listar-empleado');
    });//usando la funcion agregarempleado le pasamos el formulario de empleados

  }

}
