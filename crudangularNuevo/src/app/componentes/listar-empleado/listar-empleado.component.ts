import { Component, OnInit } from '@angular/core';
//importando el servicio de Crud contiene las funciones de agregar, ver datos y Api de php
import { CrudService } from 'src/app/servicio/crud.service';

@Component({
  selector: 'app-listar-empleado',
  templateUrl: './listar-empleado.component.html',
  styleUrls: ['./listar-empleado.component.css']
})
export class ListarEmpleadoComponent implements OnInit {
  //variables
  Empleados:any; //para almacenar informacion

  constructor(
    //variables para usar las importaciones
    private crudService:CrudService
  ) {}

  ngOnInit(): void {
    //----------------------ObtenerEmpleados() es un metodo que creamos en servicios/crud
    this.crudService.ObtenerEmpleados().subscribe(respuesta =>{
      //haciendo pruebas con mensaje por consola
      console.log(respuesta);
      //depositando la respuesta de la lista
      this.Empleados= respuesta;
    })
  }
//metodo
  borrarRegistro(id:any, iControl:any){
    //verificacion por consola del id a eliminar y el iControl
    console.log("EL id es:",id);
    console.log("La variable contadora es:",iControl);

  //validador para eliminar de registro
    if(window.confirm("Desea borrar el registro?")){

    //funcion para borrar la fila seleccionada
    this.crudService.BorrarEmpleado(id).subscribe((respuesta=>{
        //borrando el indice
        this.Empleados.splice(iControl,1);
    }));
  }
  }

}
