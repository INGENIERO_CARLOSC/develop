import { Component, OnInit } from '@angular/core';

//importando elementos recuperacion de datos
import { FormGroup, FormBuilder } from '@angular/forms'; //ayuda a recolectar informacion o gestionar los controles del formulario
// importacion para agregar elementos ala formulario
import { CrudService } from 'src/app/servicio/crud.service';
//importacion libreria para realizar redireccionamientos, devolverse a paginas etc
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editar-empleado',
  templateUrl: './editar-empleado.component.html',
  styleUrls: ['./editar-empleado.component.css']
})
export class EditarEmpleadoComponent implements OnInit {
  elID:any; //variable para almacenar el id
  formularioDeEmpleados:FormGroup; //indica que habra un grupo de elementos que haran una conexion como nombre y correo


  constructor(
    //variables de las importaciones
    private activeRoute:ActivatedRoute, //para recuperar
    private crudService: CrudService, //variable para obtener el valor
    //formulario
    public formulario:FormBuilder, // variable para obtener datos del formulario
    //variable para enrutar o devolverse a otra paginas
    private ruteador: Router
  ) {
    //recepcionando el id y almacenando en la variable elID
    this.elID=this.activeRoute.snapshot.paramMap.get('id');
    //verificando el id con un mensaje porconsola
    console.log(this.elID);
    //accediendo a los datos llamando al metodo pbtenerempleadoefitar oasandole el id
    this.crudService.ObtenerEmpleadoEditar(this.elID).subscribe(
      respuesta=>{
        console.log(respuesta);
         //
         this.formularioDeEmpleados.setValue({
          nombre:respuesta[0]['nombre'],
          correo:respuesta[0]['correo'] //se pudo traer los datos gracias al observable en el servicio
         });


      }
    );
    //mostrar losd atos en el formulario de editar
    this.formularioDeEmpleados = this.formulario.group({
      nombre:[''],
      correo:['']
    })
  }

  ngOnInit(): void {
  }
  //metodo para enviar datos editados
  enviarDatos():any{
    //VERIFICANDO EL ENVIO DE DATOS POR CONSOLA
    console.log(this.elID);
    console.log(this.formularioDeEmpleados.value);
    //realizando el editado definitivo - lo que hacemos es enviarle el id y los valores del formulario
    this.crudService.EditarEmpleadoDefinitivamente(this.elID,this.formularioDeEmpleados.value).subscribe(()=>{
       //Enrutador o redireccionador
    this.ruteador.navigateByUrl('/listar-empleado');
    });
  }

}
