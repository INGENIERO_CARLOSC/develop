//controlador del modelo
import { Injectable } from '@angular/core';
//importaciones
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'; //observador para verificar los datos
import { Empleado } from './Empleado'; //modelo, estructura de los datos que se van a enviar
@Injectable({
  providedIn: 'root'
})
export class CrudService {
  //variables
  //Api de php CRUD (API)
  API:string = 'http://localhost/empleados/'; //almacena la url de conexion
  //funcion clienteHttp para la  interaccion en el intercambio de informacion
  constructor(private clienteHttp: HttpClient) {}

  //medotodos para manipular el crud
    AgregarEmpleado(datosEmpleado:Empleado):Observable<any>{ //recibe los datos
      return this.clienteHttp.post(this.API+"?insertar=1",datosEmpleado); //retorna el envio con metodo post
    }//sirve para enviar datos del empleado
    //mostrar en el listado varios empleados
    ObtenerEmpleados(){
      return this.clienteHttp.get(this.API) //consultando
    }
    //borrar 1 empleado seleccionado
    BorrarEmpleado(id:any){ //"?borrar=" es un parametro que se encuentra en elarchivo php
      return this.clienteHttp.get(this.API+"?borrar="+id) // se usa get porque estamos obteniedno datos para borrarlos
    }
    //editar un empleado seleccionado ---OJO EL OBSERVABLE ES IMPORTANTE SI EL NO PODIA VER LOS OBJETOS ANY
    ObtenerEmpleadoEditar(id:any):Observable<any>{ //"?consultar" es un parametro que se encuentra en elarchivo php
      return this.clienteHttp.get(this.API+"?consultar="+id) // se usa get porque estamos obteniedno datos para editarlos
    }

    EditarEmpleadoDefinitivamente(id:any,datosEmpleado:any):Observable<any>{ //"?actualizar" es un parametro que se encuentra en elarchivo php
      return this.clienteHttp.post(this.API+"?actualizar="+id,datosEmpleado) // se post porque vamos editar definitivamente los datos
    }
}
