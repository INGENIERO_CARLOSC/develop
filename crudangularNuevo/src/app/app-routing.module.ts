import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//importando los componentes
import { AgregarEmpleadoComponent } from './componentes/agregar-empleado/agregar-empleado.component';
import { EditarEmpleadoComponent } from './componentes/editar-empleado/editar-empleado.component';
import { ListarEmpleadoComponent } from './componentes/listar-empleado/listar-empleado.component';

//constructor de rutas
const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo:'agregar-empleado'},//ruta por defecto
  //redireccionando directamente a cada  componente
  {path: 'agregar-empleado', component:AgregarEmpleadoComponent},
  {path: 'listar-empleado', component:ListarEmpleadoComponent},
  {path: 'editar-empleado/:id', component:EditarEmpleadoComponent}//segraga un id para poder identificar el registro a editar
];//al acceder a agregar-empleado me redirige a el componenete   AgregarEmpleadoComponent

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
