import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//importacion de los componentes
import { AddComponent } from './Persona/add/add.component';
import { EditComponent } from './Persona/edit/edit.component';
import { ListarComponent } from './Persona/listar/listar.component';

// rutas de todos los componenetes ya creados add,edit,listar (botones y listado)
const routes: Routes = [
  {path: 'listar', component:ListarComponent},
  {path: 'add', component:AddComponent},
  {path: 'listar', component:EditComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
