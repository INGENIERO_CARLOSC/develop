import { Component } from '@angular/core';
import { Router } from '@angular/router'; //se debe importar router

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'CrudAngular';

  //Contrsutor para los enrutamientos
  constructor(private router:Router){}

  //Metodos de los componenetes
   Listar(){
    this.router.navigate(["listar"]);
   }
   Nuevo(){
    this.router.navigate(["add"]);
   }
   Editar(){
    this.router.navigate(["edit"]);
   }
}
