import { Injectable } from '@angular/core';
import { Persona } from '../Modelo/Persona'; //importamos las variables del modelo persona para el listado
import {HttpClient} from '@angular/common/http' //se debe hacer la importacionde la libreria desde @angular/common/http
// Al minpliementar el servicio se encargara de hacer la conexion con el backend
@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  //constructor para la ruta del proyecto
  constructor(private http:HttpClient) { }
  Url = 'http://localhost:8000/CrudAngular/personas '//
  //metodos para traer los datos
  getPersonas(){
    //usamos directamente el objeto del Modelo/Persona.ts
    return this.http.get<Persona[]>(this.Url) //referenciamos la url
  }
}
