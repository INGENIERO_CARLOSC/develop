from django.db import models

class Producto(models.Model):
    #Categorias
    Burguer = 'burger'
    Pizza = 'pizza'
    Soda= 'soda'
    Chiken = 'chiken'
    
    #tupla de categorias para seleccionar
    ESCOJER_PRODUCTOS =(
        (Burguer, 'Burguer'),
        (Pizza, 'Pizza'),
        (Soda, 'Soda'),
        (Chiken, 'Chiken')
    )
    nombre = models.CharField(max_length=100)# textos cortos
    ingredientes = models.TextField(max_length=200, blank=True, null = True)# textos largos
    precio = models.DecimalField(max_digits=5, decimal_places=2, blank= True, null=True)#manejo de decimales
    tipo = models.BooleanField(default=False, help_text='Classic')# datos boleeanos
    main_categoria = models.CharField(choices= ESCOJER_PRODUCTOS, default= Burguer, blank =True, max_length=20)#escojer categorias    
    
    # clase meta tiene las caracteristicas de la tabla
    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        ordering= ['id'] #ordena por el id por defecto que pode django
    
    def __str__(self): #funcion para retornar el nombre del producto
        return self.nombre