from django.urls import path
from .views import  HomeView,CrearProductoView #importamos las  la clase de las vistas

app_name = 'webapp' #es importante especificar la app
#urls de la app
urlpatterns =[
    # '' es el nombre del path como no hay es el principal, sigue la clase en modo vista y el name
    path('', HomeView.as_view(), name= 'home'),    #url de vista basada en clase
    path('crear_producto', CrearProductoView.as_view(), name= 'crear_producto')
]