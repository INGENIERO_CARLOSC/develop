from django.http import HttpResponse#permite enviar objetos a las url con vistas en metodos o funciones
from django.views.generic.list import ListView #permite crear vistas para ver listados genericas basadas en clases 
from django.views.generic.edit import CreateView # permite crear elementos en la bd
from .models import Producto #importando el modelo
# fente https://www.youtube.com/watch?v=-O4G3FjcFIM
#vistas basadas en funciones
def despedida(request): #el nombre de los emtodos o clases son los que enviamos a las urls
    return HttpResponse("Matayas es una guerrero") 

#visya basada en clases, esta vista envia datos al index con la propiedad object_list
class HomeView(ListView):
    model = Producto 
    template_name = "index.html"
    paginate_by = 10 #muestra los datos en listas de 10 en 10
    
    
class CrearProductoView(CreateView):
    model = Producto 
    template_name = "crear_producto.html"
    fields = ['nombre','ingredientes','precio','tipo','main_categoria']

