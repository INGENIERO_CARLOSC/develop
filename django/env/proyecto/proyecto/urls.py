from django.contrib import admin #importa la vista de admin de django
from django.urls import path, include #permite incluir ursl de las vistas de aplicaciones
from webapp.views import  despedida #importando la vista de funciones

urlpatterns = [
    #vista de administrador django
    path('admin/', admin.site.urls), #pagina de admnistrador de app dejango
    #vista basada en clases llama las ur de la app
    path('', include('webapp.urls')), #incluye de la app Webapp todas las urls
    #Vista basada en funciones
    path('despedida', despedida),  #el segundo parametro es el nombre de la funcion que se llamara    
]
