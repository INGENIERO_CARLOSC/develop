#!/usr/bin/env python
# coding: utf-8

# In[8]:


#importacion de la libreria
import pandas as pd
#ruta de los datos
rutaFileCsv = 'https://github.com/luisguillermomolero/MisionTIC2022_2/blob/master/Modulo1_Python_MisionTIC2022_Main/Semana_5/Reto/movies.csv?raw=true'
def listaPeliculas(rutaFileCsv: str) -> str:
    # split('.') recorre ysepara la direccion web por . de escritura y [-1] selecciona el ultimo atributo .csv?raw=true
    # validamos si la curra es corecta con el if 
    if rutaFileCsv.split('.')[-1] == 'csv?raw=true':
        try:
            csv = pd.read_csv(rutaFileCsv)
            #creando subconjunto
            subGrupoPeliculas = csv[['Country','Language','Gross Earnings']]
            #pivot_table( ) creando tabla dinamica con el indice country y language
            gananciaPaisLenguage = subGrupoPeliculas.pivot_table(index=['Country','Language'])
            #mostrando solo  10 registros head muestra los argumentos de la tabla
            return gananciaPaisLenguage.head(10)
        except:
            #en caso de que no pueda abrir el archivo muestre error
            print('Error al leer el archivo de datos')
    else:
        # si la direccion web es invalidada  
        print('Extensión inválida')
    return
#enviando parametros
print(listaPeliculas(rutaFileCsv))

    


# In[ ]:




