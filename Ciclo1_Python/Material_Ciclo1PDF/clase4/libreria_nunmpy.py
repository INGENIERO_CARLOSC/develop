#!/usr/bin/env python
# coding: utf-8

# In[1]:





# In[3]:


import numpy as np


# In[4]:


pip install numpy #primero instalamos la libreria


# In[5]:


import numpy as np #usamos la libreria con un alias


# In[6]:


#array de 1 dimension
arregloNp = np.array([34,25,7])
print(type(arregloNp))
print(arregloNp) #imprimiendo el arreglo
print(arregloNp.shape) # permite ver el tamaño de la matriz


# In[7]:


# moficar el contenido de el array
arregloNp[2]= 45
arregloNp[1]= arregloNp[2]+ arregloNp[0]


# In[8]:


#Acceder a elementos del array
print(arregloNp[0],arregloNp[1],arregloNp[2])


# In[9]:


#Matrices


# In[10]:


matriz = np.array([[1,2,3],[4,5,6],[7,8,9]]) 
print(matriz)
print(type(matriz)) #tipo de dato
print("La dimencion de la matriz es : ",matriz.shape) #mostrando la dimencion de la matriz


# In[11]:


#accediendo a los valores de la matriz
print(" #accediendo a los valores de la matriz ")
print(matriz[1,0], matriz[0,2])
print(" # cambiar elementos ")
# cambiar elementos 
matriz[0,0] = 12
matriz[1,0]= 18
print(matriz)


# In[12]:


#--- usando clase random
azar = np.random.random((3,3)) #asignando el tamaño de la matriz
print("tamaño: ",azar.shape)
print(azar)
print("mostrando un valor especifico: ",round(azar[1,2],3)) #redondeando a 1 decimal


# In[13]:


#-- funciones basicas para crear matrices zeros,ones,full,eye
#-----------
Mceros = np.zeros((3,3)) #dimencion de la matriz
Munos= np.ones((1,2))
Mfull = np.full((2,2),7)# le agrega un valor por defecto a el contenido
Midenti =np.eye(2)# matriz de identidad
print("Matriz de ceros")
print(Mceros,"\n")
print("Matriz de unos")
print(Munos,"\n")
print("Matriz llena")
print(Mfull,"\n")
print("Matriz de identidad")
print(Midenti)


# In[14]:


import numpy as np
#rebanando matrices 
R = np.array([[8,3,1],[2,4,9],[12,31,43]])
print(R)
print("rebanando ")
S = R[:2, 1:3] #matroz nueva con valores del 0,2 al 1,3
print(S,"\n")


# In[15]:


# metodo frip ordena u invierte los valores originales en sentido contrarrio 
print(" # metodo frip")
print(np.fliplr(R))


# In[16]:


#convinando Rangos con numeros  enteros para rebanar

matrizvar = np.array([[1,2,3,4],[5,6,7,8],[9,10,11,12]])
print(matrizvar)
#rebanando por filas
Fila1 = matrizvar[1,:] #matriz de un rango de rama inferior
Fila2 = matrizvar[1:2]# matriz de mismo rango
print("matriz de un rango de rama inferior","\n")
print(Fila1,"  dimencion =",Fila1.shape)
print("matriz de mismo rango","\n")
print(Fila2," dimencion =",Fila2.shape)
print("--------REbanando por columnas---------","\n")
Columna1 = matrizvar[:,1]
Columna2 = matrizvar[:,1:2]
print("matriz de un rango de rama inferior","\n")
print(Columna1,"  dimencion =",Columna1.shape)
print("matriz de mismo rango","\n")
print(Columna2," dimencion =",Columna2.shape)


# In[17]:


#INDEXACION DE MATRICES CON ENTEROS permite contruir matrices con los datos de otra matriz
i = np.array([[1,2],[3,4],[5,6]])
print(i, "\n")
i[0]=10
print(i)
print("---mostrando  elementos con coordenada fila columna---")
print(i[[0,1,2],[0,1,0]])
print("otra forma")
print(i[[0,0],[1,1]])

#modificar matrices
print("Buscar en el pdf")


# In[18]:


#indexacion de expresiones boleanas
bol = np.array([[1,2],[3,4],[5,6]])
print(bol)

condicion = (bol>2)
print(condicion, "\n")
print("Mostrado los valores buscados")
print(bol[condicion])


# In[19]:


#tipos de datos para matrices
a= np.array([1,2])
b= np.array([1.0,2.5])
c= np.array([1,2], dtype = np.int64)
print(a.dtype)
print(b.dtype)
print(c.dtype)


# In[20]:


#matematicas de matrices

x= np.array([[1,2],[3,4]], dtype=np.float64)
y= np.array([[5,6],[7,8]], dtype=np.float64)

print(x, "\n",y)
print("------sumando-----------")
print(np.add(x,y))#mostrando y sumando las matrices con add
print("------resta-----------")
print(x-y)
print(np.subtract(x,y))#mostrando y restando las matrices con add
print("------multiplicacionresta-----------")
print(x*y)
print(np.multiply(x,y))#mostrando y multiplicando las matrices con add
print("------division-----------")
print(x/y)
print(np.divide(x,y))#mostrando y diviendo las matrices con add
print("------Raiz cuadrada -----------")
print(np.sqrt(x),"\n",np.sqrt(y))#mostrando y raiz  las matrices con add


# In[21]:


#producto interno de una matriz los mismo elemento de la matriz
#matriz bidiomencional
x= np.array([[1,2],[3,4]], dtype=np.float64)
y= np.array([[5,6],[7,8]], dtype=np.float64)

print(x, "\n",y)
#matriz unidimencional 
x1 = np.array([19,10])
y1 = np.array([1,12])
print(x1.dot(y1))
print(np.dot(x1,y1))

#funcion sum
print(np.sum(x1))
print(np.sum(x))
print(np.sum(x, axis =0))# suma conyenido de la columnas
print(np.sum(x, axis =1))# suma contenido de filas


# In[22]:


#broadcasting --- sumando el conytenido de un vector en las filas de la matriz
import numpy as np
x= np.array([[1,2,3],[4,5,6],[7,8,9],[10,11,12]])#matriz
print(x, "\n")
v = np.array([1,0,1]) #array
y= np.empty_like(x) #matriz vacia 4x3 rango 2

for i in range(4): #iteracion 0-3veces
    # sumando las filas y guardandolas en 7
    y[i, :] = x[i, :]+ v #sumando los rango el array v

print("nueva matriz y modificada")
print(y)


# In[23]:


# funcion tile --- otra manera de hacer el ejercicio anteior -crea numero de copias x del vector 
vv = np.tile(v, (4,1)) # le paso cuantas copias en filas necesito
print("4 copias de la misma fila del vector")
print(vv, "\n")
print("nuevo resultado")
print(x+vv)


# In[24]:


# reglas del broadcasting ----
# uso de reshape para operar array
#calcular el producto exteior de los vectores
v1 = np.array([1,2,3])
v2 = np.array([4,5])

#funcion reschape redimencionar 
print(np.reshape(v1, (3,1)))  #redimencionando
print("resultado")
print(np.reshape(v1, (3,1))*v2) #multiplica las 3 filas de la columna1 con v2
#--- otro ejemplo--usando transposicion - cambia la dimencion o invertir
x= np.array([[1,2,3],[4,5,6]])
print(x +v1, "\n")
# primera forma 
print((x.T + v2).T, "\n") #transponiendo x
# 2da forma metodo reshape
print(x+ (np.reshape(v2,(2,1))))


# In[ ]:





# In[25]:


import numpy as np
#creando un vector con rango del 10 al 49
print("ejercicio 1")
a = np.arange(10,50)
print(a)
print("ejercicio 2 invertir vector ")
print(a[::-1])
#creando matriz 3x3
print("ejercicio 3")
print(np.arange(0,9). reshape(3,3))
print()
#ejercicio 4   encontrar los indices que no son cero en el array
b= np.array([1,2,3,4,2,0,0,2,0,3,0,1,10,8,9,20])
print(np.argwhere (b!=0)) #argwhere realiza la busqueda indices
print()
#ejercicio 5 
print("#ejercicio 5 crear una matriz identica 6x6")
print(np.identity(6))
print()

 


# In[26]:


#ejercicio 6
print("#ejercicio 6 crear una matriz de 3 dimenciones con valores al azar")
      #el primer random revuelve y el segundo asigna el valor
r = np.random.random((3,3,3)) #el primer random genera y distribuye valores aleatorio 
print(r) 

# ejercicio 7 encontrar valores minimos y maximos de la matriz
print("# ejercicio 7 encontrar el indice valores minimos y maximos de la matriz")
print(r.argmin())# muestra el indice del valor minimo
print(r.argmax())# muestra el indice del valor maximo
#mostrar los valores
print("#mostrar los valores dentro de los indices")
print(r.ravel()[r.argmin()]) #valor minimo
print(r.ravel()[r.argmax()]) #valor maximo


# In[27]:


# ejercicio 8 crear una matriz 10*10 colocar 1 en los bordes y cero en el interoir
print("# ejercicio 8 crear una matriz 10*10 colocar 1 en los bordes y cero en el interior")
z = np.ones((10,10)) #rellena con 1
z[1:-1, 1:-1]=0 #recorre los rangos de la columna y fila exepto -1 y 1
print(z)


# In[ ]:




