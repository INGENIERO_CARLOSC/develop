#!/usr/bin/env python
# coding: utf-8

# In[62]:


pip install pandas


# In[19]:


import pandas as pd
#convierte en una serie lista

s= pd.Series(["matematicas","español","ciencias"])

print(s)#serie
print()
print(s.size)# tamaño de la serie 
print()
print(s.index) # top - elementos que contiene la serie, star posicion incial y step posicion final
print()
print(s.dtype) #tipo de dato


# In[33]:


# otro tipo de serie diccionario
# "keys" : valores
dic = pd.Series({"mate":3.0 ,"espa":5.0,"ingles":2,"arte":4, "progra":5})
print(dic)
print("---llamando elementos especificos por intervalos---")
print(dic[0:3])
print("---mostrando valores o valor especifico con keys---")
print(dic["espa"],dic["mate"])
print("----con doble corchete trae key y valor ----")
print(dic[["espa","ingles"]])


# In[42]:


#convertir un objeto serie a lista 
print("#crear y ver un array unidimencional como una estrcutura de series ")
print("array-----------------------------")
datos = [2,3,4,5,6,7]
print(datos)
print("Serie----------------------------")
S= pd.Series(datos) #convirtiendo el array en serie 
print(S)
print("----tamaño---")
print(S.size)
print("------describe ---------------")
print(S.describe())# desvacion estandar rangos estc, cuantos elementos hay mena prmedio std desviacion min valor min porcentajes de elementos


# In[48]:


#convertir un objeto serie a lista python
Obs = pd.Series([2,5,9,4,2,1])
print("----serie---------")
print(Obs)
print(type(Obs))
lista = Obs.tolist()
print("---lista--------")
print(lista)


# In[51]:


#aplicar las operaciones aritmeticas basicas sobre series
serie1 = pd.Series([2,3,5,6,8])
serie2 = pd.Series([5,9,11,23,1])
print("resta")
print(serie1-serie2)
print("suma")
print(serie1+serie2)
print("multiplicacion")
print(serie1*serie2)
print("division")
print(serie1/serie2)
print()


# In[59]:


print("usar operadores relacionales para comparar objetos serie")
print("serie 1 mayor a serie2")
print(serie1>serie2)
print("serie 1 igual a serie2")
print(serie1==serie2)
print("serie 1 diferente a serie2")
print(serie1!=serie2)


# In[61]:


#convertir diccionario de python en un objeto Serie
print("#convertir diccionario de python en un objeto Serie")
print("--diccionario----")
dicP = {"a":10, "b":20, "c":30}
print(dicP)
print("---Serie----")
serieP = pd.Series(dicP)
print(serieP)


# In[66]:


# convertir un arreglo Numpy en un objeto serie 
import numpy as np

arreglo = np.array([2,3,4,5,6,7])
print("-------array-numpy----")
print(arreglo)
print("--serie pandas-----")
serie = pd.Series(arreglo)
print(serie)


# In[74]:


print("Actividad 7")
convertir = pd.Series(["100", "200", "Python", "300.15", "150.4" ])
print(convertir)
#coerse convierte texto en error
convertir = pd.to_numeric(convertir, errors="coerce")
print(convertir)
print()


# In[82]:


#obtener una columna de un objeto dataframe como un onjeto serie
print("#obtener una columna de un objeto dataframe como un onjeto serie")
#diccionario y el valor esta dentro de las listas
print("Diccionario")
datos = {"A": [1,2,3,4,5], "B": [6,7,8,9,12], "c":[2,3,4,5,6]}
print(datos)
print("#convirtiendo a dataframe")
#convirtiendo a dataframe la vista es similar a una hoja de excel
df = pd.DataFrame(data=datos)
print(df)
#------iloc --para extraer filas y columnas---
#extraer una fila de un Dataframe como un objeto Serie
print("#extraer una fila de un Dataframe como un objeto Serie")
#[filas,columnas] [2,: todas las columnas]
fila = df.iloc[2,:] #mostrando los valores de la fila 2
print(fila)
print("#extraer una columna de un Dataframe como un objeto Serie")
columna = df.iloc[:,1] #mostrando todos los valores de la columna 1
print(columna)


# In[91]:


# convertir un objeto series con multiples listas en unico objeto serie
print("# convertir un objeto series con multiples listas en unico objeto serie")
serieM = pd.Series([["colombia","peru","mexico"],[2,3],["uruguay"]])
print(serieM)
print("--------serie final-------------")
#stack separa cada elemento como serie / seret:index(resetea drop true de forma verdadera)
serieM = serieM.apply(pd.Series).stack().reset_index(drop=True)
print(serieM)


# In[1]:


print("------DATAFRAMES........1 FILA DE MANZANAS Y FILA DE ---------")
datos ={'manzanas': [1,4,7,11,23,], 'naranjas':[2,67,89,12,1]}
compras = pd.DataFrame(datos, index= ('enero','febrero', 'marzo','abril','mayo'))
print(compras)
print("--------mostrando los indices de filas y columnas-----------------------")
print(compras.index)
print(compras.columns)
print()
print("propiedad axes -- muestra todo")
print(compras.axes)
print()
print("--------------------------------")
compras.index.name = "clientes"
compras.columns.name = "frutas"
print(compras)


# In[ ]:




