'''
En el parque de diversiones “AVENTURAS EXTREMAS” se requiere implementar una función en la 
cual reciba como parámetro un diccionario, el cual va a tener las variables que a continuación se 
muestran:
Nombre Tipo Descripción
id_cliente int Código único que identifica al cliente 
nombre str Nombre del cliente 
edad int Edad cliente 
primer_ingreso boolean Primera vez que el cliente ingresa puede ser: 
True/False
En la siguiente tabla se muestra la atracción y el valor de la boleta para cada una de ellas, en la cual 
los clientes podrán ingresar dependiendo de su edad, posteriormente el parque de atracciones ha 
decidido otorgar un descuento al valor de la boleta si cumple con el rango de edad y es la primera 
vez que el cliente ingresa.
Atracción Valor de boleta Edad 
(años) Primer Ingreso Descuento
X-Treme 20000 >18 años True 5% del valor de la boleta 
Carros 
chocones 5000 >=15 y 
<=18 True 7% del valor de la boleta 
Sillas 
voladoras 10000 >=7 y < 15 True 5% del valor de la boleta


Esta función debe retornar un nuevo diccionario con las llaves nombre, edad, atracción, 
primer_ingreso, total_boleta y apto del cliente: 
• En donde apto tendrá como valor una variable booleana, será verdadera si su edad cumple con 
los rangos exigidos en la tabla anterior, en el caso contrario será falsa. 
• En el caso de atraccion y total_boleta, si no se cumple el rango de edades se le asignara un valor 
de ‘N/A’ a cada uno. 
• Si primer_ingreso es verdadero, el total_boleta será el valor de la boleta menos el descuento de 
lo contrario se pagará el valor neto de la boleta.
Ejemplo: 
Id_cliente nombre edad primer_ingreso return
1
Johana 
Fernandez 20 True {'nombre': 'Johana Fernandez', 'edad': 20, 'atraccion': 'X-Treme','apto': 
True, 'primer_ingreso': True, 'total_boleta': 19000.0}
1
Johana 
Fernandez 20 False {'nombre': 'Johana Fernandez', 'edad': 20, 'atraccion': 'X-Treme','apto': 
True, 'primer_ingreso': False, 'total_boleta': 20000}
2
Gloria 
Suarez 3 True {'nombre': 'Gloria Suarez', 'edad': 3, 'atraccion': 'N/A', 'apto': 
False,'primer_ingreso': True, 'total_boleta': 'N/A'}
3
Tatiana 
Suarez 17 True {'nombre': 'Tatiana Suarez', 'edad': 17, 'atraccion': 'Carroschocones', 
'apto': True, 'primer_ingreso': True, 'total_boleta':4650.0}
3
Tatiana 
Suarez 17 False {'nombre': 'Tatiana Suarez', 'edad': 17, 'atraccion': 'Carroschocones', 
'apto': True, 'primer_ingreso': False, 'total_boleta': 5000}
4
Tatiana 
Ruiz 8 True {'nombre': 'Tatiana Ruiz', 'edad': 8, 'atraccion': 'Sillas voladoras','apto': 
True, 'primer_ingreso': True, 'total_boleta': 9500.0}
4
Tatiana 
Ruiz 8 False {'nombre': 'Tatiana Ruiz', 'edad': 8, 'atraccion': 'Sillas vo
'''

def cliente(informacion:dict):
    #listas
    id_cliente=informacion['id_cliente']   
    nombre_cliente=informacion['nombre']
    edad_cliente = informacion['edad']
    primer_ingreso_cliente = informacion['primer_ingreso']
    #validaciones
    if edad_cliente>18:
        atraccion='X-Treme'
        apto=True
        if primer_ingreso_cliente==True:
            valor_boleta=20000-(20000*0.05)        
        else:
            valor_boleta=20000              
    elif edad_cliente>=15 and edad_cliente<=18:
        atraccion='Carros chocones'
        apto=True
        if primer_ingreso_cliente==True:            
            valor_boleta=5000-(5000 *0.07)        
        else:
           valor_boleta=5000        
    elif edad_cliente>=7 and edad_cliente<=15:
        atraccion='Sillas voladoras'  
        apto=True
        if primer_ingreso_cliente==True:            
            valor_boleta=10000-(10000 *0.05)        
        else:
            valor_boleta=10000
    else:
        atraccion = 'N/A'
        apto = False
        valor_boleta = 'N/A'
        
#almacenando la lista en el diccionario
    diccionario_respuesta={
        "nombre":nombre_cliente,
        "edad": edad_cliente,
        "atraccion":atraccion,
        "apto": apto,      
        "primer_ingreso":primer_ingreso_cliente,
        "total_boleta":valor_boleta               
    }
    return diccionario_respuesta