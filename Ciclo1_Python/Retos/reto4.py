'''  
Un contador necesita llevar una bitácora acerca de los registros diarios de una papelería, en consecuencia, 
esta información que necesita automatizar es la siguientes:
Entrada:
Número de orden Código del producto Cantidad Precio unitario
int de 4 a 6 dígitos str de 4 a 6 dígitos Int Float
Salida:
Tipo de retorno Descripción
Str Cadena de caracteres de la forma:
“La factura <número> tiene un total en pesos de <cantidad>”
'''

'''
Requerimiento. Para el desarrollo de esta implementación, el programador debe permitir introducir un 
registro de ordenes desde una lista de tuplas y a través de la función “map”, “reduce” y “lambda”
desarrollar las funciones necesarias para el siguiente calculo:
• Sumar el total de cada tupla de cada lista
• Sumar los totales de todas las tuplas de toda la lista
• Suma el incremento si la compra no llega a un mínima de 600.000 pesos, en este caso, se 
incrementa 25.000 pesos al total de la compra.
Recuerde, que la salida no debe tener más de dos decimales.
Esqueleto:
def ordenes(rutinaContable):
 print('----------------- Inicio Registro diario --------------------------')
 print('----------------- Fin Registro diario -----------------------------')
'''
# recibe la lista de tuplas
def ordenes(rutinaContable):
    #importando la funcion reduce
    from functools import reduce
    #importacion d varables
    ordenMinima = 600000 #valor de la compra minima
    #primera funcion anonima que sume el total de cada tupla en cada lista
    ordenTotal = list(
        map(lambda x: [x[0]] + list(map(lambda y: y[1]*y[2], x[1:])),rutinaContable)
        )
    # segunda funcion anonima suma todos los totales de las tuplas de la lista
    ordenTotal = list(
        map(lambda x: [x[0]] + [reduce(lambda a,b: round(a+b,2), x[1:])], ordenTotal)
        )
    #tercera funion anonima suma el incremento si la compra no llega la minimo
    ordenTotal = list(
        map(lambda x: x if x[1] >= ordenMinima else (x[0], x[1] + 25000), ordenTotal)
        )
    
    print('----------------- Inicio Registro diario --------------------------')
    for x in range(len(ordenTotal)):
        print(f'La factura {ordenTotal[x][0]} titne un total en pesos de {ordenTotal[x][1]:,.2f}')
    print('----------------- Fin Registro diario -----------------------------')

# registros de prueba -------------
rutinaContable =[[1201, ("5464", 4, 25842.99), ("7854",18,23254.99), ("8521", 9, 48951.95)], 
                [1202, ("8756", 3, 115362.58),("1112",18,2354.99)],
                [1203, ("2547", 1, 125698.20), ("2635", 2, 135645.20), ("1254", 1, 13645.20),("9965", 5, 1645.20)],
                [1204, ("9635", 7, 11.99), ("7733",11,18.99), ("88112", 5, 390.95)] 
                ]
ordenes(rutinaContable)



#----------------------------------------------------------------------------------

# def ordenes(rutinaContable):   
#     from functools import reduce    
#     ordenMinima = 600000    
#     ordenTotal = list(map(lambda x: [x[0]] + list(map(lambda y: y[1]*y[2], x[1:])),rutinaContable))    
#     ordenTotal = list(map(lambda x: [x[0]] + [reduce(lambda a,b: round(a+b,2), x[1:])], ordenTotal))    
#     ordenTotal = list(map(lambda x: x if x[1] >= ordenMinima else (x[0], x[1] + 25000), ordenTotal))    
#     print('------------------------ Inicio Registro diario ---------------------------------')
#     for x in range(len(ordenTotal)):
#         print(f'La factura {ordenTotal[x][0]} tiene un total en pesos de {ordenTotal[x][1]:,.2f}')
#     print('-------------------------- Fin Registro diario ----------------------------------')
    