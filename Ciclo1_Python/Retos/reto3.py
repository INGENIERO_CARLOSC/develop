'''
entrada---tupla--------
idProducto
dProducto
pnProducto
cvProducto
sProducto
nComprador
nComprador
fVenta
-----------------

salida-----

Producto consultado : {idProducto} Descripción {dProducto} 
#Parte {pnProducto} Cantidad vendida {cvProducto} Stock 
{sProducto} Comprador {nComprador} Documento 
{nComprador} Fecha dictionaryVentas{fVenta}

---requerimiento--
Requerimiento. Para el desarrollo de esta implementación, el programador debe permitir introducir un 
registro de ventas desde una lista de tuplas como parámetro de AutoPartes y convertirlo luego en un 
diccionario con lista de tuplas.

Asimismo, se debe permitir desde la función consultaRegistro quien llama a un nuevo registro desde la 
función AutoPartes, manejar la entrada de un parámetro tipo idProducto para ubicar información 
específica acerca del producto en información general que incluye la fecha de venta
'''
# ---diccionario : dictionaryVentas{} , lista ventas[] , tupla(dProducto ,pnProducto,cvProducto,sProducto,nComprador, cComprador, fVenta)---

def Autopartes(ventas:list): #ventas -> lista
    dictionaryVentas={} #diccionario
    #--iterando las tuplas dentro de la lista ventas para guaradar datos en la lista ventas
    for idProducto,dProducto ,pnProducto,cvProducto,sProducto,nComprador,cComprador, fVenta in ventas:
        if dictionaryVentas.get(idProducto)== None: # exatre los idProducos y los almacena dentro de diccopnario en forma de lista luego valida si los idproducto  del diccionario estan vacios
            dictionaryVentas[idProducto] = [] #--- entonces este id figuarara como vacio en el diccionario y se mostrara mensaje de error
# diccionario con listas llenas de tuplas {[idProducto(dP,pn,cv,cc,fV)]}
# de lo contrario si el idProducto no esta vacio entonces le agrega los elementos la lista idproductos dentro del diccionario
        dictionaryVentas[idProducto].append((dProducto ,pnProducto,cvProducto,sProducto,nComprador, cComprador, fVenta)) # registro de nueva lista de tuplas en el diccionario  {[idProducto(dP,pn,cv,cc,)]}
    return dictionaryVentas#retorno del diccionario

'''
-------ejemplo de la estrcutura de los datos-------------------
dictionaryventas {
    ventas=[
        
    ]
    idProducto = [(
        dProducto ,pnProducto,cvProducto,sProducto,nComprador,cComprador, fVenta)
    ] 
}
----------------------------------------------------------------
'''
#--------------------------------------------------Consulta---------------------------------------------------------------------------------
def consultaRegistro(ventas, idProducto): # -> listas
    #--validar que el idproducto de la lista ventas 
    if idProducto in ventas:
        #---muestra toda la tupla de acueerdo al la lista de iDproducto o sea deacuerco a cada idProducto
         for dProducto ,pnProducto,cvProducto,sProducto,nComprador,cComprador, fVenta in  ventas[idProducto]: 
             print("Producto consultado :", idProducto," Descripción ", 
             dProducto,"#Parte ", pnProducto, "Cantidad vendida ", 
             cvProducto, "Stock ", sProducto, "Comprador ", nComprador,
             "Documento ", cComprador ,"Fecha Venta ",fVenta)
    else: 
        print("No hay registro de venta de ese producto") 

#--------------------------enviando registros para hacer la prueba--------------------------------------------------- 
consultaRegistro(Autopartes([
    (2001,'rosca', 'PT29872',2,45,'Luis Molero',3456,'12/06/2020'),
    (2010,'bujía', 'MS9512',4,15,'Carlos Rondon',1256,'12/06/2020'), 
    (2010,'bujía', 'ER6523',9,36,'Pedro Montes',1243,'12/06/2020'), 
    (3578,'tijera', 'QW8523',1,128,'Pedro Faria',1456,'12/06/2020'),
    (9251,'piñón', 'EN5698',2,8,'Juan Peña',565,'12/06/2020')]), 2010)
print()

consultaRegistro(Autopartes([
    (5489,'tornillo', 'RS8512',2,33,'Julio Perez',3654213,'13/06/2020'),
    (3215,'zocalo', 'UM8587',2,125,'Laura Macias',1256321,'13/06/2020'),
    (3698,'biela', 'PT3218',1,78,'Luis Peña',14565487,'13/06/2020'),
    (8795,'cilindro', 'AZ8794',2,96,'Carlos Casio',5612405,'13/06/2020')]), 2001)
print()

consultaRegistro(Autopartes([
    (9852,'Culata', 'XC9875',2,165,'Luis Molero',3455846,'14/06/2020'),
    (9852,'Culata', 'XC9875',2,165,'Jose Mejia',1355846,'14/06/2020'),
    (2564,'Cárter', 'PT29872',2,32,'Peter Cerezo',8545436,'14/06/2020'),
    (5412,'válvula', 'AZ8798',2,11,'Juan Peña',568975,'14/06/2020')]), 9852)
print()
#===================================================================================
#----------------------------------para enviar al robot -----------------------------
#===================================================================================

def Autopartes(ventas:list):
    dictionaryVentas={}  
    for idProducto,dProducto ,pnProducto,cvProducto,sProducto,nComprador,cComprador, fVenta in ventas:
        if dictionaryVentas.get(idProducto)== None: 
            dictionaryVentas[idProducto] =[] 
        dictionaryVentas[idProducto].append((dProducto ,pnProducto,cvProducto,sProducto,nComprador, cComprador, fVenta))
    return dictionaryVentas

def consultaRegistro(ventas, idProducto):    
    if idProducto in ventas:
           
         for dProducto ,pnProducto,cvProducto,sProducto,nComprador,cComprador, fVenta in  ventas[idProducto]: 
             print("Producto consultado :", idProducto, " Descripción ", dProducto, " #Parte ", pnProducto, " Cantidad vendida ", cvProducto, " Stock ", sProducto, " Comprador", nComprador, " Documento ", cComprador, " Fecha Venta ", fVenta)
    else: 
        print("No hay registro de venta de ese producto") 