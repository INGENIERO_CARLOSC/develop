# Actividad 1

print ("Activida 1")
import pandas as pd
import numpy as np
nombre = ['Oliva', 'Daniela', 'Juan', 'Germán', 'Edward', 'Alex', 'Julio',
'Edgar', 'Angie', 'Irlesa']
puntaje = [11.5, 8, 15.5, np.nan, 8, 19, 13.5, np.nan, 8, 18]
intentos = [1, 3, 2, 3, 2, 3, 1, 1, 2, 1]
califico = ['Sí', 'No', 'Sí', 'No', 'No', 'Sí', 'Sí', 'No', 'No', 'Sí']
indices = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
jugadores = {'nombre': nombre, 'puntaje': puntaje, 'intentos': intentos,
'califico': califico}
df = pd.DataFrame(data=jugadores, index=indices)
print (df)
print ("")
print (df.iloc[:3])
print ("")
print (df.iloc[0:3])
print ("")

#Actividad 2

print ("Activida 2")
nombre = ['Oliva', 'Daniela', 'Juan', 'Germán', 'Edward', 'Alex', 'Julio',
'Edgar', 'Angie', 'Irlesa']
puntaje = [11.5, 8, 15.5, np.nan, 8, 19, 13.5, np.nan, 8, 18]
intentos = [1, 3, 2, 3, 2, 3, 1, 1, 2, 1]
califico = ['Sí', 'No', 'Sí', 'No', 'No', 'Sí', 'Sí', 'No', 'No', 'Sí']
indices = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
jugadores = {'nombre': nombre, 'puntaje': puntaje, 'intentos': intentos,
'califico': califico}
df = pd.DataFrame(data=jugadores, index=indices)
print (df)
print ("")
print (df.iloc[-5:])
print ("")

#Actividad 3

print ("Activida 3")
datos = {'X': [1, 2, 3, 4, 5], 'Y': [5, 4, 3, 2, 1], 'Z': [2, 3, 5, 7, 11]}
df = pd.DataFrame(data=datos)
print (df)
print ("")
print (df.to_numpy())
print ("")

#Actividad 4

print ("Activida 4")
nombre = ['Oliva', 'Daniela', 'Juan', 'Germán', 'Edward', 'Alex', 'Julio',
'Edgar', 'Angie', 'Irlesa']
puntaje = [11.5, 8, 15.5, np.nan, 8, 19, 13.5, np.nan, 8, 18]
intentos = [1, 3, 2, 3, 2, 3, 1, 1, 2, 1]
califico = ['Sí', 'No', 'Sí', 'No', 'No', 'Sí', 'Sí', 'No', 'No', 'Sí']
indices = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
jugadores = {'nombre': nombre, 'puntaje': puntaje, 'intentos': intentos,
'califico': califico}
df = pd.DataFrame(data=jugadores, index=indices)
print (df)
print ("")
print (df[['nombre', 'califico']])
print ("")

#Actividad 5

print ("Activida 5")
datos = {'X': [1, 2, 3, 4, 5], 'Y': [5, 4, 3, 2, 1], 'Z': [2, 3, 5, 7, 11]}
df = pd.DataFrame(data=datos)
print (df)
print ("")
print (df.axes)
print ("")
indices = ['a', 'b', 'c', 'd', 'e']
df = pd.DataFrame(data=datos, index=indices)
print (df)
print ("")
print (df.axes)
print ("")

#Activida 6

print ("Activida 6")
nombre = ['Oliva', 'Daniela', 'Juan', 'Germán', 'Edward', 'Alex', 'Julio',
'Edgar', 'Angie', 'Irlesa']
puntaje = [11.5, 8, 15.5, np.nan, 8, 19, 13.5, np.nan, 8, 18]
intentos = [1, 3, 2, 3, 2, 3, 1, 1, 2, 1]
califico = ['Sí', 'No', 'Sí', 'No', 'No', 'Sí', 'Sí', 'No', 'No', 'Sí']
indices = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
jugadores = {'nombre': nombre, 'puntaje': puntaje, 'intentos': intentos,
'califico': califico}
df = pd.DataFrame(data=jugadores, index=indices)
print (df)
print ("")
print (df.iloc[[1, 4], [0, 3]])
print ("")

#Activida 7

print ("Activida 7")
datos = {'X': [1, 2, 3, 4, 5], 'Y': [5, 4, 3, 2, 1], 'Z': [2, 3, 5, 7, 11]}
df = pd.DataFrame(data=datos)
print (df)
print ("")
print (df.ndim)
print ("")
print (df.size)
print ("")
print (df.shape)
print ("")

#Actividad 8

print ("Activida 8")
nombre = ['Oliva', 'Daniela', 'Juan', 'Germán', 'Edward', 'Alex', 'Julio',
'Edgar', 'Angie', 'Irlesa']
puntaje = [11.5, 8, 15.5, np.nan, 8, 19, 13.5, np.nan, 8, 18]
intentos = [1, 3, 2, 3, 2, 3, 1, 1, 2, 1]
califico = [True, False, True, False, False, True, True, False, False, True]
indices = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
jugadores = {'nombre': nombre, 'puntaje': puntaje, 'intentos': intentos,
'califico': califico}
df = pd.DataFrame(data=jugadores, index=indices)
print (df)
print ("")
df.memory_usage()
print ("")

#Actividad 9

print ("Activida 9")
nombre = ['Oliva', 'Daniela', 'Juan', 'Germán', 'Edward', 'Alex', 'Julio',
'Edgar', 'Angie', 'Irlesa']
puntaje = [11.5, 8, 15.5, np.nan, 8, 19, 13.5, np.nan, 8, 18]
intentos = [1, 3, 2, 3, 2, 3, 1, 1, 2, 1]
califico = ['Sí', 'No', 'Sí', 'No', 'No', 'Sí', 'Sí', 'No', 'No', 'Sí']
indices = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
jugadores = {'nombre': nombre, 'puntaje': puntaje, 'intentos': intentos,
'califico': califico}
df = pd.DataFrame(data=jugadores, index=indices)
print (df)
print ("")
print (df.loc[['a', 'i'], ['nombre', 'califico']])
print ("")
print (df.loc[['a', 'i'], ['nombre', 'califico']])
print ("")

#Actividad 10

print ("Activida 10")
df = pd.DataFrame({'X': []})
print (df)
print ("")
print (df.size)
print ("")
print (df.empty)
print ("")
df = pd.DataFrame({'X': [np.nan]})
print (df)
print ("")
print(df.size)
print ("")
print (df.empty)
print ("")

#Actividad 11

print ("Activida 11")
nombre = ['Oliva', 'Daniela', 'Juan', 'Germán', 'Edward', 'Alex', 'Julio',
'Edgar', 'Angie', 'Irlesa']
puntaje = [11.5, 8, 15.5, np.nan, 8, 19, 13.5, np.nan, 8, 18]
intentos = [1, 3, 2, 3, 2, 3, 1, 1, 2, 1]
califico = ['Sí', 'No', 'Sí', 'No', 'No', 'Sí', 'Sí', 'No', 'No', 'Sí']
indices = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
jugadores = {'nombre': nombre, 'puntaje': puntaje, 'intentos': intentos,
'califico': califico}
df = pd.DataFrame(data=jugadores, index=indices)
print (df)
print ("")
print (df[df['puntaje'].isnull()])
print ("")

#Actividad 12

print ("Activida 12")
nombres = pd.Series(['Edward', 'Germán', 'Daniela'])
edades = pd.Series([27, 23, 19])
personas = {'nombre': nombres, 'edad': edades}
df = pd.DataFrame(data=personas)
print (df)
print ("")

#Actividad 13

print ("Activida 13")
datos = {'X': [2, 3], 'Y': [5, 7]}
df = pd.DataFrame(data=datos)
print (df)
print ("")
print (df.info())
print ("")
print (df.dtypes)
print ("")
print (df.astype({'X': 'int32'}).dtypes)
print ("")
print (df.astype({'X': 'int32', 'Y': 'float64'}).dtypes)
print ("")


#Actividad 14

print ("Activida 14")
nombre = ['Oliva', 'Daniela', 'Juan', 'Germán', 'Edward', 'Alex', 'Julio',
'Edgar', 'Angie', 'Irlesa']
puntaje = [11.5, 8, 15.5, np.nan, 8, 19, 13.5, np.nan, 8, 18]
intentos = [1, 3, 2, 3, 2, 3, 1, 1, 2, 1]
califico = ['Sí', 'No', 'Sí', 'No', 'No', 'Sí', 'Sí', 'No', 'No', 'Sí']
indices = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
jugadores = {'nombre': nombre, 'puntaje': puntaje, 'intentos': intentos,
'califico': califico}
df = pd.DataFrame(data=jugadores, index=indices)
print (df)
print (df[df['puntaje'].between(13, 19)])
print ("")

#Actividad 15

print ("Activida 15")
nombres = ['Edward', 'Daniela', 'Germán']
edades = [5, 6, np.nan]
fecha_nacimiento = [pd.Timestamp('1990-01-03'), None, np.nan]
personas = {'nombre': nombres, 'edad': edades, 'fecha_nacimiento': fecha_nacimiento}
df = pd.DataFrame(data=personas)
print (df)
print ("")
print (df.isna())
print ("")

#Actividad 16

print ("Activida 16")
nombres = ['Edward', 'Daniela', 'Germán']
edades = [5, 6, np.nan]
fecha_nacimiento = [pd.Timestamp('1990-01-03'), None, np.nan]
personas = {'nombre': nombres, 'edad': edades, 'fecha_nacimiento': fecha_nacimiento}
df = pd.DataFrame(data=personas)
print (df)
print ("")
print (df.notna())
print ("")

#Actividad 17

print ("Activida 17")
datos = {'X': [1, 2, 3, 4, 5], 'Y': [5, 4, 3, 2, 1], 'Z': [2, 3, 5, 7, 11]}
indices = ['a', 'b', 'c', 'd', 'e']
df = pd.DataFrame(data=datos, index=indices)
print (df)
print ("")
print (df.at['b', 'Y'])
print ("")
print (df.at['e', 'Z'])
print ("")

#Actividad 18

print ("Activida 18")
datos = {'X': [1, 2, 3, 4, 5], 'Y': [5, 4, 3, 2, 1], 'Z': [2, 3, 5, 7, 11]}
indices = ['a', 'b', 'c', 'd', 'e']
df = pd.DataFrame(data=datos, index=indices)
print (df)
print ("")
df.at['b', 'Y'] = 29
print (df)
print ("")
df.at['e', 'Z'] = 33
print (df)
print ("")

#Actividad 19

print ("Activida 19")
datos = {'X': [1, 2, 3, 4, 5], 'Y': [5, 4, 3, 2, 1], 'Z': [2, 3, 5, 7, 11]}
indices = ['a', 'b', 'c', 'd', 'e']
df = pd.DataFrame(data=datos, index=indices)
print (df)
print ("")
df.loc['a', 'Z'] = 12
print (df)
print ("")

#Actividad 20

print ("Activida 20")
datos = {'X': [1, 2, 3, 4, 5], 'Y': [5, 4, 3, 2, 1], 'Z': [2, 3, 5, 7, 11]}
indices = ['a', 'b', 'c', 'd', 'e']
df = pd.DataFrame(data=datos, index=indices)
print (df)
print ("")
print (df.loc['a', 'Z'])
print ("")
print (df.loc['d', 'Z'])
print ("")