#avion_con_mayor_uso
vuelosEjemplo = [{"aerolinea": "AVIANCA", 'codigo': "AHF21", "origen": "BOG", "destino":
"CTG", "distancia": 295, "retraso": 5, "duracion": 120, "salida":600},
        {"aerolinea": "VIVAAIR", 'codigo': "VVE01", "origen": "BOG", "destino":
"CTG", "distancia": 295, "retraso": 2, "duracion": 115, "salida":555},
        {"aerolinea": "AVIANCA", 'codigo': "AHF21", "origen": "CTG", "destino":
"BOG", "distancia": 295, "retraso": 15, "duracion": 120, "salida":830},
        {"aerolinea": "VIVAAIR", 'codigo': "VVE01", "origen": "CTG", "destino":
"PEI", "distancia": 325, "retraso": 5, "duracion": 135, "salida":800},
        {"aerolinea": "AVIANCA", 'codigo': "AHF23", "origen": "BOG", "destino":
"CLO", "distancia": 255, "retraso": 25, "duracion": 170, "salida":605},
        {"aerolinea": "VIVAAIR", 'codigo': "VVE01", "origen": "PEI", "destino":
"BOG", "distancia": 220, "retraso": 5, "duracion": 60, "salida":1030},
        {"aerolinea": "AVIANCA", 'codigo': "AHF23", "origen": "CLO", "destino":
"CTG", "distancia": 400, "retraso": 20, "duracion": 160, "salida":1200}]

# calcular los diferentes aviones
aviones = set()
for vuelo in vuelosEjemplo:
    aviones.add(vuelo['codigo'])
    
#alistar en un diccionario calcular el uso de cada avion
miDiccionario = dict()
for avion in aviones:
    miDiccionario[avion] = 0
    
#realizar la acumulacion de duraciones cada avion del listado de vuelos
mayorDuracion = -1
for avion in aviones:
    for vuelo in vuelosEjemplo:
        if avion == vuelo['codigo']:
            miDiccionario[avion]= miDiccionario[avion] + vuelo['duracion']
        #aprovechar los fors para encontrar el avion con mayor duracion
        if miDiccionario[avion]> mayorDuracion:
            mayorDuracion =miDiccionario[avion]
            avionConMayorDuracion = avion
            
# preparacion de la tupla de salida
miTupla=(avionConMayorDuracion,miDiccionario[avionConMayorDuracion])

print (miTupla)