def conversor(sis):
    def sis_bin(numero):
        print('dec:', numero, 'bin:', bin(numero))
 
    def sis_oct(numero):
        print('dec:', numero, 'oct:', oct(numero))
   
    def sis_hex(numero):
        print('dec:', numero, 'hex:', hex(numero))
  
    sis_func = {'bin': sis_bin, 'oct': sis_oct, 'hex': sis_hex}
    return sis_func[sis]

# Crea una instancia del conversor hexadecimal
conversorhex = conversor('hex')

# Convierte 100 de decimal a hexadecimal
conversorhex(100)

# Otro modo de usar el conversor. 
# Convierte 9 de decimal a binario
conversor('bin')(9)