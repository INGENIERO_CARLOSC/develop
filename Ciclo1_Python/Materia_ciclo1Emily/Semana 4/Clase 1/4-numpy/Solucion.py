import numpy  as np

# Actividad 1
a = np.arange(10,50)
print (a)

#Actividad 2

print (a[::-1])

#Actividad 3

print (np.arange(0,9).reshape(3,3))

#Actividad 4

a = np.array([1,2,4,2,4,0,1,0,0,0,12,4,5,6,7,0])
print (np.argwhere( a!=0 ))

#Actividad 5

print (np.identity(6))

#Activida 6

r = np.random.random((3,3,3))
print (r)

#Activida 7

print( r.argmax() )
print( r.ravel()[r.argmax()] )
print(r)
print(np.unravel_index(r.argmax(), r.shape))
print (r[np.unravel_index(r.argmax(), r.shape)])

#Actividad 8

z = np.ones((10,10))
z[1:-1,1:-1] = 0
print (z)

#Actividad 9

print (np.tile( np.arange(0,5) , 5).reshape(5,5))

#Actividad 10

a = np.random.random((3,3))
b = np.random.random((3,3))
print (a == b)