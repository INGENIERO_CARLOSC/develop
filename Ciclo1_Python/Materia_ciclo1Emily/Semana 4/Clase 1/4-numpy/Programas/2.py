import numpy as np
import matplotlib.pyplot as plt

#Operaciones básicas con numpy arrays
vector_a = np.arange(100)
vector_b = np.arange(100)**(1/2)

suma_v = vector_a + vector_b
mult_v = vector_a* vector_b

#calculo con matrices
matriz = np.array([[1,2],[3,4]])
matriz.sum()
matriz.mean()
matriz.std()
matriz**2
inversa = np.linalg.inv(matriz)
np.dot(matriz,inversa)

#Ajuste polinómico
x= np.arange(-2,2,0.1)
y = np.array([np.sin(valor * (180/np.pi)) for valor in x])
plt.plot(x,y)
coeficientes = np.polyfit(x,y,39)
y_estimada = np.polyval(coeficientes,x)
plt.plot(x,y_estimada)

#Números aleatorios en numpy
cantidad = 100000

uniformes = np.random.uniform(20,80,cantidad)
plt.subplot(2,2,1)
plt.hist(uniformes, bins = 100)

normales = np.random.normal(50,10,cantidad)
plt.subplot(2,2,2)
plt.hist(normales,bins=100)

triangular = np.random.triangular(20,30,80,cantidad)
plt.subplot(2,2,3)
plt.hist(triangular,bins=100)

exponenciales = np.random.exponential(100,cantidad)
plt.subplot(2,2,4)
plt.hist(exponenciales,bins=100)