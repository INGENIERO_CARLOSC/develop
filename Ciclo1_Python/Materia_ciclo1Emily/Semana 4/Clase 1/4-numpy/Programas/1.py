import numpy as np
import time
import sys

tamaño = 10000000

print("vector numpy")
t_vector = time.time()
vector_numpy = np.arange(tamaño,dtype="int64")
suma_vector = vector_numpy.sum()
print("Tipo objeto: " + str(type(vector_numpy)))
print("suma: "+ str(suma_vector))
print("Tamaño vector: " +  str(sys.getsizeof(vector_numpy)/1024))
print("Tiempo ejecución: " + str(time.time()-t_vector))

print("----------------------------")

print("lista")
t_lista = time.time()
lista = list(range(tamaño))
suma_lista = sum(lista)
print("Tipo objeto: " + str(type(lista)))
print("suma: "+ str(suma_lista))
print("Tamaño lista: " +  str(sys.getsizeof(lista)/1024))
print("Tiempo ejecución: " + str(time.time()-t_lista))

#Dimensionalidad
#0D
escalar_0d = 10
#1D
vector_1d = np.array([1,2,3,4,5])
#2D
matriz_2d = np.array([[1,2,3],[4,5,6],[7,8,9]])
#3D
matriz_3d = np.array([[[1,2,3],[4,5,6],[7,8,9]],[[11,11,13],[14,15,16],[17,18,19]]])

#Slicing numpy
vector_1d[0]
vector_1d[-1]
vector_1d[0:3]

matriz_2d[1,0]
matriz_2d[:,1]
matriz_2d[2,:]

matriz_3d[1]
matriz_3d[1,:,2]