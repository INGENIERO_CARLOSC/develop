import pandas as pd

def resumen_cotizaciones(fichero):
    df = pd.read_csv(fichero, sep=';', decimal=',', thousands='.', index_col=0)
    return pd.DataFrame([df.min(), df.max(), df.mean()], index=['Mínimo', 'Máximo', 'Media'])

print (resumen_cotizaciones('C:/Users/juand/Documents/Trabajo con el Ing.Johan/TIC/Diapositivas A/Unidad 4/Clase 3/Soluciones/cotizacion.csv'))