def CDT(usuario:str, capital:int, tiempo:int):
    mensaje = "El tiempo ingresado es invalido"
    if (tiempo <=2 and tiempo >0):
        valorTotal = capital-(capital*0.02)
        mensaje = f"Para el usuario {usuario} La cantidad de dinero a recibir, según el monto inicial {capital} para un tiempo de {tiempo} meses es: {valorTotal}"
    elif (tiempo > 2):
        valorTotal =(capital*0.03*tiempo)/12+capital
        mensaje = f"Para el usuario {usuario} La cantidad de dinero a recibir, según el monto inicial {capital} para un tiempo de {tiempo} meses es: {valorTotal}"
    return mensaje

print(CDT("AB1012", 1000000, 3))
print(CDT("ER3366", 650000, ))
    
    
        