def cliente(informacion:dict) ->dict:
    result ={ "nombre":informacion["nombre"], "edad":informacion["edad"],"atraccion" : "N/A", "apto": False, "primer_ingreso":informacion["primer_ingreso"], "total_boleta":"N/A"}
    if (informacion["edad"]>18):
        result["atraccion"] ="X-Treme"
        result["total_boleta"] = 20000
        result["apto"]= True
        if (informacion["primer_ingreso"] == True):
            result["total_boleta"] -=result["total_boleta"]*0.05
    elif (informacion["edad"]>= 15 and informacion["edad"] <=18):
        result["atraccion"] ="Carros chocones"
        result["total_boleta"] = 5000
        result["apto"]= True
        if (informacion["primer_ingreso"] == True):
            result["total_boleta"] -=result["total_boleta"]*0.07
    elif (informacion["edad"]>= 7 and informacion["edad"] <15):
        result["atraccion"] ="Sillas voladoras"
        result["total_boleta"] = 10000
        result["apto"]= True
        if (informacion["primer_ingreso"] == True):
            result["total_boleta"] -=result["total_boleta"]*0.05
    return result

informacion = {"id_cliente":1, "nombre": "Henry Cavill", "edad":39, "primer_ingreso": True}  
print(cliente(informacion))
        