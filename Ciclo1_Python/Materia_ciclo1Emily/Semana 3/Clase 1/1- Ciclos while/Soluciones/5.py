def main():
    print("DIGA S O s PARA CONTINUAR")
    respuesta = input("¿Desea continuar el programa?: ")

    while respuesta == "S" or respuesta == "s":
        respuesta = input("¿Desea continuar el programa?: ")

    print("¡Hasta la vista!")


if __name__ == "__main__":
    main()