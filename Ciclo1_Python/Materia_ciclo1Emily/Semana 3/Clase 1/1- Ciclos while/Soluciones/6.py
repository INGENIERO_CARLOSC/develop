def main():
    print("DIGA S O s PARA TERMINAR")
    respuesta = input("¿Desea terminar el programa?: ")

    while respuesta != "S" and respuesta != "s":
        respuesta = input("¿Desea terminar el programa?: ")

    print("¡Hasta la vista!")


if __name__ == "__main__":
    main()