def main():
    print("HUCHA")

    objetivo = float(input("¿Cuántos euros quiere ahorrar?: "))

    ahorrado = 0.0
    while objetivo > ahorrado:
        ahorrado += float(input("¿Cuántos euros quiere meter en la hucha?: "))

    print(f"¡Objetivo conseguido! Ha ahorrado usted {ahorrado} euros.")


if __name__ == "__main__":
  main()