def main():
    print("DIGA N PARA CONTINUAR")
    respuesta = input("¿Desea terminar el programa?: ")

    while respuesta == "N":
        respuesta = input("¿Desea terminar el programa?: ")

    print("¡Hasta la vista!")


if __name__ == "__main__":
    main()