#PASO 3 - Creamos las facciones que vamos a usar 
#Funcion que se usa para cargar un estudiante a los estudiantes_activos
def cargarEstudiante(identifiacion: int, nombre: str):
    estudiantes_activos[identifiacion]=[nombre]
    print ("*Estudiante ingresado con exito*")
    return estudiantes_activos

#Funcion que se usa para imprimir a todos los estudiantes
def imprimirListadoEstudiantes(estudiantes):
    for identifiacion,datos in estudiantes.items():
      print("-Número:",identifiacion)
      print("-Nombre:",datos[0])
      print ("---------------------------------") 

#PASO 1  - Creamos el diccionario y se rellana con los datos 
estudiantes_activos={1:["JUAN ARTURO VELASQUEZ DUQUE"], 2:["KEVIN JHOAN	ALVIS GOMEZ"], 3:["NATALIA CLAVIJO PINZON"]}

print ("---------------------------------") 

#PASO 2  - Creamos una variable para identificar el estudiante que se le hará la modificación el nombre 
print("Modificar nombre")
numeroEstudiante= int (input("Ingrese un id del estudiante que quiere modificar:    ")) #variable para identificar el id 
estudiantes_activos[numeroEstudiante][0]= str (input("Ingrese nuevo nombre:     ")) #variable para el nuevo nombre 
print ("Estudiante :",estudiantes_activos[numeroEstudiante][0] , ",  nombre con exito"  ) #Mensaje que confirma que la nombre fue actualizado

print ("---------------------------------") 
#PASO 4  - Damos la orden de imprimir los datos
print (imprimirListadoEstudiantes(estudiantes_activos)) #Imprimir el listado de estudiantes