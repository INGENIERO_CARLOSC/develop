#PASO 3 - Creamos las facciones que vamos a usar 
#Funcion que se usa para cargar un estudiante a los estudiantes_activos
def cargarEstudiante(identifiacion: int, nombre: str):
    estudiantes_activos[identifiacion]=[nombre]
    print ("*Estudiante ingresado con exito*")
    return estudiantes_activos

#Funcion que se usa para imprimir a todos los estudiantes
def imprimirListadoEstudiantes(estudiantes):
    for identifiacion,datos in estudiantes.items():
      print("-Número:",identifiacion)
      print("-Nombre:",datos[0])
      print ("---------------------------------") 

#PASO 1  - Creamos el diccionario y se rellana con los datos 
estudiantes_activos={1:["JUAN ARTURO VELASQUEZ DUQUE"], 2:["KEVIN JHOAN	ALVIS GOMEZ"], 3:["NATALIA CLAVIJO PINZON"]} 


#PASO 2  - Creamos las nuevas variables y se almaceanan en el diccionario 
print ("---------------------------------") 

print("Cargar estudiantes")

identicacionEstudiante= int (input("Ingrese id del estudiante:  ")) # Variable para asignar la id
nombreEstudiante= str (input("Ingrese nombre del estudiante:    ")) # Variable para asignar la nombre
estudiantes_activos=cargarEstudiante(identicacionEstudiante,nombreEstudiante) # Almacenar las variables

print ("---------------------------------") 
#PASO 4  - Damos la orden de imprimir los datos
print (imprimirListadoEstudiantes(estudiantes_activos)) #Imprimir el listado de estudiantes