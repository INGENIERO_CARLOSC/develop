#PASO 3  - Creamos las facciones que vamos a usar 
#Funcion que se usa para cargar un estudiante a los estudiantes_activos
def cargarEstudiante(identifiacion: int, nombre: str):
    estudiantes_activos[identifiacion]=[nombre]
    print ("*Estudiante ingresado con exito*")
    return estudiantes_activos

#Funcion que eliminar estudiante
def numeroEstudiantes(estudiantes, nombre):
        for identifiacion,datos in estudiantes.items():
            if datos[0].lower()==nombre.lower():
                return identifiacion
        return 0

#Funcion que se usa para imprimir a todos los estudiantes
def imprimirListadoEstudiantes(estudiantes):
    for identifiacion,datos in estudiantes.items():
      print("-Número:",identifiacion)
      print("-Nombre:",datos[0])
      print ("---------------------------------") 

#PASO 1  - Creamos el diccionario y se rellana con los datos 
estudiantes_activos={1:["JUAN ARTURO VELASQUEZ DUQUE"], 2:["KEVIN JHOAN	ALVIS GOMEZ"], 3:["NATALIA CLAVIJO PINZON"]}

#PASO 2  - Creamos una variable para identificar el estudiante que sera eliminado
print("Eliminar Estudiante")
eliminarEstudiante = int (input ("Ingrese id del estudiante a eliminar: ")) #variable para identificar el id 
nombreEstudiante = estudiantes_activos[eliminarEstudiante][0] #variable para guardar el nombre del usuario que sera eliminado
eliminarEstudiante=numeroEstudiantes(estudiantes_activos, nombreEstudiante) #Procedimiento para eliminar al estudiante 
if eliminarEstudiante in estudiantes_activos:  #Procedimiento para eliminar al estudiante 
   del estudiantes_activos[eliminarEstudiante]  #Procedimiento para eliminar al estudiante 
else:  #Procedimiento para eliminar al estudiante 
   print ("El estudiante no se encuentra registrado")  #Procedimiento para eliminar al estudiante 
print ("*Estudiante: ", nombreEstudiante, ", eliminado con exito*") #Mensaje ser eliminado el estudiante 

#PASO 4  - Damos la orden de imprimir los datos
imprimirListadoEstudiantes(estudiantes_activos) #Imprimir el listado de estudiantes
