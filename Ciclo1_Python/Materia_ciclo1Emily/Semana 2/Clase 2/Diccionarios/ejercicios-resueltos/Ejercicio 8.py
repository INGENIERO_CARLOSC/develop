precio = float (input ( "Ingrese el precio: "))
cantidad = float (input ( "Ingrese el cantidad: "))
descuento = float ( input ( "Ingrese el descuento: "))
iva =  int(input ( "Ingrese el iva: "))

dic = {"precio": precio ,"cantidad": cantidad, "descuento": descuento, "iva": iva}

# Calcular el precio de un articulo teniendo
# en cuenta los parametros de descuento a aplicar
if dic["precio"] > 20:
    dic["descuento"] = dic["descuento"] + 2
if dic["cantidad"] < 100:
    dic["descuento"] = dic["descuento"] + 1
elif dic["cantidad"] < 200:
    dic["descuento"] = dic["descuento"] + 2
else:
    dic["descuento"] = dic["descuento"] + 3

dic["euros_descuento"] = (( dic["precio"] / 100 ) * dic["descuento"] )
dic["subtotal_precio"] = dic["precio"] - dic["euros_descuento"]
dic["euros_iva"] = (( dic["subtotal_precio"] / 100 ) * dic["iva"])
dic["precio_final"] = dic["subtotal_precio"] + dic["euros_iva"]
dic["total_factura"] = dic["precio_final"] * dic["cantidad"]


print("Precio: ",dic["precio"])
print("Cantidad: ",dic["cantidad"])
print("Dto: ",dic["descuento"])
print("Iva: ",dic["iva"])
print("Euros Dto: ",dic["euros_descuento"])
print("Precio con Dto: ",dic["subtotal_precio"])
print("Euros Iva: ",dic["euros_iva"])
print("Precio Final: ",dic["precio_final"])
print("Total: ",dic["total_factura"])