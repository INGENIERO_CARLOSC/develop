dic = {}
# Preguntar al usuario por la renta
dic[1] = float(input("¿Cuál es tu renta anual? "))
# Condicional para determinar el tipo impositivo dependiendo de la renta
if dic[1] < 10000:
    dic[2] = 5
elif dic[1] > 10000 and dic[1] <= 20000:
    dic[2] = 15
elif dic[1] > 20000 and dic[1] <= 35000:
    dic[2] = 20
elif dic[1] > 35000 and dic[1] <= 60000:
    dic[2] = 30
else:
    tipo = 45
# Mostrar por pantalla el producto de la renta por el tipo impositivo
print("Tienes que pagar ", dic[1] * dic[2] / 100, "€", sep='')