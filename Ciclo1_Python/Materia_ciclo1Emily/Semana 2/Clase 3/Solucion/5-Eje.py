def main(informacion: dict):
    
    numero_1 = informacion ['numero1']
    numero_2 = informacion ['numero2']

    if numero_1 >= numero_2 and numero_1 % numero_2 != 0:
        print(numero_1 ," no es múltiplo de ", numero_2)
    elif numero_1 >= numero_2 and numero_1 % numero_2 == 0:
        print(numero_1 ," es múltiplo de ", numero_2)
    elif numero_1 < numero_2 and numero_2 % numero_1 != 0:
        print(numero_2 ," no es múltiplo de ", numero_1)
    else:
        print(numero_2 ," es múltiplo de ", numero_1 )


informacion = {
    'numero1': 5,
    'numero2': 5
}  
print(main(informacion))            