def main(informacion: dict):
    
    p_fecha = informacion ['fecha']

    if p_fecha %4 != 0:
        print("El año ", p_fecha ," no es un año bisiesto.")
    elif p_fecha % 100 == 0 and p_fecha % 400 != 0:
        print("El año ", p_fecha ," no es un año bisiesto porque es múltiplo de 100 ", "sin ser múltiplo de 400.")
    elif p_fecha % 4 == 0 and p_fecha % 100 != 0:
        print("El año ", p_fecha ," es un año bisiesto porque es múltiplo de 4 sin ser múltiplo de 100.")
    else:
        print("El año ", p_fecha ," es un año bisiesto porque es múltiplo de 400.")


print("COMPROBADOR DE AÑOS BISIESTOS")
informacion = {
    'fecha': 1999
}  
print(main(informacion))           