def main(informacion: dict):
    
    numero_1 = informacion ['numero1']
    numero_2 = informacion ['numero2']
    numero_3 = informacion ['numero3']

    if (numero_1 == numero_2 != numero_3 or numero_1 == numero_3 != numero_2 or
        numero_2 == numero_3 != numero_1):
        print("Ha escrito uno de los números dos veces.")
    elif numero_1 == numero_2 == numero_3:
        print("Ha escrito tres veces el mismo número.")
    else:
        print("Los tres números que ha escrito son distintos.")


informacion = {
    'numero1': 5,
    'numero2': 5,
    'numero3': 5
}  
print(main(informacion))            