import pandas as pd 

#Convertir un objeto Series con múltiples listas en un único objeto Series.
print("Ejercicio 11")
datos = [["Colombia", "Peru", "Argentina"], ["Bolivia", "Uruguay"], ["Chile"]]
serie = pd.Series(datos)
print(serie)
serie = serie.apply(pd.Series).stack().reset_index(drop=True)
print(serie)
print()

#Ordenar los valores de un objeto Series con el método sort_values.
print("Ejercicio 12")
serieOrdenar = pd.Series(["1.1","Python", "0.5", "Pandas", "200"])
print(serieOrdenar)
serieOrdenar = pd.Series(serieOrdenar).sort_values()
print(serieOrdenar)
print()

#Agregar datos a un objeto Series existente.
print("Ejercicio 13")
serieOrdenar1 = pd.Series(["1.1","Python", "0.5", "Pandas", "200"])
serieOrdenar1=serieOrdenar1.append(pd.Series(["Java", "HTML"])).reset_index(drop=True)
print(serieOrdenar1)
