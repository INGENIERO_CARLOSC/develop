num1 = float(input("Digite el valor 1 : "))
num2 = float(input("Digite el valor 2 : "))
operacion = input("Digite la letra de la operación S=Suma, R=Resta, M=Multiplicacion, D=Division:  ") .lower()

if operacion == "s":
    suma = num1 + num2
    print (f"La suma de los numero {num1} + {num2} = {suma}")
    
elif operacion == "r":
    resta = num1 - num2
    print (f"La resta de los numero {num1} - {num2} = {resta}")

elif operacion == "m":
    mutiplica = num1 * num2
    print (f"La multiplicacion de los numero {num1} * {num2} = {mutiplica}")

elif operacion == "d":
    if(num2 != 0):
        div = num1 / num2
        print (f"La division de los numero {num1} / {num2} = {div}")
    else:
        print ("El division no puede ser cero")
else:
    print("Operacion no válida")