# asignacion de variables multiples con un solo valor
x = y = z = 300
print (x)
print (y)
print (z)

#asignacion de variables multiples con varios valores
a, b, c, d  = 20, 58.9, "Hola que tal" , False
print ("El tipo de dato de la variable a es :")
print ( type(a))
print ("El tipo de dato de la variable b es :")
print ( type(b))
print ("El tipo de dato de la variable c es :")
print ( type(c))
print ("El tipo de dato de la variable d es :")
print ( type(d))