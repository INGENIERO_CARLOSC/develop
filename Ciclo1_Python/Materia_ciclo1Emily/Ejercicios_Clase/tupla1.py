frutas =("naranja", "pera", "uva", "banana")
print(frutas)

#dimension de una tupla
print(len(frutas))

#acceder a un elemento de la tupla
print(frutas[2])

#navegacion inversa
print(frutas[-3])

#modifcar un elemento
#frutas[0]="papaya"

frutasListas = list(frutas)
frutasListas[0] = "papaya"
frutas = tuple(frutasListas)
print(frutas)

#iterar una tupla

for fruta in frutas:
    print(fruta, end=", ")
    
#NO PODEMOS NI AGREGAR NI ELIMINAR  ELEMENTOS DE UNA TUPLA

