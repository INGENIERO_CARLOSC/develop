import pandas as pd
s = pd.Series({"Matematicas":3.5, "Historia":4.3, "Economia":2.3, "Programacion":4.6, "Ingles":3.3})
print(s)
print()

print(s[0:3])
print()

print(s["Historia"])
print()

print(s[["Matematicas", "Programacion"]])
print()
