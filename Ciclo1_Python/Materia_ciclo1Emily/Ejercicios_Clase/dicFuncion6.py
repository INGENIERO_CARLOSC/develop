def edadAlta (diccionario:dict):
    
    name = diccionario["nombre"]
    lastname = diccionario ["apellido"]
    age = diccionario["edad"]
    year= diccionario["year"]
    
    if age >= 0 and age < 18:
        year = 2022 - age
        mayorEdad = year + 18
        return name, lastname,"Eres menor de edad, no podemos darte de alta hasta el año ", mayorEdad
    
    elif age > 18 and age < 25:
        return "Tienes un 10%  de descuento"
    
    elif age >25 and age <100:
        return "Lo sentimos, pero no tienes descuento"
    
    elif age ==18 or age==25:
        return "Premio, tienes un descuento especial del 20%"
    
    elif  age <0:
        return "no se puede calcular la edad"
    
    elif age >=100:
        return "Todavia sigue vivo?"
    
    else: 
       return "dato no valida"
        
nombre = input("ingrese su nombre : ")
apellido = input("ingrese su apellido: ")
edad = int(input("Digite su edad: "))


diccionario ={"nombre":nombre, "apellido":apellido, "edad":edad, "year":0}

print(edadAlta(diccionario))