def convertir(sistema):
    
    def sistema_bin(numero):
        print(f"El numero decimal: {numero}, en binario es {bin(numero)}")
        
    def sistema_oct(numero):
        print(f"El numero decimal: {numero}, en octal es {oct(numero)}")
        
    def sistema_hex(numero):
        print(f"El numero decimal: {numero}, en hexadecimal es {hex(numero)}")
        
    sis_fun = {"bin": sistema_bin, "oct":sistema_oct, "hex":sistema_hex}
    return sis_fun[sistema]

numeroC = int(input("Digite el numero a convertir: "))
convertir ("bin")(numeroC)
convertir ("oct")(numeroC)
convertir ("hex")(numeroC)