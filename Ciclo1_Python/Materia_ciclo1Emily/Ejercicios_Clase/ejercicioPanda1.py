#Escribir un programa que pregunte al usuario por las ventas de un rango de años y muestre por pantalla una serie con los datos de las ventas indexada por los años, antes y después de aplicarles un descuento del 10%.
from numpy import inner
import pandas as pd

inicio = int(input("Digite el año de inicio de las ventas: "))
final = int(input("Digite el año final de las ventas: "))
ventas ={}
print()

for year in range(inicio, final+1):
    ventas[year] = float(input(f"Digite las ventas del año {year}$: "))

print()
ventas = pd.Series(ventas)
print(f"las ventas por años son: \n {ventas}")
print()
print(f"las ventas por años con descuento: \n {ventas*0.9}")



