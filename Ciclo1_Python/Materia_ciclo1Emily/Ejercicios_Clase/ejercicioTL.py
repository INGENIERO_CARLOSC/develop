def cargarPaisesPoblacion():
    paises = []
    for x in range(5):
        nombre = input(f"Ingrese el nombre del pais No. {x + 1}: ")
        cantidad =int(input(f"Ingrese la cantidad del habitantes No. {x + 1}: "))
        paises.append((nombre,cantidad))
    return paises

def imprimir(paises):
    print("Paises y su poblacion")
    for x in range(0,len(paises)):
        print(paises[x][0], paises[x][1])

def paisMasPoblacion(paises):
    pos=0
    for x in range(1,len(paises)):
        if paises[x][1]>paises[pos][1]:
            pos=x
    print(f"El pais con mayor cantidad de habitantes: {paises[pos][0]} con {paises[pos][1]} habitantes")
    
paises=cargarPaisesPoblacion()
imprimir(paises)
paisMasPoblacion(paises)

