import pandas as pd
#Crear y visualizar un arreglo unidimensional como una estructura Series.

datos = [2,3,5,7,11]
serie = pd.Series(datos)
print(serie)
print(serie.size)
print(serie.describe())
print("")

#Convertir un objeto Series a una lista Python.

datos1 = [2,3,5,7,11]
serie1 = pd.Series(datos1)
print(type(datos1))
print(type(serie1))
lista= serie1.tolist()
print(type(lista))
print(lista)
print()
#Usar operadores relacionales para comparar objetos Series
print("Ejercicio 5")
serie2= pd.Series([1,2,3,4,5])
serie3= pd.Series([7,2,8,4,9])
print(serie2)
print(serie3)
print(serie2 < serie3)
print(serie2 > serie3)
print(serie2 == serie3)
print()

#Aplicar las operaciones aritméticas básicas sobre objetos Series.
print("Ejercicio 4")
print(serie2 + serie3)
print(serie2 - serie3)
print(serie2 / serie3)
print(serie2 * serie3)
print()

#Convertir un diccionario Python en un objeto Series.
print("Ejercicio 6")
datos = {"A": 10, "B":20, "C":30}
serie = pd.Series(datos)
print(serie)
print()

#Convertir un arreglo NumPy en un objeto Series.
import numpy as np
print("Ejercicio 7")
arreglo = np.array([1,2,3,4,5])
print(arreglo)
seriea = pd.Series(arreglo)
print(seriea)
print()

#Cambiar el tipo de datos de un objeto Series.
print("Ejercicio 8")
tdatos = pd.Series(["100","200", "300", "400","Minios", "500.2"])
tdatos= pd.to_numeric(tdatos, errors="coerce")
print(tdatos)
print()

#Obtener una columna de un objeto DataFrame como un objeto Series.
print("Ejercicio 9 y 10")
dataF= {"A": [1,2,3,4], "B":[5,6,7,8], "C":[9,10,11,13]}
df=pd.DataFrame(data=dataF)
print(df)
print()
columna = df.iloc[:,2]
print(columna)
print()
fila = df.iloc[0,:]
print(fila)
print(df.iloc[2,1])



