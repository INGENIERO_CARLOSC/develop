import pandas as pd

def resumenCotizaciones(fichero):
    df= pd.read_csv(fichero, sep=";", decimal=",", thousands=".", index_col=0)
    return pd.DataFrame([df.min(), df.max(), df.mean()], index=["Valor Minimo", "Valor Maximo", "Promedio" ])

print(resumenCotizaciones("C:/Users/emily/Desktop/1mision_tic/proyecto/ciclo1/44/semana1/cotizacion.csv"))