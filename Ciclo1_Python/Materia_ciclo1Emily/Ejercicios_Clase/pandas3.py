#atributos de serie
import pandas as pd

s= pd.Series([1,2,3,4,5,3,3,2,5,4,4,4,4])

#Conocer el tamaño de una serie
print(s.size)
print()

#Devolver donde inicia y tamaño de la serie
print(s.index)
print()

#Devuelve el tipo de dato de la serie
print(s.dtype)
print()
