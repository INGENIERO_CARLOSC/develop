import pandas as pd

#GENERAR UN DATAFRAME CON LOS DATOS DEL FICHERO

titanic = pd.read_csv("C:/Users/emily/Desktop/1mision_tic/proyecto/ciclo1/44/semana1/titanic.csv")

#MOSTRAR POR PANTALLA LAS DIMENSIONES QUE TIENE EL FICHERO
#EL NUMERO DATOS QUE CONTIENE LOS NOMBRES DE INDICES Y FILAS

print("Dimensiones del archivo son :", titanic.shape)
print(f"Numero de elementos del archivo son: {titanic.size} ")
print(f"El nombre de las columnas son: {titanic.columns}")
print(f"El nombre de las filas son: {titanic.index}")
print(f"El tipo de datos del archivo es : {titanic.dtypes}")
print(f"Las primeras 10 filas son:\n {titanic.head(10)} ")
print(f"Las ultimas 10 filas son:\n {titanic.tail(10)} ")

#MOSTRAR POR PANTALLA LOS DATOS DEL PASAJERO CON ID 148
print(f"El pasajero buscado es : {titanic.loc[148]}")

#MOSTRAR POR PANTALLA LAS FILAS PARES DEL DATAFRAME
print (f"Filas de pares \n {titanic.loc[range(0,titanic.shape[0],2)]}")

#MOSTRAR POR PANTALLA LOS NOMBRES DE LAS PERSONAS QUE IBAN EN PRIMERA CLASE ORDENADAS ALFABÉTICAMENTE.

print(titanic[titanic["Pclass"]==1]["Name"].sort_values())



