nombres = ["alex","andres","camilo","constanza","dora","fabian"]
print(type(nombres))
print(nombres)

#conocer la dimension de la lista
print(len(nombres))

#conocer la informacion individual de una lista
print(nombres[3])
print(nombres[5])

#navegacion inversa
print(nombres[-6])
print(nombres[-2])

#imprimir un rango
print(nombres[2:5])
print(nombres[1:4])

#cambiar elementos de una lista
nombres[2]="daniel"
print(nombres)

#iterar en una lista
for nombre in nombres:
    print(nombre)
    
#Revisar que un elemento exista
if "camilo"in nombres:
    print("se encuentra en la lista")

else:
    print("no se encuentra en la lista")
    
#agregar informacion al final de la lista
nombres.append("anthony")
print(nombres)

# agregar un elementos en posicion
nombres.insert(3,"santiago")
print(nombres)

#remover elementos por el nombre
nombres.remove("santiago")
print(nombres)

#remover el ultimo elemento de la lista
nombres.pop()
print(nombres)

#remover elemento especifico de la lista posicion
del nombres[1]
print(nombres)
del nombres[1]
print(nombres)

#eliminar por completo la lista
nombres.clear()
print(nombres)
print(type(nombres))