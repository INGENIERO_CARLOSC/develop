#Escribir una función que reciba un diccionario con las notas de los alumnos en curso en un examen y devuelva una serie con la nota mínima, la máxima, media y la desviación típica.

import pandas as pd

notas = {"anderson": 0, "Beatriz": 0, "Camilo":0, "Dora":0, "Anthony":0, "Carlos"
         :0}

for keys in notas:
    notas[keys] =float(input(f"Digite la nota de {keys}: "))

def estadisticasNotas (notas):
    notas = pd.Series(notas)
    estadisticas = pd.Series([notas.min(), notas.max(), notas.mean(), notas.std()], index=["Nota Minima","Nota Maxima", "Nota Promedio ", "Desviacion Estandar"])
    return estadisticas

print(estadisticasNotas(notas))