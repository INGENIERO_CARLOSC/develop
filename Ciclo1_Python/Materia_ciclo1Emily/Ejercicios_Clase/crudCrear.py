estudiantesActivos={ 1512:["Angela Benavides"], 5478:["Anthony Racors"], 2530: ["Camilo Jaramillo"], 5812:["Dora Saldaña"]}

print("Carga estudiantes")
idEstudiante = int(input("Digite el Id del estudiante: "))
nombreEstudiante= str(input("Digite el nombre del estudiante: "))

def cargarEstudiante(identificacion:int, nombre:str):
    estudiantesActivos[identificacion]=[nombre]
    print("Informacion cargada con exito")
    return estudiantesActivos

def imprimirEstudiantes(estudiantes):
    for identificacion, nombre in estudiantes.items():
        print(f"El ID del estudiantes es: {identificacion}")
        print(f"El nombre del estudiante es: {nombre}")
        print("----------------------------")

estudiantesActivos = cargarEstudiante(idEstudiante, nombreEstudiante)
imprimirEstudiantes(estudiantesActivos)

