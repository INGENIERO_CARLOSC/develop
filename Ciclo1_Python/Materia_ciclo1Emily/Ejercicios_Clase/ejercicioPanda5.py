#Escribir una función que reciba un DataFrame con el formato del ejercicio anterior, una lista de meses, y devuelva el balance (ventas - gastos) total en los meses indicados.
import pandas as pd

mes = []
ventas =[]
gastos=[]

cuantos =int(input("Cuantos meses desea ingresar: "))
while cuantos<1 or cuantos >12:
    cuantos =int(input("Cuantos meses desea ingresar un valor entre 1 y 12: "))

for i in range(cuantos):
    mes.append(str(input(f"Digite el mes {i+1} " )).capitalize())

for i in mes:
    ventas.append(float(input(f"Ingrese el valor de las ventas en el mes {i}: ")))
    gastos.append(float(input(f"Ingrese el valor de los gastos en el mes {i}: ")))

datos = {"Mes":mes, "Ventas":ventas, "Gastos": gastos}

contabilidad = pd.DataFrame(datos)

def balance(contabilidad, meses):
    contabilidad["Balance"] = contabilidad.Ventas - contabilidad.Gastos
    return contabilidad[contabilidad.Mes.isin(meses)].Balance.sum()
print()
print(contabilidad)
print()
mes1=str(input("Digite el mes 1 para el balance: ").capitalize())
mes2=str(input("Digite el mes 2 para el balance: ").capitalize())
print()
print(f"El balance del mes {mes1} y del mes {mes2} es: {balance(contabilidad, [mes1, mes2])}")