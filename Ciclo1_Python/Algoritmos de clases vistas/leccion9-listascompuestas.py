#----listas compuestas---almacena listas dentro de listas-------
#--- anidacion de listas ---matrices--
# notas = [[4,5,4],[3,6,7],[7,3]]

# for f in range(len(notas)):
#     for c in range(len(notas[f])):
#          print(notas[f][c])
#     print("\n")
         

#--- lista por asignacion--suma de elementos de listas-

# lista = [[1,1,1],[2,2,2]]

# for f in range(len(lista)):
#     suma = 0
#     for c in range(len(lista[f])):
#         suma +=lista[f][c]
#     print(suma)

#--------------crear una lista ---y hacer una suma general ---------

lista1= [[1,4,5],[1,1,1],[4,8,9]]
suma = 0
for f in range(len(lista1)):
    
    for c in range(len(lista1[f])):
        suma +=lista1[f][c]
print(suma)

#---- carga dinamica de listas ---------------
padres=[]
hijo=[]
for k in range(3):
    pa= input("Ingrese el nombre del padre: ")
    ma= input("Ingrese el nombre del madre: ")
    padres.append([pa,ma])
    cant = int(input("Ingrese la cantidad de hijos : "))
    hijo.append([])

    for x in range(cant):
        nom = input("Ingrese el nombre del hijo")
        hijo[k].append(nom)

print("Listado del padre madre e hijos")
for k in range(3):
    print("Padre: ",padres[k][0])
    print("Madre: ",padres[k][1])
    for x in range(len(hijo[k])):
        print("Hijos: ", hijo[k][x])

print("listado del padre y cantidad de hijos que tiene")
for x in range(3):
    print("Padre: ", padres[x][0])
    print("Cantidad de hijos: ",len(hijo[x]))

