#desiciones generalizadas --ALL--ANY 
# all recibe objetos iterables



uids = ['B1CD102354', 'B1CDEF2354']

for i in uids :
    cond=[]
    # propiedad isupper() verifica la existencias de letras mayuculas
    #letras mayusculas en alfabeto /len digitos/lis es una lista/ filter para solo mosotrar elementos mayuculas/ lambda convierte lo que tiene i en una lista
    cond.append(len(list(filter(lambda x: x.isupper(), list(i)))) >=2)
    #validar que tenga 3 digitos ----
    cond.append(len(list(filter(lambda x: x.isdigit(), list(i)))) >=3)
    #--- validacion solo alfanumerico nada de #$%&&/ propiedad  isalnum() verifica la existencias de caracteres
    cond.append(len(list(filter(lambda x: not(x.isalnum()), list(i)))) ==0 )
    #--- validar que no existan repeticiones---
    cond.append(len(i) == len(set(i)))
    #--- exactamente 10 caracteres 
    cond.append(len(i)==10)

    print("valido") if all(cond) else print ("invalido")