class Persona: #self convierte los datos de entrada para la clase
    def __init__(self, nombre, edad): #creando las variables de la clase
        #objetos
        self.nombre = nombre
        self.edad = edad
        
    def __str__(self): # funcion que strings __str__ retorna strings
        return "{} tiene {} años ".format(self.nombre, self.edad)

#lista
listapersonas = [
    #objetos
    Persona("carlos",23),
    Persona("maria",3),
    Persona("pedro",23),
    Persona("juan",13),
    Persona("mario",2)
]

#funcion anonima para filtrar la edad (per es la variable global de lambda comparada con la lista)
menoresEdad = filter(lambda per: per.edad <18,listapersonas)
for menor in menoresEdad: #menor es la variable global del for recorriendo la funcion lambda
    print(menor)

