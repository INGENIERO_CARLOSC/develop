'''
En Python existendiferentesestructurasde datos las cuales nos permitenalmacenar y
gestionar diferentes tipos de datos, por ejemplo tenemos a las listas,las tuplas y a los
diccionarios.
• Hoy vamos a hablarde los diccionarios,la estructuracaracterísticaqueposee Python
• El diccionario, defineuna relaciónuno a uno entre una clave y su valor.
• Los diccionarios en Python, al igual que las listas y las tuplas, nos permiten almacenar
diferentestiposde datos: Strings,enteros,flotantes,booleanos,tuplas,listase inclusive
otros diccionarios.
• Los diccionarios son mutables,es decir,es posiblemodificar su longitud,podemosagregaro
quitar elementos de él; de igual forma todos los valores almacenados en el diccionario
puedensermodificados.
'''

'''
Estructura!!!!

El nombrede cada variable, ahora será la llave (key) de
cada elemento deldiccionario.
• El contenido de cada variable, ahoraserá el valor (value)
de cada elemento deldiccionario.
• Los diccionarios son contenedores, dondecada elemento
(ítem) que contienen presenta la estructurallave:valor.
• Los ítems pueden ser de diferentetipo de dato: string,
float,int,bool oincluso otro diccionario.
'''
#======mas usado ============DICCIONARIOS=========clave y valor=========
#primer_diccionario = { # se crea con llaves
    #"nombre": "carlos",
    #"apellidos": "cogua",
    #"edad": 30,
    #"telefono": True
#} 
#print(primer_diccionario)
# ==Otra forma no tan comun de usar ===funcion dict para crear un diccionario======
#diccionario2= dict(
    #nombre= "pedro",
    #apellido = "rojas",
    #telefono = 310887562
#)
#print(diccionario2)
#===========Acceder a datos dentro del diccionario con ej ejemplo anterioir==========================
#print(diccionario2["nombre"]+ " "+primer_diccionario["apellidos"]) #concatenando y estrallendo datos de 2 diccionarios diferentes
#print(diccionario2["telefono"],primer_diccionario["edad"])

#=========condicional =usando datos de ===========
#if primer_diccionario["edad"] >18:
    #print("El estudiante "+primer_diccionario["nombre"].upper()+" es mayor de edad")

#else:
    #print("El estudiante "+primer_diccionario["nombre"]+"es menor de edad")
#====Sobreescribir datos del diccionario==================================
#diccionario2["edad"]=25
#diccionario2["nombre"]= "nuevonombre"
#print(diccionario2)
#=====================llenar diccionario con input ===================
# diccionario3 = {}
# '''  llenando el diccionario'''
# nombre= input("ingrese nombre")
# apellido = input("ingrese apellido")
# edad = input("ingrese la edad")

# ''' reasignando valores en el diccionario '''
# diccionario3["nombre"]= nombre
# diccionario3["apellido"]= apellido
# diccionario3["edad"]= edad
# print(diccionario3)

# #=======diccionario anidado =========
# dicc4 ={
#     "nombre": "pepe",
#     "apellido": "mujica",
#     "asignatura": {
#         "materia": "matematicas",
#         "horario": "nocturno"
#     }
# }

# print("El alumno "+dicc4["nombre"] +" tiene la asignatura "+ dicc4["asignatura"]["materia"] +" en la jornada "+dicc4["asignatura"]["horario"])
#======Encapsulamiento con diccionarios=============

# def promedionotas(dicnotas: dict):
#     sumatoria = 0
#     sumatoria += dicnotas["nota1"]
#     sumatoria += dicnotas["nota2"]
#     sumatoria += dicnotas["nota3"]
#     sumatoria += dicnotas["nota4"]
#     promedio= round(sumatoria/4, 2)  # 2 decimales con round
#     return promedio
# # --- creando diccionario -----------
# dicnotas={
# }
# # ----- llenando con input ------

# dicnotas["nota1"]= float(input("ingrese la nota 1 "))
# dicnotas["nota2"]= float(input("ingrese la nota 2 "))
# dicnotas["nota3"]= float(input("ingrese la nota 3 "))
# dicnotas["nota4"]= float(input("ingrese la nota 4 "))
# # ----------validar resultado de promedio--
# variablepromedio= promedionotas(dicnotas) # almacenando valores en una variable para consumir menos recursos
# if promedionotas(dicnotas)> 3 :
#     print("El estudiante gano el promedio es: " + str(variablepromedio)) # enviamos el diaccionario  a la funcion para mostrar el resultado
# else:
#      print("El estudiante perdio el promedio es: " + str(variablepromedio)) # enviamos el diaccionario  a la funcion para mostrar el resultado
#--- metodos de diccionarios-----

#//////////////////////////ejercicio de la guia////////////////revisar whatssap////////////////////////////////

def reportarPromedio(dicReporte):
    return dicReporte["estudiante"]+ " = " +str(dicReporte["promedio"] +" y fue " + dicReporte["estado"]) #--muestra el resultado-- envia 
#------fin de la funcion reportarPromedio  ---------------
def generarReporteNotas(dicNotas): #----recibe todos los datos del diccionario dictnotas
    sumatoria = 0
    sumatoria += dicNotas["nota1"]
    sumatoria += dicNotas["nota2"]
    sumatoria += dicNotas["nota3"]
    sumatoria += dicNotas["nota4"]
    promedio = round(sumatoria/4, 2)    
    #----------validado promedio--- -----
    if promedio >= 3.0:
        estado = "aprobado"
    else:
        estado = "reprobado"
        
    #---almacenando el valor del promedio y el nombre del estudiante para enviar a generarreportedenotas()
    reporteNotas={
        "promedio": promedio,
        "estudiante": dicNotas["estudiante"]
    }
    
    return reporteNotas
   
#-----------fin de la funcion generarReporteNotas-------

#---------diccionario vacio------
dicnotas ={    
}
#-------------llenando el diccionario--------------------
dicnotas["estudiante"] = input("Ingrese el nombre del alumno: ")
dicnotas["nota1"] = float(input("Ingrese la nota 1: "))
dicnotas["nota2"] = float(input("Ingrese la nota 2: "))
dicnotas["nota3"] = float(input("Ingrese la nota 3: "))
dicnotas["nota4"] = float(input("Ingrese la nota 4: "))

#-----------enviando parametros y mostrando datos      
print(reportarPromedio(generarReporteNotas(dicnotas))) #-  enviando datos a la fuincion anidada 
