#--- funciones lambda o anonimas ----
# suma = lambda val1 =0, val2 =0: val1 +val2
# operacion = lambda funcion, val1=0, val2=0 : funcion(val1,val2)

# resultado = operacion(suma, 15, 20)
# print(resultado)

#/////////////////////////////////////

#sinparametros = lambda : True

#Convalores = lambda val, val1=10, val2=1 : val + val1+ val2

#--------------funcion anonima con argumento definido
# from ast import arg
# conAsterisco= lambda *args: args[1] + args[2] #permite recibir cualquier cantidad de parametros
# res= conAsterisco(11,23,12,10)
# print(res)

#----------funcion anonima con diccionarios ---------------------------------
# ConDobleAsterisco = lambda **kwargs : kwargs["uno"] # recibe un diccionario con elementos clave valor
# resultado = ConDobleAsterisco(uno =1,dos =2, tres=3)
# print(resultado)

#conAsterisco = lambda *args, **kwargs : 