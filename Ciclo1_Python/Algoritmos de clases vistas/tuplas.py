#TUPLAS  --- son INMUTABLES, se separa con , y se usa (), 
from sre_constants import RANGE_UNI_IGNORE


frutas = ("Naranja", "Pera", "Uva", "Manzana")
print(type(frutas))

#-----------dimension de la tupla
print(len(frutas))

#--------------Acceder a un elemento de la tupla
print(frutas[2])

#-----------------Navegacion a l inversa
print(frutas[-1])

#----------------Modificar un elemento de una tupla

frutaslista=list(frutas)
frutaslista[0]="Papaya"
frutas=tuple(frutaslista) #nueva tupla
print(frutas)
print("------------------------------------------------------------")
#--------------ejercicio---cargar tuplas dentro de una lista ---------
'''
almacenar una lista con 5 elementos de tuplas
guarde el nombr de un pais y la cantidad de habitantes

Definir 3 funciones:

1. cargar la lista
2. imprimir 
3. mostrar el nombre con mayor cantidad de habitantes
'''

def funcion1_cargar():
    paises =[] #lista que almacenara tuplas
    for i in range(2):
        nombre = input("Ingrese el nombre del pais: ")
        cant = int(input("Ingrese la cantidad de habitantes: "))
        paises.append((nombre,cant)) # () creando las tuplas dentro de la lista paises
    return paises #retornando la lista para ser usado por las otras funciones

def funcion2_imprimir(paises: list):
    print("Paises con su poblacion")
    for i in range(len(paises)):#recorriendo las veces que hay cantidades en listas
        #se coloca paises[][] 2 veces por que se esta recorriendo 2 datos que contiene cada tupla (nombre,cant) 
        print(paises[i][0], paises[i][1]) # [0][1] son la sposicines estaticas de la tupla [i] es la iteracion de la lista recorre las tuplas dentro de la lista
        
def funcion3_mayorP(paises: list):
    posicion= 0 #variable para comparar el indice del pais com mayor cantidad
    for i in range(len(paises)): 
        #posicion almacena el indice del pais con mayor habitantes
        if paises[i][1] > paises[posicion][1]: #recorriendo 
            pos=i #almacena en pos la iteracion de i
    print("El pais con mayor catidad de havitantes es ",paises[posicion][0]) #---muestra el valor mayor


#--------------invocando las funciones ----------------
# paises= funcion1_cargar() # ejecutando el llenado de la lista paises con la funcion
# funcion2_imprimir(paises) #imprimiendo la lista
# funcion3_mayorP(paises) #filtrando el pais con mayor gente


# ----2do ejercicio de tuplas----validando tuplas  con while--------------
'''
crear una tupla que 
'''

meses = ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio ", "Julio", "Agosto", "Septiembre","Octubre","Noviembre","Diciembre")
salir = False
while(not salir):
    numero = int(input("Porfavor digite un numero: "))
    if numero == 0:
        salir = True
    elif numero >=1 and numero <= len(meses):
        print(meses[numero-1])#muestra la posicion real 
    else:
        print(f"Porfavor escriba un numero entre el 1 y {len(meses)} ")
