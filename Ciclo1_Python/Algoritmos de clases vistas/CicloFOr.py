# ----ciclo bajo cumplimiento de rangos rangos ---------For-----------
# for x in range(0,3): # x es una variable local que solo se usa en el proceso de iteracion al inicip x vale 0 y va aumentando
#     print("Estamos en la iteracion ",x)

#------------almacenando strins en una variable----
# for letra in "colombia":
#     if letra == "o":
#         print(letra)
#         break

#----------tabla de multiplicar con for ------------------
# numero= int(input("Digite el numero de la tabla: "))
# for i in range(1,11):
#     resultado= numero*i
#     print(f"{numero} x {i} = {resultado}")

#--------------usando end---------------------------------
print("Comienzo")
for i in [0,1,2,3,4,5]:  #--- forma de presentar los rangos x cantidad de elementos
    print("Hola", end = " ") #end imprime de forma horizontal
print()
print("Final")