#----datos ordenados --mutables-----contenedor de estructura ordenadas o secuencial --
#------[],,,---secuencial mutable Listas  igual que los diccionarios --------------

#-----creando lista-mixta---
prueba = [1, 2, 3, 'ejemplo', [50,"telefono"], 4, 200]
print("primera lista ",prueba)
#-----consultas para listas ---
print("consulta 1, atra vez del indice ->  ",prueba[3])
print("consulta 2, atra vez derangos de indices: ->  ",prueba[0:2])
#----asiganar valores a las listas atrevez de variables ---
var1 = 23
var2 = 11
var4 = ["nueva", "lista"]
listaVariable = [var1,var2, 67, ["listado", True], 200, 3, 3, 3, 3]
print("sin modificar   ",listaVariable)
# modificando el valor segun el indice
listaVariable[2] = 100
# agregar un elemento a lo ultimo de la lista
listaVariable += [1000]
print("listas con variables modificada  ",listaVariable)
#---propiedades
print("cantidad de elementos de las listaVariable ", len(listaVariable))
print("Busca si existe un elemento en la lista ", listaVariable.append(12))
print("cuenta la cantidad de veces que se repite el elemento 3 en el listado", listaVariable.count(3))
#----extendiendo una lista o agregando elemntos de una lista dentro de  otra
listaVariable.extend(var4)
print("lista extendida ",listaVariable)
#---metodo index recibe argumento y devuelve el indice 
print("mtodo index muestra el indice del primer elemento buscado ",listaVariable.index(200))

#-- metodo pop elomina el ultimo elemento de la lista y muestra que elimino
print(listaVariable.pop())
print(listaVariable)

#-- metodo remove -borra un elemnto especifico---
listaVariable.remove(200)
print(listaVariable)

#-- metodo reverse incvierte el orden de la lista
listaVariable.reverse()
print(listaVariable)

#-- metodo sort se usa cuando todos los valores son enteros Ordena de manera ascendente los elementos de la lista---
#listaVariable.sort()
print(listaVariable)

#------mostrar elementos de una lista usando for ---buscar en pdf los ejemplos --
listanueva = [2,3,4,5]
for item in range(len(listanueva)):
    print("posicion: " + str(item)+ "valor " + str(listanueva[item]))
# consultar como recorrer listas anidadas con for anidado

listanueva = [2,3,4,5]
for item  in enumerate(listanueva):
    print("posicion: " + str(item)+ "valor " + str(v))
# consultar como recorrer listas anidadas con for anidado