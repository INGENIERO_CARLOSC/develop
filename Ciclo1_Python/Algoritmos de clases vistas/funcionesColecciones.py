#-----------maniuplar enormes colecciones de datos------

#-MAP-- una funcion a cada uno de los elemnbtos de la coleccion lista, tuplas, conjuntos
#-se usa esta funcion cada vez que quiera transformar el valor del elemento en otro
#--- hayar el cuadrado de todos los elementos de la lita
#def cuadrado(numero:int): #recive un elemento
#     return numero * numero

# lista = [1,2,3,4,5,6,7,8,9,10]
#-es silimiar a un for ---map permitio crear una nueva lista con el resultado de la operacion
# Resultado = list(map(cuadrado,lista))
# print(Resultado)

#  ------- otro ejemplo pero con lambda--------------------

# resultado1 = list(map(lambda numero: numero * numero, lista))
# print(resultado1)

#---EJEMPLO "2"----------------usando librerias ----------------
#---elevar ala potencia de otra lista
# from math import pow #importando la funcion especifica pow de la libreria math

# numero= [4,2,3]
# potencias=[3,3,3]
# potemciados = list(map(pow, numero,potencias))
# print(potemciados)

#--- ejemplo 3 Filter---------nos deja filtrar los elementos de la coleecion----------
#--- retorna un valor voleano , true o false

# def mayor5(elemento): #mostrar los numeros mayores a 5
#     return elemento >5

# tupla = (2,6,7,3,4,5,9,8,7,5)
# resultado4 = tuple(filter(mayor5,tupla)) #el resultado debe ser una tupla
# print(resultado4)

# ------- FUNCION REDUCE   --->reto4 ciclos, funciones lambda,map
#--- cuando tenemos una coleccion de elementos y necesitamos un unico resultado 
#--- no permite reducir los elementos -- es un contador

#-----acumulacion o suma de los valores------evitar usar for para cantidades enormes de datos-----------
#importamos libreria
from functools import reduce

lista = [1,2,3,4]

def funcion_acumulador(acumulador=0, elemento =0):
    return acumulador + elemento #operacion

resultado = reduce(funcion_acumulador,lista) #usamos la funcion reduce enviando a la funcion la lista e imprimimos la variable resultado
print(resultado)

#-------------ejemplo pero con lambda

resul= reduce(lambda acum = 0, elem=0: acum +elem, lista)
print(resul)

#----2do ejemplo ---con strings-------------------

lista2 = ["python","java","ruby"]
R = reduce(lambda acomulador= " ", elemento = "": acomulador +"-"+elemento,lista2)
print(R)

#------funcion Zip-------combina las listas en tuplas individuales----------------
#---sirve para reorganizar listas----recibe un conjunto de listas y toma cada uno de los elemento y los une en un tupla y une las tuplas en 1 sola lista

nombres = ["Raul", "Juan"]
apellidos = ["Lopez","Perez"]

nombrescomlpetos = list(zip(nombres,apellidos))
print(nombrescomlpetos)

