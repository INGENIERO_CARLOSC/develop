def doble (numero):
    return numero**2


numeros =[4,8,12,20]

print(list(map(doble,numeros))) #map permite reccorrer como un for el listado
print("Otra forma pero con funcion anonima usando map y lamnda")
print(list(map(lambda variableL: variableL**2,numeros)))

#----------------otro ejemplo
a=[1,2,3,4,5,6]
b=[2,2,2,2,2,2]
c=[5,5,5,5,5,5]
print("------------------------------------------------------")
#almacena los valores en una nueva lista List
print(list(map(lambda A,B,C: A*B*C, a,b,c)))


