library(utils)

library(dplyr)
# debe usar la url version raw en git hub  o git lab 
dataset <- read.csv("https://gitlab.com/INGENIERO_CARLOSC/develop/-/raw/main/AnaliticaR/Fundamentos%20R/mtcars.csv")
head(dataset) #muestra los 5 filas primeras del dataset / tambien desde consola podemos ver el dataset con View(dataset)
dim(dataset) #muestra la dimencion dl dataset

str(dataset) #muestra la estrcutura del data set y los tipos de variable de cada dolumna
class(dataset$hp) #vera el tipo de columna o variable del dataset

#conversion a variables logicas
dataset$hp = as.logical(dataset$hp)

#--------------------------factores, listas, datasetss---------------------
Nivel_curso <- c("Basico","Intermedio","Avanzado")
Nivel_curso

head(dataset)
tail(dataset)# muestra ultimos 5 filas

#mostrando  filas especificas del dataframe
my_dataframe <- dataset[1:4,]
my_dataframe 

#-----------------------Cruces de variables---------------------------------------------

# ----["todas las observaciones o filas ",coulmnainicial:columna final ]
pairs(dataset[,2:6])

# otro ejemplo ----
newdata <- subset(dataset, select =c(2,7:8,11,12))
pairs(newdata)


# como hacer filtros  Hacer filtros / vehiculos con mas de 30 millas por galon

Autos_eficientes <- filter(dataset, mpg >=30)
Autos_eficientes

# parejas de correlacion de las variables 

pairs(Autos_eficientes[,2:6])

#==========Desviacion estandart "datos que se alejan negativa o positivamente del promedio"===


# hallando coeficiente de variacion con una variebale que contiene el promedio

prom <- mean(dataset$mpg) #promedio dataset observaciones  millas por galon almacenado en prom
desv <- sd(dataset$mpg) #desviacion estandar almacenada en una variable

#formula para hallay coeficiente de variacion  desviacion estandar / promedio *100
CoefVar <- (desv/prom)*100
CoefVar #nos muestra un porcentaje si es > al 25 por ciento quiere decir que el promedio no es confiable 
#hay valores significativos que se alejan del promedio y afectan el analisis


#****************************************************************************************************
#********************************Generando tablas, filtrando y seleccionando datos - dplyr-Parte 1********
#*#****************************************************************************************************
# -----------Vehiculos mas eficientes ------------
eficientes <- mean(dataset$mpg) # promedio de millas por galon

#Mutate crea una columna en el dataset original  y la llena deacuerdo a la decision
mtcars <- dataset %>%
  mutate(Mas_eficientes = ifelse(mpg< eficientes,
                                "Bajo promedio", "En o sobre el promedio" ))

# -----------Vehiculos mas veloces ------------
Mas_veloces <- mtcars[mtcars$qsec <16, ] #guarada en mas_veloces el nuevo dataset con kilo*segundo menosres a 16 segundos
# la coma despues del 16 indica que se aplique a todas las observaciones
Mas_veloces

#añadiendo categorizacion nueva de segundos cuartos de milla
mtcars <- mtcars %>% 
  mutate(Velocidad_cuarto_milla  = ifelse(qsec < 16,
                                          "Menos de 16 segundos","Mas de 16 segundos"
    
  ))

#----Columna nueva -----Peso del vehiculo en kilos-----
mtcars <- mtcars %>% 
  mutate(Peso_Kilos= (wt/2)*1000)

#--------CAtegorizando el peso en base a la columna nueva Peso_Kilos--------------

mtcars <- mtcars %>% 
  mutate(Peso= ifelse(Peso_Kilos <= 1500,
                      "Livianos", "Pesados"))

#****************************Visualizacion de datos ***********************************
# clasificando en orden descendente los behiculos por pesos
mtcars %>%
  arrange(desc(Peso_Kilos))

#filtrando en una nueva variable

Mas_pesados <- mtcars %>%
  filter(model %in% c("Lincoln Continental", "Chrysler Imperial",
                      "Cadillac Fleetwood", "Merc 450SE")) #model es una columna del dataset busca en un contenedor con categorias

Mas_pesados

# visualizando la informacion 
ggplot(Mas_pesados, aes(x= hp, y= mpg))+
  geom_point()+
  facet_wrap(~model) #facet_wrap permite tener diferentes observaciones en la misma graficas


#---------- grafica donde se evaluan milla spor galon, cilindros, de autos manuales y automaticos con sus pesos en kilos
#--Categorizando el tipo de vehiculo
mtcars <- mtcars %>% 
  mutate(CategoriaV = ifelse(am <= 0,"Automatico", "Manual" ))

#diagramando 
ggplot(mtcars, aes(x= cyl , y= mpg, size = Peso_Kilos))+
  geom_point()+
  facet_wrap(~CategoriaV)
  
