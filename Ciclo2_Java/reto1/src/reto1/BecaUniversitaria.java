/** 
 Descripción del problema
Una operadora de becas estudiantiles requiere adicionar una funcionalidad al sistema de
información que tiene actualmente. Esta nueva funcionalidad implica que el sistema calcule
los intereses generados sobre un monto determinado si se utiliza una proyección con interés
compuesto, una proyección con interés simple y una proyección con la diferencia de estos.
Los montos mencionados están relacionados con las becas universitarias (de pregrado y
posgrado) que se van a otorgar por parte de la operadora a los potenciales beneficiarios. Las
ecuaciones para determinar los intereses son las siguientes: 
 */

/**
Requerimientos :
* 
1) Crear clase 'BecaUniversitaria' ✔️
 * 2) Crear método compararInversion -> Sobrecar de métodos con parámetros y sin parámetros
 *      parámetros:
 *          * tiempo
 *          * monto
 *          * interes
 *      Retornar un String:
 *       *"La diferencia entre la proyección de interés compuesto e interés simple es: $" + diferencia
 *      En el caso de que no se envien datos o se instancie con el constructor sin parámetros:
 *       *"No se obtuvo diferencia entre las proyecciones, revisar los parámetros de entrada."
 * 
 * Aplicar sobrecarga de métodos al constructor.
 * 
 * NOTA:
 *  Redondear los resultados con Math.round()
 * 
 * 
 * FÓRMULAS:
 * interesSimple = monto * (interes/100) * tiempo ✔️
 * interesCompuesto = monto * ( (1+ (interes/100))^tiempo - 1 ) ✔️
 * compararInversion = interesCompuesto - interesSimple 

/** 
 Para integrar esta nueva funcionalidad al sistema de información de la operadora, se solicita
crear una clase llamada BecaUniversitaria, la cual debe tener el método
compararInversion(). Dicho método, podrá recibir como parámetros las entradas (int
pTiempo, double pMonto, double pInteres), o no recibir ningún parámetro, y realizar la
comparación a partir de los atributos de la clase. El método compararInversion() debe
utilizar los métodos calcularInteresSimple() y calcularInteresCompuesto(), los cuales
deben retornar el total de interés simple y compuesto a partir de las ecuaciones dadas, en
formato double, sin recibir parámetros de entrada.
Tener en cuenta que si no se pasan argumentos al constructor de la clase BecaUniversitaria,
sus atributos deben ser inicializados en cero.
 
 */
package reto1;


public class BecaUniversitaria {
    
    //1-ATRIBUTOS o variables globales
    private int pTiempo;
    private double pMonto;
    private double pInteres;

    //2-------------Metodos CONSTRUCTORES me permiten crear objetos de las clases - es el puente para crear un objeto--------------------
    public BecaUniversitaria(int pTiempo, double pMonto, double pInteres){ /**parametros recibidos locales*/
        //this.pTiempo -> Atributo //pTiempo -> Parámetro        
        this.pTiempo = pTiempo; //this es el contexto de la clase a la cual voy a crear el objeto
        this.pMonto = pMonto;
        this.pInteres = pInteres;
    }

    public BecaUniversitaria(){ //3--------------------metodo para inicializar en cero los atributos--------------------------------------
        // por buenas oractocas todos los atributos deben inicializarse en los metodos contructores y no en laas variables globales POO
        this.pTiempo = 0;
        this.pMonto = 0.0;
        this.pInteres = 0.0;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //ACCIONES / MÉTODOS
    
    public String compararInversion(int pTiempo, double pMonto, double pInteres){ //metodo para hallar resultado de la resta del interesComp- Interes simple
        this.pTiempo = pTiempo; // this se utiliza para diferenciar un atributo de una variable local o de un parametro
        this.pMonto = pMonto;
        this.pInteres = pInteres;
        double diferencia = calcularInteresCompuesto() - calcularInteresSimple();
        return "La diferencia entre la proyección de interés compuesto e interés simple es: $" + diferencia; //retornando un mensaje con el resultado de la resta
    }

    public String compararInversion(){ //MEtodo string para retornar un mensaje cuando no se reciben parametros 
         if(pInteres == 0 && pTiempo == 0 && pMonto == 0){
            return "No se obtuvo diferencia entre las proyecciones, revisar los parámetros de entrada.";
        }else{
            double diferencia = calcularInteresCompuesto() - calcularInteresSimple();
            return "La diferencia entre la proyección de interés compuesto e interés simple es: $" + diferencia;
        } 
        
    }

    public double calcularInteresSimple(){ // 4- primera operacion que pide el ejercicio - funcion dooble por las operaciones
        double interesSimple = Math.round( pMonto * (pInteres/100) * pTiempo ); //atributos de los constructores, mathround es para redondear los decimales resultantes
        return interesSimple;//retornamos el resultado de la operacion
    }

    public double calcularInteresCompuesto(){ //5- segunda operacion solicitada por el ejercicio
        double resultado = Math.pow((1+ (pInteres/100)), pTiempo); //Variable auxiliar para hacer la operacion mas corta y entendible / potencia y operacion
        double interesCompuesto =  Math.round(pMonto * ( resultado - 1 )) ; //redondeando el resultado
        return  interesCompuesto;
    }
    
}
