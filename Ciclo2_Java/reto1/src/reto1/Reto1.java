
package reto1;
/** 
 Descripción del problema
Una operadora de becas estudiantiles requiere adicionar una funcionalidad al sistema de
información que tiene actualmente. Esta nueva funcionalidad implica que el sistema calcule
los intereses generados sobre un monto determinado si se utiliza una proyección con interés
compuesto, una proyección con interés simple y una proyección con la diferencia de estos.
Los montos mencionados están relacionados con las becas universitarias (de pregrado y
posgrado) que se van a otorgar por parte de la operadora a los potenciales beneficiarios. Las
ecuaciones para determinar los intereses son las siguientes: 
 */

/**
Requerimientos :
* 
1) Crear clase 'BecaUniversitaria' ✔️
 * 2) Crear método compararInversion -> Sobrecar de métodos con parámetros y sin parámetros
 *      parámetros:
 *          * tiempo
 *          * monto
 *          * interes
 *      Retornar un String:
 *       *"La diferencia entre la proyección de interés compuesto e interés simple es: $" + diferencia
 *      En el caso de que no se envien datos o se instancie con el constructor sin parámetros:
 *       *"No se obtuvo diferencia entre las proyecciones, revisar los parámetros de entrada."
 * 
 * Aplicar sobrecarga de métodos al constructor.
 * 
 * NOTA:
 *  Redondear los resultados con Math.round()
 * 
 * 
 * FÓRMULAS:
 * interesSimple = monto * (interes/100) * tiempo ✔️
 * interesCompuesto = monto * ( (1+ (interes/100))^tiempo - 1 ) ✔️
 * compararInversion = interesCompuesto - interesSimple 

/** 
 Para integrar esta nueva funcionalidad al sistema de información de la operadora, se solicita
crear una clase llamada BecaUniversitaria, la cual debe tener el método
compararInversion(). Dicho método, podrá recibir como parámetros las entradas (int
pTiempo, double pMonto, double pInteres), o no recibir ningún parámetro, y realizar la
comparación a partir de los atributos de la clase. El método compararInversion() debe
utilizar los métodos calcularInteresSimple() y calcularInteresCompuesto(), los cuales
deben retornar el total de interés simple y compuesto a partir de las ecuaciones dadas, en
formato double, sin recibir parámetros de entrada.
Tener en cuenta que si no se pasan argumentos al constructor de la clase BecaUniversitaria,
sus atributos deben ser inicializados en cero.
 
 */
public class Reto1 {
// una clase esta compuesta por 5 secciones : 1- atributos 2-constructores 3-consultores  4- modificadores 5-acciones
    
    public static void main(String[] args) {
        // PRIMER OBJETO
        BecaUniversitaria becaUniversitaria = new BecaUniversitaria(); //creando una instancia de la clase becau
        System.out.println(becaUniversitaria.calcularInteresSimple()); //llamado de la funcion calcular interes simple
        System.out.println(becaUniversitaria.calcularInteresCompuesto()); //llamado de la funcion calcular interes compuesto
        System.out.println(becaUniversitaria.compararInversion(60, 13000, 1.4)); //// enviando parametros y llamado de la funcion comparar inversion
        
        //SEGUNDO OBJETO
        System.out.println("*******************************************************************");
        BecaUniversitaria becaUniversitaria_2 = new BecaUniversitaria(48, 10000, 2.0); //CREANDO OBJETO CON PARAMETROS (Ptiempo, Pmonto, Pinteres)
        System.out.println(becaUniversitaria_2.calcularInteresSimple());
        System.out.println(becaUniversitaria_2.calcularInteresCompuesto());
        System.out.println(becaUniversitaria_2.compararInversion(48,10000,2));

        // TERCER OBJETO
        System.out.println("*******************************************************************");
        BecaUniversitaria becaUniversitaria_3 = new BecaUniversitaria();
        System.out.println(becaUniversitaria_3.calcularInteresSimple());
        System.out.println(becaUniversitaria_3.calcularInteresCompuesto());
        System.out.println(becaUniversitaria_3.compararInversion(0,0,0));
        System.out.println(becaUniversitaria_3.compararInversion());
    }
    
}
