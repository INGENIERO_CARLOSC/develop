/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package matriz3;

import java.util.Scanner;

/**
 Crear dos matrices de nxn y sumar sus valores,
 * los resultados se deben almacenar en otra matriz.
 * Los valores y la longitud, serán insertados por el usuario. 
 * Mostrar las matrices originales y el resultado.

 */
public class Matriz3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada =  new Scanner(System.in);
        System.out.println("Porfavor digite la dimencion de ma matriz cuadrada 1 ");
        int dimencion =  entrada.nextInt();
      
        // delcarando las matrices y asignando la dimencion
        int matriz1[][] = new int[dimencion][dimencion];
        int matriz2[][] = new int[dimencion][dimencion];
        int matrizResultado[][] = new int[dimencion][dimencion];
        
        //pidiendo dato por dato de las 2 matrices y sumando la resultante
        for (int i = 0; i < matriz1.length ; i++) {
            for (int j = 0; j < matriz1.length; j++) {
                //-----------
                System.out.println("Digite el valor para la fila: "+i+ " en la columna "+j+ " Matriz 1");
                matriz1[i][j]=entrada.nextInt();
                
                System.out.println("Digite el valor para la fila: "+i+ " en la columna "+j+ " Matriz 2");
                matriz2[i][j]=entrada.nextInt();
                
                matrizResultado[i][j]=matriz1[i][j]+matriz2[i][j];
            }
            
        }
        System.out.println("matriz 1");
        mostrarMatriz(matriz1);
        System.out.println("matriz 2");
        mostrarMatriz(matriz2);
        System.out.println("matriz resultante");
        mostrarMatriz(matrizResultado);      
        
        
    } //funcion que imprime las 3 matrices
    public static void mostrarMatriz(int matrizfuncion[][]){
        for(int i=0; i<matrizfuncion.length; i++){
            for(int j=0; j<matrizfuncion.length; j++){
                System.out.print(matrizfuncion[i][j]+" ");
            }
            System.out.println(" ");
        }
    }
}