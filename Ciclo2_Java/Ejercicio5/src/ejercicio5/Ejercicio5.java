/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejercicio5;

import javax.swing.JOptionPane;

/**
 	Calcule las estaciones del año haciendo uso de las sentencias if y else


 */
public class Ejercicio5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       var mes =  Integer.parseInt(JOptionPane.showInputDialog("Ingrese el mes del año"));
       var estacion = "Estacion desconocida";
       
       if (mes ==12 || mes ==1 || mes ==2){
           estacion = "Invierno";       
       }
       else if(mes ==3 || mes ==4 || mes ==5){
           estacion = "Primavera";  
       }
       else if (mes >= 6 && mes <=3){
           estacion = "verano"; 
       }
       else if(mes ==9 || mes ==10 || mes ==11){
           estacion = "Otoño";  
       }
       JOptionPane.showMessageDialog(null,"La estacion es:" +estacion);
        System.out.println("La estacion es : "+estacion);
    }
    
}
