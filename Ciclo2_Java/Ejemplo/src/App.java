import java.util.List;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        // esto es un comentario de linea

        /*
         * bloque de comentario
         */

        /**
         * comentario de documentacion
         */

         /*  SENTENCIAS  */
        
         var list = List.of(1,2,3, "hola");
         var example = "example";
         
         System.out.println(example);
        System.out.println(list);

        /* Tipos de datos
         * boolean / verdadero o falso / 1 bit
         * byte /   8 bits   / -128 a 127
         * char  / 16 bits / 1 caracter
         * short / 16 bits / -32768 al 32767
         * int / 32 bits / -3.402823e38 al 3.402823e38
         * long / 64 bits / -90000000
         * float / 32 bits 
         * double / 64 bits
         */

         /*
          *  codigo asci
          \t tabulador
          \b retroceso backspace
          \n nueva linea
          \r retorno de carro
          \f salto de linea
          \' comilla simple
          \" comilla doble
          \\ barra invertida

          */

         // declaracion de Objetos
         Scanner entrada = new Scanner(System.in); /** objeto scanner para leer por teclado */
         /*Declaracion de variables */
         int numero; /**declarando variables */
         double decimal; /** muchos decimales**/
         float flotante; /* valores decimales pero con 1 decimal*/
         String cadena; /*cadena de caracteres */
 //        char caracter ;
         
            /* Tipos de variables ingresadas por teclado * /
         System.err.println("Digite un valor entero: "); /** Mensaje de pantalla*/        
         numero = entrada.nextInt(); /* nextInt() variable de tipo entero*/ 
         System.err.println("Digite un valor decimal: "); /** Mensaje de pantalla*/
         decimal = entrada.nextDouble(); /* valores decimales*/
         System.err.println("Digite otro valor decimal: "); /** Mensaje de pantalla*/
         flotante = entrada.nextFloat();
         System.err.println("escriba su nombre "); /** Mensaje de pantalla*/
         cadena = entrada.next();
 //        System.err.println("escriba una letra o caracter "); /** Mensaje de pantalla*/
 //        caracter = entrada.nextLine().charAt(1); /** captura el valor 1 de un texto */
         
         /*Mostrando los resultados*/        
         System.err.println("El primer valor es: "+numero +" el segundo valor es:"
                 + decimal+" El tercer valor es:  "+flotante+ " El cuarto valor es: "+cadena); /* mostrando impresion*/
    }
}
