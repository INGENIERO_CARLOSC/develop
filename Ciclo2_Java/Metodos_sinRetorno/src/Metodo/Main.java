/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Metodo;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Main {
    //psvm para crear 
    public static void main(String[] args) {
        Scanner entrada =  new Scanner(System.in);
        System.out.println("Digite el valor 1: ");
        int num1 = entrada.nextInt();
        System.out.println("Digite el valor 2: ");
        int num2 = entrada.nextInt();
        
        //creando objeto instanciado de la clase operacion
        //clase    objeto           constructor
        Operacion operacion1 = new Operacion();
        
        
        // llamando los metodos
        operacion1.sumar(num1, num2);
        operacion1.resta(num1, num2);
        operacion1.multi(num1, num2);
        operacion1.div(num1, num2);
        
        //mostrando resultados con el metodo         
        operacion1.mostrardatos();
        
    }
}
