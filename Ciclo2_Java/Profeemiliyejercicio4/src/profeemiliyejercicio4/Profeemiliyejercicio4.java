/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package profeemiliyejercicio4;

/**
3.	Para este ejemplo haremos algo un poco más complejo.
* El ejemplo consiste en contar al interior de un ciclo for,
* cuántos números entre el 0 y el 10.000 son múltiplos del 20.
* Para ello haremos uso del operador % (módulo) 
* que obtiene el residuo de una división y también usaremos 
* un pequeño condicional para verificar que el módulo
* sea cero al dividir por 20.

 */
public class Profeemiliyejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int contador = 0;
        for(int i =0; i <= 10000 ; i++){
            if(i%20 == 0){
               contador ++;             
            }                                   
        }
        System.out.println("La cantidad de numeros multiplo de 20 son: " +contador);
    }    
}
