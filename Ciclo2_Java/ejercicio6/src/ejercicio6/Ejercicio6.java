/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejercicio6;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Ejercicio6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner Imp =  new Scanner(System.in);
        System.out.println("Ingrese la cantidad de notas ");
        int notas = Imp.nextInt();
        int sum =0;
        float proomedio =0;
        for (int i=0; i < notas; i++){
               System.out.println("Ingrese la nota "+(i+1));
               float  Not = Imp.nextFloat();
               sum = (int) (sum + Not);
        }
        proomedio = (sum/notas);
        System.out.println("El promedio de las notas es: "+proomedio);        
    }    
}
