
package matrices1;

import java.util.Scanner;

public class Matrices1 {

    
    public static void main(String[] args) {
        Scanner valores = new Scanner(System.in);
        System.out.println("Digite la cantidad de filas: ");
        int filas = valores.nextInt();
        System.out.println("Digite la cantidad de columnas: ");
        int columnas = valores.nextInt();
        
        int Matriz[][] =  new int[filas][columnas];
        
        for (int i = 0; i < Matriz.length; i++) { //i recorre las filas
            for (int j = 0; j < Matriz.length; j++) { //j reccoree las columnas
                Matriz[i][j] = (i*Matriz.length)+(j+1);
                System.out.print(Matriz[i][j]+" ");
            }
            System.out.println(" ");//salto de linea por filas
        }
 
    }
    
}
