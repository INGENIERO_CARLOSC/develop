/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ciclo_for;

/**
 Realizar un programa donde los, 
 * números pares entre el número 500 y el 1000.

 */
public class Ciclo_for {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
            /**
     Realizar un programa donde los, 
     * números pares entre el número 500 y el 1000.

     */
        for(int i = 500 ; i<= 1000; i +=2 ){
            System.out.println(i);
        }
        
        /*
        2.	También haga sus iteraciones en sentido inverso, 
        es decir disminuyendo el valor de la variable de control vamos 
        a imprimir por pantalla una cuenta regresiva desde el número 100
        hasta el CERO, veamos:

        */
        System.out.println("Ejercicio 2");
        for(int i = 100 ; i>= 0; i-=10 ){
            System.out.println(i);
        }
        //-----------aumente con una variable y dismuya con otra----------------------
        System.out.println("Oto ejemplo 3");
        
        for(int a = 0, b= 6; a <10 && b>0 ; a++, b--){
            System.out.println(a+"-"+b+" ");
        
        }
        
        
    }
    
}
