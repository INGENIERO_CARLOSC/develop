/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Colegio;

/**
 *
 * @author Usuario
 */
public class Persona {
    //Atributos
    private String nombre;
    private String apellido;
    private int edad;

    // creando metodo constructor control + espacio
    public Persona(String nombre, String apellido, int edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
// insertando getter para heredar click derecho generar codigo getter seleccionar atributos
    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public int getEdad() {
        return edad;
    }
    
    
}
