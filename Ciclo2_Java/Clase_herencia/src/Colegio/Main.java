/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Colegio;
//clase abtracta o superior para llamar otra clases aun que depende de otra clases usa persona pero no lo usa directamente
/**
 *
 * @author Usuario
 */
public class Main {
    public static void main(String[] args) {
        // acceder a otras clases con objets instanciando la clase estudiante
        //5.0f indica que el valor es flotante
        Estudiante estud1 =  new Estudiante("carlos","cogua",30, 2354,5.0f); //enviando datos al metodo contructor 
        Estudiante estud2 =  new Estudiante("mario","benedeti",34, 2354,5.0f); //enviando datos al metodo contructor 
        Docente doc1 = new Docente("tor","odin",60,23453,12000000);
        //instanciando el metodo para mostrar datos
        estud1.mostrarDatos();       
        estud2.mostrarDatos();
        // instanciando docente 
        doc1.mostrarDatos();
        
        
    }
    
}
