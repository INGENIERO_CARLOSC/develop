
package Colegio;
//colocar exten para heredar de persona
public class Docente extends Persona {
    //atributos
    private int codigoDocente;
    private float sueldo;
    
    
    //contructor
    public Docente(String nombre, String apellidos, int edad, int codigoDocente, float sueldo ) {
        super(nombre, apellidos, edad); //hereda 
        this.codigoDocente = codigoDocente;
        this.sueldo = sueldo;
    }
    
    // metodo para mistrar datos
    public void mostrarDatos(){
        System.out.println("El nombre es: "+getNombre());
        System.out.println("Los apellidos son: " +getApellido());
        System.out.println("La edad es: "+ getEdad());
        System.out.println("El codigo del docente es: "+codigoDocente);
        System.out.println("El sueldo es: "+sueldo);
        System.out.println("----------------------------------------------");
    
    }
    
}

