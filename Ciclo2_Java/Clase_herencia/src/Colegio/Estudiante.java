
package Colegio; // extendes es para heredar de persona
public class Estudiante  extends Persona{    
    //clase hija hereda de personas o extiende 
    //atributos propios del estudiante, los demas estan siendo heredados de Persona 
    //no se necesita poner los demas atributos heredados
    
    private int codigoEstudiante;
    private float notaFibal;

    //constructor automatico con lick derecho y seleccionando la clase padre y la hija
    //reordenar los parametros segun el diagrama porque por defecto el contructor crea en orden local y luego de herencia 
    public Estudiante( String nombre, String apellido, int edad, int codigoEstudiante, float notaFibal) {
        super(nombre, apellido, edad);// con el contructor accede a los atributos de la calse padre
        this.codigoEstudiante = codigoEstudiante;
        this.notaFibal = notaFibal;
    }
    
    // click derecho crear getter solo crea los locales por que los heredados pueden ser llamados 

    public int getCodigoEstudiante() {
        return codigoEstudiante;
    }

    public float getNotaFibal() {
        return notaFibal;
    }
    
    //metodo vacio y publico para mostrar un mensaje
     public void mostrarDatos(){ 
    //------llamando atributos encabsulados por que son privados en la clase padre
         System.out.println("El nombre es: "+getNombre());
         System.out.println("El apellido es: "+getApellido());
         System.out.println("La edad  es: "+getEdad());
         //los atrbutos locales se pueden llamar dorectamente
         System.out.println("El codigo del estudiante es : "+codigoEstudiante);
         System.out.println("La nota final  es : "+notaFibal);     
     }
    
    
    
  
        
}
