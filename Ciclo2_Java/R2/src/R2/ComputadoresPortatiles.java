/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package R2;

/**
 *
 * @author Usuario
 */
public class ComputadoresPortatiles extends Computadores{
    private final static Integer PULGADAS_BASE = 20;
    private Integer pulgadas;
    private boolean camaraITG; //false
    
    
        // Constructor
    public ComputadoresPortatiles(){//si no trae parametros devuelva los siguientes valores
        this(PRECIO_BASE, PESO_BASE, CONSUMO_W, PULGADAS_BASE, false);
    }
    // Constructor
    public ComputadoresPortatiles(Double precioBase, Integer peso){
        this(precioBase, peso, CONSUMO_W, PULGADAS_BASE, false); 
    }
    // Constructor
    public ComputadoresPortatiles(Double precioBase, Integer peso, char consumoW, Integer
    pulgadas, boolean camaraITG) {
        super(precioBase, peso, consumoW);//treyendo datos del get 
        this.pulgadas = pulgadas;
        this.camaraITG =  camaraITG;
    }
    
    
    // Métodos--------------------------------------------------------
    public Double calcularPrecio() {
    // Método Código return adicion;
        Double adicion = super.calcularPrecio();
        if(pulgadas > 40){
            adicion += precioBase * 0.3;        
        }
        if(camaraITG){
            adicion += 50.0;
        }
        return adicion;
    }   
    
}
