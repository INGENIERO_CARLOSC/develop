/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package R2;

/**
 *
 * @author Usuario
 */ 
//--------------------extendiendo o hereando de la clase computadores
public class ComputadoresMesa  extends Computadores{
    //atributos privados y constantes    
    private final static Integer ALMACENAMIENTO_BASE = 50;
    private Integer almacenamiento;
    
    //constructuores o sobrecarga de metodos
    public ComputadoresMesa() {//si no tiene entradas devuelva 
        this(PRECIO_BASE, PESO_BASE, CONSUMO_W,ALMACENAMIENTO_BASE );
    }
    
    public ComputadoresMesa(Double precioBase, Integer peso) { // si solo ingresa precio base y peso devuelva eso mismo
        this(precioBase, peso, CONSUMO_W, ALMACENAMIENTO_BASE);        
    }
    
    public ComputadoresMesa(Double precioBase, Integer peso, char consumoW, Integer
almacenamiento) { //si recibe los parametros completos  llame los gets de la clase padre y opere el switch 
        super(precioBase, peso, consumoW);
        this.almacenamiento = almacenamiento; //almacene el valor en la variable local
    }
    
        // Métodos-----------------------------
    public Double calcularPrecio() { //sobrecarga de metodos
        Double adicion = super.calcularPrecio(); //llamando el metodo de la clase padre el swtich
                if (almacenamiento > 100) {
            adicion += 50.0;
        }

        return adicion;
    }

    public Integer getCarga() {
        return almacenamiento;
    }
}

    
   
