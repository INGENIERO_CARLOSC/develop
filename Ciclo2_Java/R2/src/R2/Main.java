/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package R2;

/**
 *
 * @author Usuario
 */
public class Main {
    public static void main(String[] args) {
    // creando objeto de primer ejemplo con el contructor de la clase padre
    Computadores computadores[] = new Computadores[6];
    // creando objetos 6 pcs
    computadores[0] = new Computadores(150.0, 70, 'A');
    computadores[1] = new ComputadoresMesa(70.0, 40);
    computadores[2] = new ComputadoresPortatiles(600.0, 70, 'D', 50, false);
    computadores[3] = new Computadores();
    computadores[4] = new Computadores(500.0, 60, 'A');
    computadores[5] = new Computadores(700.0, 50, 'D');
    
    // creando objeto con clase hija preciototal
    PrecioTotal solucion1 = new PrecioTotal(computadores);
    //haciendo uso de la funcion para mostrar precio del pedido
    solucion1.mostrarTotales();
    
    //saltos de linea
    System.out.println("");
    System.out.println("");
    
    // ejemplo 2creando el objetos computador con el constructor
    Computadores computador[] = new Computadores[4];
    // enviando parametros  de 4 tipos de pc
    computador[0] = new Computadores(60.0, 10, 'D');
    computador[1] = new ComputadoresMesa(300.0, 40, 'Z', 40);
    computador[2] = new ComputadoresPortatiles(50.0, 10, 'A', 70, false);
    computador[3] = new Computadores(50.0, 10);
    
    // creando objeto para calcular el precio con contructor de la clase preciototal
    PrecioTotal solucion2 = new PrecioTotal(computador); //le enviamos el objeto creado
    // iniciando metodo de la clase precio total
    solucion2.mostrarTotales();
    System.out.println();//salto de linea
    

    }
    
}
