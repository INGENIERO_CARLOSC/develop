/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package matris2;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Matris2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Digite el numero de filas: ");
        int filas = entrada.nextInt();
        System.out.println("Digite el numero de columnas: ");
        int columnas = entrada.nextInt();
        //declarando la matriz
        int matriz[][] =  new int[filas][columnas];
        
        //llenando matriz
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                matriz[i][j]= generarnumeroaleatorio(10,30);
                System.out.print(matriz[i][j]+" ");
            } 
            System.out.println("");
        }
    }
    //funcion para los numeros aleatorios
    public static int generarnumeroaleatorio(int minimo, int maximo) { //metodo  con retorno
        return ((int)Math.floor(Math.random()*(minimo-(maximo+1))+(maximo+1)));                
    }    
}
