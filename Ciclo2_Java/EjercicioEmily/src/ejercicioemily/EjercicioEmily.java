/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejercicioemily;

import java.util.Scanner;

/**
hora veremos otro ejemplo sencillo en cual haremos que el que muestre
* el nombre del cliente y la cantidad de artículos comprados, 
* si la cantidad es menor de 5 pagara en efectivo, 
* si es mayor o igual de 5 y menor de 12 pagara con tarjeta, 
* si es mayor de 11 pagara con cheque.

 */
public class EjercicioEmily {
    
    public static void main(String[] args) {
        Scanner articulo = new Scanner(System.in); /* objeto escanner capturar datos*/
       /*----------------------------------------------------------*/
        System.out.println("Digite el nombre del cliente: ");
        String nombre = articulo.nextLine(); /*almacenar el valor en la variables*/
        System.out.println("ingrese la cantidad de productos: ");
        var cantidad = articulo.nextInt();
        
        /*-------------------------------------------------*/
        if(cantidad > 0 && cantidad <5 ){
            System.out.println("el señor " +nombre+" Paga con efectivo ");        
        }
        else if(cantidad >= 5 && cantidad <12 ){  
            System.out.println("el señor " +nombre+"Paga con tarjeta ");          
        }
        else if(cantidad > 11  ){  
            System.out.println("el señor "+nombre+" Paga con cheque ");          
        }
        else{
            System.out.println("el señor "+nombre+"Ingreso un valor No valido");  
        }
        
    }
    
}
