
package arraysmetodos;

import javax.swing.JOptionPane;

/**
 *
 * @author Usuario
 */
public class ArraysMetodos {

    /**
     *sumar los numeros del 1 al 100 y mostrar su promedio
     */
    public static void main(String[] args) {
        
        
        
        
         String texto =  JOptionPane.showInputDialog("Digite el tamaño del arreglo"); //pidiendo el valor por pantalla grafica
        int numero [] = new int[Integer.parseInt(texto)]; //llanando el tamañao del vector con la variable texto convertida a entero
        
        
        // invocamos los metodos 
        rellenarAleatorioArray(numero,0,9); //invocando el metodo y enviandole parametros de tamaño de vector y el intervalo entre (0 y 9 o a y b)
        mostrarArray(numero); //mostrando el contenido del array
    }
    
    
    //-------------------------METODO PARA RELLENAR EL VECTOR -----------------
    public static void rellenarAleatorioArray(int Tarreglo[], int a, int b){ //valor inicial a valor final b 
        for (int i = 0 ; i <Tarreglo.length; i++){
            //tamaño del arreglo--------------------generacion de numeros aleatorios entre 0 y 9
            Tarreglo[i] = ((int)Math.floor(Math.random()*(a-b)+b)); //variables de entrada
        
        }
            
    }
    //-----------------------METODO PARA MOSTRAR CONTENIDO DEL VECTOR-----
    public static void mostrarArray(int Marreglo[]){
          for (int i = 0 ; i <Marreglo.length; i++){
              System.out.println("En el indice :" +i+ " Se encuentra el valor : "+Marreglo[i]);
          
          }
    
       
    }
    
}
