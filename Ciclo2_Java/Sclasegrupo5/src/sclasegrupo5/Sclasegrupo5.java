
package sclasegrupo5;


public class Sclasegrupo5 {

    
    public static void main(String[] args) {
        /**
      ==========CLASE ====STRINGS=======================================
     */ System.out.println("========CLASE ====STRINGS=======================================");
        String cadena = "Programando en java";
        /*Mostrando la posicion o existencia de un caracter o palabra indexOf*/
        System.out.println("Mostrando la posicion o existencia de un caracter o palabra: "+cadena.indexOf("java"));
        /*Mostrando la longiud del string*/
        System.out.println("Mostrando la longiud del string: "+cadena.length());
        /*Mostrando la ultima aparicion de el caracter*/
        System.out.println("Mostrando la ultima aparicion de el caracter: "+cadena.lastIndexOf("a"));
        /*Mostrando caracteres con el indice indicado*/
        System.out.println("Mostrando caracteres con el indice indicado: "+cadena.charAt(0));
        /*strando caracteres por rangos o revanadas*/
        System.out.println("Mostrando caracteres por rangos o revanadas posicion 0 al 11 : "+cadena.subSequence(0, 11));
        /*convertir la cadena en mayusculas   para hacer la inversa es tolowercase()*/
        System.out.println("Mostrando cadena en mayuscula : "+cadena.toUpperCase());
        /*validando cadenas*/
        System.out.println("Validando el contenido de la cadena : "+cadena.equals("programando en Java"));
        /*validando cadenas discriminando mayusculas y minusculas*/
        System.out.println("validando cadenas discriminando mayusculas y minusculas: "+cadena.equalsIgnoreCase("programando en Java"));
        /*Me devuelve copia de la cadena sin espacion intermedios*/
        System.out.println("Me devuelve copia de la cadena sin espacion intermedios: " +cadena.trim());
        
        /* ==================CLASE MATH=======================================*/
        System.out.println(" /* ==================CLASE MATH=======================================*/ ");
        /*usando la constante Pi*/
        System.out.println("usando la constante Pi:  "+Math.PI);
        /*usando la constante E*/
        System.out.println("usando la constante E:  "+Math.E);
        /*mostrabdo valor absoluto de un numero abs()*/
        System.out.println("mostrabdo valor absoluto de un numero abs():  "+Math.abs(-5));
        /* sin()*/
         System.out.println("seno :  "+Math.sin(0.5));
        /*mostrabdo valor absoluto de un numero cos()*/
        /*mostrabdo valor absoluto de un numero tan()*/
        /*mostrabdo valor absoluto de un numero log()*/
        /*Potencia de un numero pow()*/
        System.out.println("Potencia de un numero pow():  "+Math.pow(2,4));
        /*Redonde de un numero decimal round()*/
        /*Sproximacion entera mayor floor()*/
        /*Aproximacion entera menor ceil()*/
        /*===================CLASE WRAPPER =======================================*/
        System.out.println("/*===================CLASE WRAPPER =======================================*/ ");
        /*Tratando datos primitivos como objetos, clases envoltorio */
        
        String cadena2 = "3.15";
        float num = Float.valueOf(cadena2);
        System.out.println("convierte un string a numero "+ num+2);
        
        /*===CLASE SYSTEM==========*/
        System.out.println(" /*===CLASE SYSTEM======dispone de variables in, out y err ====*/ ");
        
        /* Ejercicio numero de la suerte */    
        
        
    }   
   
}
