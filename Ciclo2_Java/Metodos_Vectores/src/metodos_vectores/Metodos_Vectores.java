
package metodos_vectores;

import javax.swing.JOptionPane;

public class Metodos_Vectores {

    public static void main(String[] args) {
        final int size =10; // final es  una constante  palabra reservada no cambiara
        int num[] =  new int[size]; //asignando la constante
        
        //invocador    
        rellenarArray(num);
        mostrarArray(num);
    }
    
    // MEtodos --------------------------------------------------
    public static void rellenarArray(int vector[]){
        for (int i = 0; i < vector.length; i++) {
            String texto = JOptionPane.showInputDialog("Digite el numero: "); 
            vector[i] = Integer.parseInt(texto); //captura el valor escrito por teclado y loc pasa de string a entero
        }
    }
    
     public static void mostrarArray(int vector[]){
        for (int i = 0; i < vector.length; i++) {
            System.out.println("En el indice: "+ i + "Esta el numero : "+ vector[i] );
        }
    }
    
}
