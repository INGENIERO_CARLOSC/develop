
package com.mycompany.calculadora71.vista;

import com.mycompany.calculadora71.controlador.CalculadoraController;


public interface CalculadoraVista {
    //medotos de calculadora consola
    public String getNumeroUno();
    public String getNumeroDos();
    public void setResultado(String resultado);
    public void iniciar(CalculadoraController controller);
    public void mostrarException(Exception ex);
 

    
}
