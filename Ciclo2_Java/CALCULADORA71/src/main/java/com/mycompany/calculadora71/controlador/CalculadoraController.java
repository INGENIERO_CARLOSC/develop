package com.mycompany.calculadora71.controlador;

import com.mycompany.calculadora71.modelo.CalculadoraModelo;
import com.mycompany.calculadora71.vista.CalculadoraVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculadoraController implements ActionListener{
    
    private final CalculadoraVista view;
    private final CalculadoraModelo model;
    private Operacion operation;

    public CalculadoraController(CalculadoraVista view, CalculadoraModelo model) {
        this.view = view;
        this.model = model;
        this.operation = Operacion.SUMA;
    }
    
   public void setOperacion(Operacion operation){
       this.operation = operation;
   }
    
   public void actionPerformed(ActionEvent e){ // transformacion de string a intero y enviandolo al modelo basicamente en es un bridge
      
       try{
       model.setNumeroUno(Integer.valueOf(view.getNumeroUno()));
       model.setNumeroDos(Integer.valueOf(view.getNumeroDos()));
       switch(operation){
           case SUMA:
               model.sumar();
               break;
            case RESTA:
               model.restar();
               break;   
            case MULTIPLICACION:
               model.multiplicar();
               break;   
            case DIVISION:
               model.dividir();
               break;  
            case MODULO:
               model.calcularModulo();
               break;   
            default:  
               throw new UnsupportedOperationException("Operacion no implementada "+ operation); 
       }      
       // resultado
       view.setResultado(model.getResultado().toString());

       
       
       }
       catch(Exception ex){
           view.mostrarException(ex); //es la mispa exepcion de calculadora consola 
       }

      
   }
   //iniciar
   public void iniciar(){
       view.iniciar(this);
   }   
   
   
    
}
