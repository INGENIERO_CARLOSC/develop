package com.mycompany.calculadora71.vista;

import com.mycompany.calculadora71.controlador.CalculadoraController;
import com.mycompany.calculadora71.controlador.Operacion;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CalculadoraConsola implements CalculadoraVista {
    
    private CalculadoraController controller;
    private String numeroUno;
    private String numeroDos;
    
    private void menuPrincipal(){
        try (var input = new BufferedReader (new InputStreamReader(System.in))){
            var mainLoop = true;
            while(mainLoop){
                System.out.println("CALCULADORA - MANUAL");
                System.out.println("Puede Realizar operaciones matematicas");
                System.out.println("Seleccione '+': SUMA");
                System.out.println("Seleccione '-': RESTA");
                System.out.println("Seleccione '*': MULTIPLICACION");
                System.out.println("Seleccione '/': DIVISION");
                System.out.println("Seleccione '%': MODULO");
                System.out.println("Seleccione '.': SALIR");
                System.out.println("Ingrese el simbolo de la operacion a realizar: ");
                var opcion = input.readLine();
                switch(opcion){
                    case "+":
                        controller.setOperacion(Operacion.SUMA);
                        break;
                    case "-":
                        controller.setOperacion(Operacion.RESTA);
                        break;  
                    case "*":
                        controller.setOperacion(Operacion.MULTIPLICACION);
                        break;    
                    case "/":
                        controller.setOperacion(Operacion.DIVISION);
                        break;    
                    case "%":
                        controller.setOperacion(Operacion.MODULO);
                        break;    
                    case ".":
                        System.out.println("HASTA LA VISTA BABY");
                        mainLoop = false;
                        continue;
                    default:
                        System.err.println("La opcion "+opcion+ "NO ES VALIDA!!");
                        System.out.println("Presione 'Enter' para continuar: ");
                        input.readLine();
                        continue;
                }
                System.out.println("Ingrese el primer numero : ");
                numeroUno = input.readLine();
                System.out.println("Ingrese el segundo numero : ");
                numeroDos = input.readLine();
                
                controller.actionPerformed(null);
                System.out.println("Presione 'Enter' para continuar");
                input.readLine();
                
            }
        } catch (IOException ex) {
            System.err.println("Error en el sistema, LO SENTIMOS!!" +ex.getMessage());
        }
        
    }
//_------------procesi de sobreescritura-------------------------------------------------------------------
    @Override
    public String getNumeroUno() {
        return numeroUno;
    }

    @Override
    public String getNumeroDos() {
        return numeroDos;
    }
    
     @Override
   public void setResultado(String resultado){
       System.out.println("El resultado es: "+ resultado);
   }
    @Override
   public void iniciar(CalculadoraController controller){
       this.controller = controller;
       menuPrincipal();
   }
    @Override
   public void mostrarException(Exception ex){
       System.err.println("Exception: "+ ex.getMessage());
   }


   
  
    
}
